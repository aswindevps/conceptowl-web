$(function () {
	//Answer select
	$('.qa-checkbox:checked').parent().addClass('active');
	
	$('body').on('click', '.qa-a-overlay',  function () {
		if($(this).parent().hasClass('active') && $(this).siblings('qa-checkbox').prop('checked', true)){
			$(this).parent().removeClass('active').children('.qa-checkbox').prop('checked', false);
		}else{
			$(this).parent().addClass('active').children('.qa-checkbox').trigger('click');
		}
	});
	
	
	
	$(window).load(function(){
		//Question slider
		var start = $('.current-count').data('number');
		var total = $('.question-list-slides').children().length;
		total = total - 6;
		var pos;
		
		if(start <= 6){
			pos = 0;
		}else if(start >= total){
			pos = total;
		}else{
			pos = start - 3;
		}
		
		$('.question-list-pane').flexslider({
			animation: "slide",
			selector: ".question-list-slides > a", 
			direction: "vertical",
			animationSpeed:50,
			animationLoop: false,
			slideshow: false,
			controlNav: false,
			prevText: "",
			nextText: "",
			itemWidth:40,
			itemMargin: 0,
			startAt: pos
		});
		
		if(start > 7){
			$('.flex-prev').trigger('click');
		}
		
		
		//Previous slide
		$('.prev-set').mousedown(function(){
			int00 = setInterval(function() { prevSlide(); }, 50);
		}).mouseup(function() {
			clearInterval(int00);
		});

		function prevSlide() {
			var current = $('.question-list-pane').data('flexslider').currentSlide;
			
			if(current != 0){
				$('.flex-prev').trigger('click');
			}
		}
		
		
		//Next slide
		$('.next-set').mousedown(function(){
			int00 = setInterval(function() { nextSlide(); }, 50);
		}).mouseup(function() {
			clearInterval(int00);
		});

		function nextSlide() {
			var current = $('.question-list-pane').data('flexslider').currentSlide;
			
			if(current < total){
				$('.flex-next').trigger('click');
			}
		}
	});
	
	//Accordion
	$('body').on('click', '.qa-answer-accordion-title',  function () {
		if ( $(this).siblings('.qa-answer-accordion-solution').is( ":hidden" ) ) {
			$(this).siblings('.qa-answer-accordion-solution').slideDown( "fast" );
		} else {
			$(this).siblings('.qa-answer-accordion-solution').slideUp("fast");
		}
	});

	//Matrix select
	$('.qa-checkbox:checked').parent().addClass('active');
	
	$('body').on('click', '.qa-answer-matrix td',  function () {
		if($(this).hasClass('active') && $(this).children('qa-checkbox').prop('checked', true)){
			$(this).removeClass('active').children('.qa-checkbox').prop('checked', false);
		}else{
			$(this).addClass('active').children('.qa-checkbox').prop('checked', true);
		}
	});
	
	//Error reporting
	$('body').on('click', '.qa-report', function() {
		$('.report-popup-container').addClass('active');
	});
	
	$('body').on('click', '.report-no', function() {
		$('.report-popup-container').removeClass('active');
	});
	
	//Hints
	$('body').on('click', '.qa-hints', function() {
		$('.hints-popup-container').addClass('active');
	});
	
	$('body').on('click', '.hints-popup .popup-close', function() {
		$('.hints-popup-container').removeClass('active');
	});
	
	//Open left panel
	$('body').on('click', '.menu-icon', function() {
		$('.qa-sidebar').animate({
			marginLeft: 0
		}, 500);
	});
	
	$('body').on('click', '.sidebar-close', function() {
		$('.qa-sidebar').animate({
			marginLeft: -500
		}, 500);
	});
});
