/*global angular */

angular.module('concept.factories', [])


.factory('localstorage', ['$window', '$rootScope', function($window, $rootScope) {
            'use strict';

            return {
                set: function(key, value) {
                    $window.localStorage.setItem(key, value);
                },
                get: function(key, defaultValue) {
                    return $window.localStorage.getItem(key, defaultValue);
                },
                setObject: function(key, value) {
                    $window.localStorage[key] = JSON.stringify(value);
                },
                getObject: function(key) {
                    return JSON.parse($window.localStorage[key] || '{}');
                }
            };
        }

    ])
    .factory('TestService', ['$http', function($http) {
        'use strict';
        return {
            getAllTest: function(url) {
                return $http.get(url);
            },
            getPrevTest: function(url) {
                return $http.get(url);
            },
            getUpTest: function(url) {
                return $http.get(url);
            },
            postTestAnswers: function(url, sub) {
                return $http({
                    method: 'POST',
                    url: url,
                    data: sub,
                    'Content-Type': 'application/json'
                });
            }
        };
    }])
    .factory('Home', ['$http', function($http) {
        'use strict';
        return {
            getAllTest: function(url) {
                return $http.get(url);
            },
            postTestAnswers: function(url, sub) {
                return $http({
                    method: 'POST',
                    url: url,
                    data: sub,
                    'Content-Type': 'application/json'
                });
            }
        };
    }])



        .service("directiveService", function() {
        var listeners = [];

        return {
            subscribe: function(callback) {

                listeners.push(callback);
            },
            publish: function(msg) {
                console.log("ok");
                angular.forEach(listeners, function(value, key) {
                    value(msg);

                });
            }
        };
    })
    .factory('TestService', ['$http', function($http) {
        'use strict';
        return {
            getTestQuestions: function(url) {
                return $http.get(url);
            },
            postTestAnswers: function(url, sub) {
                return $http({
                    method: 'POST',
                    url: url,
                    data: sub,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
            }
        };
    }])
    .directive("mathjaxBind", function($compile) {
        return {
            restrict: "A",
            scope: {
                text: "@mathjaxBind"
            },
            controller: ["$scope", "$element", "$attrs", function($scope, $element, $attrs) {

                $scope.$watch('text', function(value) {

                    var $script = angular.element("<script type='math/tex; mode=" + ($attrs.mathjaxMode == undefined ? "inline" : $attrs.mathjaxMode) + "'>")
                        .html(value == undefined ? "" : value);
                    $element.html("");
                    $element.append($script);
                    MathJax.Hub.Queue(["Reprocess", MathJax.Hub, $element[0]]);

                });
            }]
        };
    })
    .directive('myDirective', function($compile, localstorage) {
        return {
            restrict: 'A',
            scope: {
                varToBind: '=myDirective'
            },
            link: function(scope, elm, attrs) {
                var j = '';


                window.setTimeout(function() {

                    console.log(scope.varToBind); //Outputs "Changed in controller"
                    outerList = scope.varToBind;
                    outerList = $compile(outerList)(scope);
                   
                    outerList = $compile(outerList)(scope);
                    elm.replaceWith(outerList);
                }, 10)


            }
        }
    })
    .directive("jQueryDirective", function(directiveService, $compile, $rootScope) {
        directiveService.subscribe(function(msg) {
            // pretend this is jQuery 
            var scope = $rootScope.sc;
            var template = $compile(msg)(scope);

            document.getElementById("question-option-" + 0).innerHTML = template;

            //$compile(document.getElementById("question-option-" + 0))(scope);
        });
        return {
            restrict: 'E'
        };
    })
    .directive('elemReady', function($parse) {
        return {
            restrict: 'A',
            link: function($scope, elem, attrs) {
                elem.ready(function() {
                    $scope.$apply(function() {
                        var func = $parse(attrs.elemReady);
                        func($scope);
                    })
                })
            }
        }
    });
