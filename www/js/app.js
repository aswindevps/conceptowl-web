// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('concept', ['ionic', 'concept.controllers', 'concept.services', 'ngCordova', 'googleplus'])

.value('appConfig', {
    baseUrl: "http://conceptowl.com/apis/"
})


.run(function($ionicPlatform, $rootScope, $ionicLoading, $window, localstorage, FACEBOOK_APP_ID, $state, $location, HardwareBackButtonManager, ngFB) {


        ngFB.init({
            appId: '1695283740705689'
        });
        $rootScope.online = navigator.onLine;
        $window.addEventListener("offline", function() {
            $rootScope.$apply(function() {
                $rootScope.online = false;
            });
        }, false);
        $window.addEventListener("online", function() {
            $rootScope.$apply(function() {
                $rootScope.online = true;
            });
        }, false);
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        localstorage.set("bookmark",false);
        localstorage.set('topic', "");
        $rootScope.logging = localstorage.get('logg_type');
        $rootScope.profile = JSON.parse(localstorage.get('profile'));
        //console.log($rootScope.profile);
        //console.log($rootScope.logging);
        if ($rootScope.logging === "0") {
            //console.log("done");
            // $rootScope.nav_views = false;
            $rootScope.prof_imag = $rootScope.profile.prof_imag;
            $rootScope.prof_name = $rootScope.profile.prof_name;
            $location.path('/heatmap');
        } else if ($rootScope.logging === "1") {
            //console.log("facebook");
            $rootScope.prof_imag = $rootScope.profile.prof_imag;
            $rootScope.prof_name = $rootScope.profile.prof_name;
            $location.path('/heatmap');
            /*facebookConnectPlugin.getLoginStatus(function(success) {
                if (success.status === 'connected') {
                    
                } else {
                    $state.go('newlogin');
                }
            });*/
        } else if ($rootScope.logging === "2") {
            //console.log("google");
            $rootScope.prof_imag = $rootScope.profile.prof_imag;
            $rootScope.prof_name = $rootScope.profile.prof_name;
            $location.path('/heatmap');
        }

        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleLightContent();
        }

        $rootScope.$on('$stateChangeSuccess', function(evt, toState, toParams, fromState, fromParams) {

            if (toState.userLogin && localstorage.get('logged') !== "true") {
                $state.go('chm');
            }

            if (fromState.testState && localstorage.get('logg_type') == "0") {
                HardwareBackButtonManager.disable();
            }
        });
        $rootScope.log_out = function(type) {
            // $rootScope.nav_views = true;
            //console.log(type);
            /*if (type === 1) {
                if (!window.cordova) {
                    facebookConnectPlugin.browserInit(FACEBOOK_APP_ID);
                }

                facebookConnectPlugin.logout(function() {

                    localstorage.set('logged', false);
                    localstorage.set('logg_type', 5);
                    $location.path('/newlogin');
                });
            }
            if (type === 2) {
                window.plugins.googleplus.logout(
                    function(msg) {
                        //console.log(msg);
                        localstorage.set('logged', false);
                        localstorage.set('logg_type', 5);
                        $location.path('/newlogin');
                    },
                    function(fail) {
                        //console.log(fail);
                    }
                );
            }*/
            localstorage.set('logged', false);
            localstorage.set('logg_type', 5);
            // $location.path('/newlogin');
        }
    })
    .service('HardwareBackButtonManager', function($ionicPlatform) {
        this.deregister = undefined;

        this.disable = function() {
            this.deregister = $ionicPlatform.registerBackButtonAction(function(e) {
                e.preventDefault();
                return false;
            }, 101);
        }

        this.enable = function() {
            if (this.deregister !== undefined) {
                this.deregister();
                this.deregister = undefined;
            }
        }
        return this;
    })
    .directive("scrolly", function($window, localstorage, $ionicScrollDelegate) {
        return function(scope, element, attrs) {
            angular.element($window).bind("scroll", function() {
                // //console.log(this.pageYOffset);
                if (this.pageYOffset != 0) {
                    localstorage.set('topic_pos', this.pageYOffset);
                }
                scope.$apply();
            });
        };
    })
    .config(function($stateProvider, $urlRouterProvider, GooglePlusProvider, $locationProvider) {

        GooglePlusProvider.init({
            clientId: '782733457039-ae986q0qsq93tshk9vqhsm2096qcsnou.apps.googleusercontent.com',
            apiKey: 'W2y65_hGsqUxPe_QkF2h-2yU'
        });
        
        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider



        // Each tab has its own nav history stack:

            .state('dash', {
                url: '/dash',
                cache: false,
                templateUrl: 'templates/tab-dash.html',
                userLogin: true,
                controller: 'DashCtrl'

            })
            .state('practice', {
                url: '/practice/:name/:sub/:paper/:type',
                cache: false,
                testState: true,
                templateUrl: 'templates/practice-test.html',
                userLogin: true,
                controller: 'testCtrl'

            })
            .state('schedule', {
                url: '/schedule',
                userLogin: true,
                cache: false,
                templateUrl: 'templates/schedule.html',
                controller: 'scheduleCtrl'

            })
            .state('ask', {
                url: '/ask-ques',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/ask-a-ques.html',
                controller: 'askCtrl'

            })
            .state('heatmap', {
                url: '/heatmap',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/heatmap.html',
                controller: 'heatmapCtrl'

            })
            .state('test', {
                url: '/test/:test/:paper/:time/:name',
                cache: false,
                userLogin: true,
                testState: true,
                templateUrl: 'templates/test.html',
                controller: 'alltestCtrl'

            })
            .state('analysis', {
                url: '/analysis/:num/:name',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/detailed-analysis.html',
                controller: 'analysisCtrl'

            })
            .state('pract_analysis', {
                url: '/pract_analysis/:num/:name',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/practice-analysis.html',
                controller: 'practAnalysisCtrl'

            })
            .state('home', {
                url: '/home',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/home.html',
                controller: 'homeCtrl'

            })
            .state('prev', {
                url: '/prev',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/previous.html',
                controller: 'prevCtrl'

            })
            .state('test-details', {
                url: '/test-details/:paper_id',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/thank.html',
                controller: 'thankCtrl'

            })
            .state('newlogin', {
                url: '/newlogin/:name/:sub/:mode/:id',
                cache: false,
                userLogin: false,
                templateUrl: 'templates/newLogin.html',
                controller: 'newLoginCtrl'
            })
            .state('newregister', {
                url: '/newregister',
                cache: false,
                userLogin: false,
                templateUrl: 'templates/newRegister.html',
                controller: 'newRegisterCtrl'
            })
            .state('practice_review', {
                url: '/practice_review',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/practice-review.html',
                controller: 'practViewCtrl'

            })
            .state('test_quest', {
                url: '/test_quest/:num',
                cache: false,
                templateUrl: 'templates/test-quest.html',
                controller: 'testQuestCtrl'

            })
            .state('quest_num', {
                url: '/quest_num',
                cache: false,
                templateUrl: 'templates/quesNum.html',
                controller: 'QuestNumCtrl'

            })
            .state('chm', {
                url: '/chm',
                cache: false,
                userLogin: false,
                templateUrl: 'templates/preview.html',
                controller: 'previewCtrl'
            })
            .state('tests', {
                url: '/tests',
                cache: false,
                userLogin: false,
                templateUrl: 'templates/taketest_preview.html',
                controller: 'testpreviewCtrl'
            })
            .state('forgot', {
                url: '/forgot',
                cache: false,
                userLogin: false,
                templateUrl: 'templates/forgotPassword.html',
                controller: 'forgotCtrl'
            })
            .state('pay', {
                url: '/pay/:id',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/pay.html',
                controller: 'payCtrl'
            })
            .state('practiceSet', {
                url: '/practiceSet/:topic/:subject',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/practice-set.html',
                controller: 'practiceSetCtrl'
            })
            .state('upcoming', {
                url: '/upcoming',
                cache: false,
                userLogin: true,
                templateUrl: 'templates/upcoming.html',
                controller: 'upCtrl'
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/chm');

    });
