angular.module('concept.controllers', [])
    .controller('heatmapCtrl', function($scope, $location, $ionicHistory, $state, $timeout, $interval, $ionicPlatform, $ionicScrollDelegate, HardwareBackButtonManager, TestService, PayService, $rootScope, $stateParams, localstorage, appConfig, $window) {
        $scope.overlay = true;
        $scope.sub_topics = false;
        $scope.dataFound = false;
        $ionicHistory.nextViewOptions({
            disableAnimate: true
        });
        $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 30000);
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $scope.overlay = true;
            $scope.getHeatMapData();
        }
        HardwareBackButtonManager.enable();

        $scope.top = localstorage.get('topic_pos');
        //////console.log('Position : ' + $scope.top);

        $scope.topic = localstorage.get('topic');
        if ($scope.topic == 'phy') {
            $scope.phy_sub_topics = true;
        } else if ($scope.topic == 'che') {
            $scope.che_sub_topics = true;
        } else if ($scope.topic == 'mat') {
            $scope.mat_sub_topics = true;
        }

        $scope.test_details = localstorage.getObject('lastTestDetails');
        //////console.log($scope.test_details);
        $scope.heatData;
        $scope.p = [];
        $scope.c = [];
        $scope.m = [];
        $scope.opener = [false, false, false];

        var phydata = 0;
        var chedata = 0;
        var matdata = 0;

        $scope.stud_id = localstorage.get('stud_id');
        /*$scope.position = localstorage.get('topic_pos');
        //////console.log($scope.position);*/

        $scope.phypercent = 0;
        $scope.chepercent = 0;
        $scope.matpercent = 0;
        $scope.heat_paper = $scope.test_details.paperId;
        $scope.subject_id = 0;
        $scope.opener[$scope.subject_id] = true;

        $scope.hide_nav = function() {
                $rootScope.navhider = false;
            }
            /*$scope.log_out = function() {
                $scope.nav_views = true;
                localstorage.set('logged', false);
            }*/
        $scope.phy_topics = function(id) {
            $scope.phy_sub_topics = !$scope.phy_sub_topics;
        }
        $scope.che_topics = function(id) {
            $scope.che_sub_topics = !$scope.che_sub_topics;
        }
        $scope.mat_topics = function(id) {
            $scope.mat_sub_topics = !$scope.mat_sub_topics;
        }

        $scope.getHeatMapData = function() {
            TestService.getHeatMap(appConfig.baseUrl + "get_concept_heatmap_details.json?&student_id=" + $scope.stud_id).success(function(successData) {

                if (successData !== null) {
                    $scope.dataFound = true;
                    $scope.overlay = false;
                    console.log(successData);
                    $scope.heatData = successData;
                    // $timeout(function() {
                    //     $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                    //     //////console.log($scope.top);
                    // }, 1000);

                    /* $interval(function() {
                         //////console.log($ionicScrollDelegate.getScrollPosition());
                     }, 1000);*/

                    // $scope.heatData[0].topics[0].score=20;
                    // $scope.heatData[0].total_score=200;

                    if ($scope.heatData[0].topics[0] != undefined) {
                        phydata = 1;
                    }
                    if ($scope.heatData[1].topics[0] != undefined) {
                        chedata = 1;
                    }
                    if ($scope.heatData[2].topics[0] != undefined) {
                        matdata = 1;
                    }

                    //////console.log(($scope.heatData[0].score / $scope.heatData[0].total_score) * 100);

                    if ($scope.heatData[0].total_score != 0) {
                        $scope.phypercent = ($scope.heatData[0].score / $scope.heatData[0].total_score) * 100;
                    } else {
                        $scope.phypercent = 0;
                    }

                    if ($scope.heatData[1].total_score != 0) {
                        //$scope.heatData[1].previous_score=8;
                        $scope.chepercent = ($scope.heatData[1].score / $scope.heatData[1].total_score) * 100;
                    } else {
                        $scope.chepercent = 0;
                    }

                    if ($scope.heatData[2].total_score != 0) {
                        $scope.matpercent = ($scope.heatData[2].score / $scope.heatData[2].total_score) * 100;
                    } else {
                        $scope.matpercent = 0;
                    }

                    $scope.p = [];
                    $scope.c = [];
                    $scope.m = [];
                    for (i = 0; i < $scope.heatData[$scope.subject_id].topics.length; i++) {
                        $scope.p[i] = false;
                        $scope.c[i] = false;
                        $scope.m[i] = false;
                    }
                }
            });
        }
        $scope.getHeatMapData();

        $scope.openCheck = function(id) {
            return $scope.opener[id];
        };
        $scope.practClick = function(id, sub) {
            localstorage.set('topic', sub);
            //////console.log($scope.heatData[0].topics[id].topic_name);
            if (sub === 'phy') {
                $location.path('/practiceSet/' + $scope.heatData[0].topics[id].topic_name + '/' + $scope.heatData[0].subject);
                // $location.path('/practice/' + $scope.heatData[0].topics[id].topic_name + '/' + $scope.heatData[0].subject + '/' + '0000/practice');
            } else if (sub === 'che') {
                $location.path('/practiceSet/' + $scope.heatData[1].topics[id].topic_name + '/' + $scope.heatData[1].subject);
                // $location.path('/practice/' + $scope.heatData[1].topics[id].topic_name + '/' + $scope.heatData[1].subject + '/' + '0000/practice');
            } else if (sub === 'mat') {
                $location.path('/practiceSet/' + $scope.heatData[2].topics[id].topic_name + '/' + $scope.heatData[2].subject);
                // $location.path('/practice/' + $scope.heatData[2].topics[id].topic_name + '/' + $scope.heatData[2].subject + '/' + '0000/practice');
            }

        };

        $scope.getOrderStatus = function(win) {
            var i = 0;
            $scope.orderTimer = $interval(function() {
                i++;
                console.log(i);
                PayService.getStatus(616, $scope.stud_id).success(function(successData) {
                    console.log(successData);
                    if (successData.status === "1") {
                        $state.go($state.current, {}, {reload: true});
                    }
                });
            }, 10000)

            $timeout(function() {
                $interval.cancel($scope.orderTimer);
                win.close();
                /*if(data === "0"){
                    $state.go($state.current, {}, {reload: true});
                } */             
            }, 60000 * 10)
        }

        $scope.unlock = function() {
            //$scope.payment = true;
            var openedWindow = window.open("", "_system", "location=yes");
            openedWindow.document.body.innerHTML = "<html><body><p style='color: white;margin: auto;bottom: 30px;position: absolute;top: 0;max-height:55px;font-size: 30px;max-width: 140px;right: 0;left: 0;padding-top: 20px;padding-left: 15px;border-radius: 25px;background-color:rgb(71, 72, 79);'>Loading....</p></body></html>";
            $scope.getOrderStatus(openedWindow);

            $scope.overlay = true;
            var payObj = {};
            payObj.product_id = 616;
            payObj.product_type = "AllFeatures";
            payObj.user_id = $scope.stud_id;
            PayService.setPayment("/apis/create_order.json", payObj).success(function(successData) {
                //console.log(successData);
                if (successData.status === "success") {
                    // $location.path('/pay/' + successData.order_id);
                    var amountObj = {};
                    amountObj.amount = 4499;
                    amountObj.order_id = successData.order_id;
                    PayService.proceedPayment("/apis/Pay", amountObj).success(function(paymentpage) {
                        $scope.overlay = false;
                        console.log(paymentpage);
                        $scope.paynow(openedWindow, paymentpage);

                        /*openedWindow.addEventListener("load", function() {
                            //console.log("received load event");
                            openedWindow.resizeTo(250, 250);
                            
                            //openedWindow.close();
                        }, false);*/
                        //$ionicHistory.goBack();
                    });
                }
            });
        }
        $scope.cancel = function() {
            //$scope.payment = false;
        }
        $scope.paynow = function(windows, page) {

            windows.document.body.innerHTML = page;

        }
        $scope.collapClick = function(id) {
            $scope.opener[id] = !$scope.opener[id];
            if (phydata == 0) {
                $scope.phy = true;
            } else {
                $scope.phy = false;
            }
            if (chedata == 0) {
                $scope.che = true;
            } else {
                $scope.che = false;
            }
            if (matdata == 0) {
                $scope.mat = true;
            } else {
                $scope.mat = false;
            }

        }
        $rootScope.navhider = false;
        $rootScope.doubt = false;
        $rootScope.mock = false;
        $rootScope.practice = true;
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.navClick = function(bol) {


            $rootScope.navhider = !$rootScope.navhider;

            //////console.log($rootScope.navhider);

        };
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };

        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
    })
    .controller('scheduleCtrl', function($scope, $ionicPlatform, HardwareBackButtonManager, TestService, $rootScope, appConfig) {
        $scope.overlay = true;
        $rootScope.navhider = false;
        $rootScope.doubt = true;
        $rootScope.mock = false;
        $rootScope.practice = false;
        HardwareBackButtonManager.enable();
        $scope.hide_nav = function() {
                $rootScope.navhider = false;
            }
            /*$scope.log_out = function() {
                $scope.nav_views = true;
                localstorage.set('logged', false);
            }*/
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.navClick = function(bol) {


            $rootScope.navhider = !$rootScope.navhider;

            //////console.log($rootScope.navhider);

        };
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };

        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
        $ionicPlatform.ready(function() {
            $scope.mySelect = "Select";
            $scope.topicSelect = "Select";
            $('.input-group.date').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "dd/mm/yyyy",
                startDate: "today",
                todayHighlight: true

            });

            $('.clockpicker').clockpicker({
                placement: 'top',
                align: 'left'
            });




            $('body').on('click', '.session-popup', function() {
                var sd = $('.session-date').val();
                var st = $('.session-time').val();
                var s = $('.subject').val();
                var t = $('.topic').val();

                swal({
                        title: "Session on " + sd + " at " + st,
                        text: "For " + s + ', ' + t,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#1ABC9C",
                        cancelButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, schedule it!",
                        cancelButtonText: "No, cancel it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $scope.submitSchedule();
                            swal({
                                title: "Session confirmed!",
                                text: "On " + sd + " at " + st,
                                type: "success",
                                confirmButtonColor: "#1ABC9C"
                            });
                        } else {
                            swal({
                                title: "Session cancelled!",
                                text: "",
                                type: "error",
                                confirmButtonColor: "#DD6B55"
                            });
                        }
                    });
            });


        });
        $scope.sub = [];
        $scope.topic = [];
        $scope.top;
        TestService.getTestQuestions("http://conceptowl.com/apis/get_subject_topics.json").success(function(successData) {
            if (successData !== null) {
                $scope.overlay = false;
                //////console.log(successData);
                for (i = 0; i < successData.length; i++) {
                    $scope.sub[i] = successData[i].subject;
                    $scope.topic[i] = successData[i].topic;
                }
            }
        });
        $scope.subSelect = function(sub) {
            //////console.log(sub);
            for (i = 0; i < $scope.sub.length; i++) {
                if ($scope.sub[i] == sub) {
                    $scope.top = $scope.topic[i];
                    break;
                }
            }
        };

        $scope.submitSchedule = function() {
            if (!$scope.desc)
                des = "null"
            else
                des = $scope.desc;
            var answerJson = {
                "subject_name": $scope.mySelect,
                "topic_name": $scope.topicSelect,
                "description": des,
                "date_time": $('.session-date').val() + " " + $('.session-time').val()
            };
            //////console.log(answerJson);
            TestService.postSchedule("/apis/save_schedule_data.json", answerJson).success(function(successData) {
                if (successData !== null) {
                    //////console.log(successData);
                }

            });
        };
    })
    .controller('askCtrl', function($scope, $ionicPlatform, HardwareBackButtonManager, TestService, $rootScope, appConfig) {

        $rootScope.navhider = false;
        $rootScope.doubt = true;
        $rootScope.mock = false;
        $rootScope.practice = false;
        HardwareBackButtonManager.enable();
        $scope.hide_nav = function() {
                $rootScope.navhider = false;
            }
            /*$scope.log_out = function() {
                $scope.nav_views = true;
                localstorage.set('logged', false);
            }*/
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.navClick = function(bol) {


            $rootScope.navhider = !$rootScope.navhider;

            //////console.log($rootScope.navhider);

        };
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };

        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
        $scope.submitQues = function() {
            if (!$scope.ques)
                ques = "null"
            else
                ques = $scope.ques;
            var answerJson = {

                "description": ques
            };
            //////console.log(answerJson);

            TestService.postTestQ("/apis/save_question_data.json?description=" + $scope.ques).success(function(successData) {
                if (successData !== null) {
                    //////console.log(successData);
                }
            });

        };
    })
    .controller('testCtrl', function($scope, $ionicHistory, directiveService, HardwareBackButtonManager, $ionicSideMenuDelegate, $interval, $location, $stateParams, $sce, $http, TestService, $ionicLoading, localstorage, $rootScope, $timeout, $ionicScrollDelegate, $ionicPopup, $state, appConfig) {
        $scope.dataFound = false;
        $ionicHistory.nextViewOptions({
            disableAnimate: true
        });
        $scope.loaderTimer = $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 10000);
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $ionicHistory.goBack();
        }

        $ionicHistory.nextViewOptions({
            disableAnimate: true
        });


        $scope.Booked = localstorage.get("bookmark");


        $scope.topic_name = $stateParams.name;
        $scope.topic_subject = $stateParams.sub;
        $scope.paperid = $stateParams.paper;
        $scope.test_type = $stateParams.type;
        $scope.subjective_sol = false;
        //////console.log($scope.test_type);

        $scope.hintForm = false;
        $scope.no_solution = true;
        HardwareBackButtonManager.disable();
        $scope.stud_id = localstorage.get('stud_id');

        $scope.subj_solu = function() {
            $scope.subjective_sol = !$scope.subjective_sol;
        }

        $scope.close_side_bar = function() {
            $scope.sidebarCollapse = true;
        }
        $scope.show_side = function() {
            $scope.sidebarCollapse = false;
        }

        $scope.ObjectLength = function(object) {
            var length = 0;
            for (var key in object) {
                if (object.hasOwnProperty(key)) {
                    ++length;
                }
            }
            return length;
        };

        $scope.paper_id = 0;
        $scope.answered = 0;
        $scope.skippedQues = 0;
        $scope.counts = 0;
        $scope.matrix_counts = 0;

        $scope.showTestButton = {
            'Physics': false,
            'Chemistry': false,
            'Maths': false
        };
        // Logic to identify the test
        if ($scope.topic_subject === 'PHYSICS') {
            $scope.currentView = 0;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Maths = true;
        } else if ($scope.topic_subject === 'physics') {
            $scope.currentView = 0;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Maths = true;
        } else if ($scope.topic_subject === 'CHEMISTRY') {
            $scope.currentView = 1;
            $scope.showTestButton.Physics = $scope.showTestButton.Maths = true;
        } else if ($scope.topic_subject === 'chemistry') {
            $scope.currentView = 1;
            $scope.showTestButton.Physics = $scope.showTestButton.Maths = true;
        } else if ($scope.topic_subject === 'MATH') {
            $scope.currentView = 2;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Physics = true;
        } else if ($scope.topic_subject === 'math') {
            $scope.currentView = 2;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Physics = true;
        }

        $scope.overlay = true;

        MathJax.Hub.Config({
            skipStartupTypeset: true,
            messageStyle: "none",
            "HTML-CSS": {
                showMathMenu: false
            }
        });
        MathJax.Hub.Configured();

        $scope.getNumber = function(num) {
            return new Array(num);
        }

        $scope.timeInSeconds = 0;;
        $scope.readableTime = {
            hours: 0,
            minutes: 0,
            seconds: 0
        };
        // show 0:0 as 00:00
        $scope.addZero = function(i) {
            if (i < 10) {
                i = "0" + i
            };
            return i;
        }
        $scope.startTimer = function() {
            $scope.testTimer = $interval(function() {
                $scope.timeInSeconds++;
                $scope.readableTime.seconds = $scope.addZero($scope.timeInSeconds % 60);
                $scope.readableTime.minutes = $scope.addZero(Math.floor(($scope.timeInSeconds / 60) % 60));
                $scope.readableTime.hours = $scope.addZero(Math.floor($scope.timeInSeconds / 3600));
            }, 1000);
            //  $scope.timeToEndTest = parseInt($stateParams.time) || 0;
            // End test after specified time 
            // $timeout(function() {
            //     $scope.readableTime.seconds = 00;
            //     $scope.readableTime.minutes = 00;
            //     $scope.readableTime.hours = 00;
            //     $interval.cancel($scope.testTimer);
            //     $interval.cancel($scope.questionTimer);
            //     if ($scope.groupedQuestions.length != 0) {
            //         $scope.showConfirm();
            //     }

            // }, $scope.timeToEndTest);
        };
        // Capture the time take for each question
        $scope.changeQuestionTimer = function() {
            if ($scope.questionTimer) {
                $interval.cancel($scope.questionTimer);
            }
            $scope.questionTimer = $interval(function() {
                $scope.groupedQuestions[$scope.currentQuestion].time++;
            }, 1000);
        };

        $scope.getQuestions = function() {
            if ($scope.test_type === 'practice') {
                $scope.Topic = localstorage.get("topic");
                if ($scope.Topic === "") {
                    $scope.Topic = $stateParams.name;
                }
                console.log('practice', $scope.paperid, $scope.topic_name)
                TestService.getTestQuestions("http://conceptowl.com/apis/practice_test_details.json?paperid=" + $scope.paperid + "&testid=" + $scope.topic_name).success(function(successData) {
                        if (successData !== null) {
                            $timeout.cancel($scope.loaderTimer);
                            $scope.dataFound = true;
                            $scope.overlay = false;
                            console.log(successData);
                            $scope.paper_id = successData.paper_id;
                            $scope.groupedQuestions = successData.questions;
                            if ($scope.groupedQuestions.length == 0) {
                                $scope.showEndpop();
                            }
                            $scope.timeToEndTest = $scope.groupedQuestions.length * 2 * 1000 * 60;

                            $scope.startTimer();
                            $scope.addAnswerField();
                            $scope.currentQuestion = 0;
                            $scope.changeQuestionTimer();
                            //////console.log($scope.groupedQuestions);
                            for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                                if ($scope.groupedQuestions[i].hint === "" && $scope.groupedQuestions[i].hint_2 === "" && $scope.groupedQuestions[i].hint_3 === "") {
                                    $scope.groupedQuestions[i].no_hint = true;
                                } else {
                                    $scope.groupedQuestions[i].no_hint = false;
                                }
                                if ($scope.groupedQuestions[i].review === true) {
                                    $scope.answered = $scope.answered + 1;
                                    $scope.processAnswers(i);
                                }
                                console.log($scope.groupedQuestions[i].type, i);
                            };
                            $scope.groupedQuestionsFilter = $scope.groupedQuestions;


                            //  $scope.chan$scope.groupedQuestionsgeQuestionTimer();
                        }
                    })
                    .error(function(e) {
                        //////console.log(e);
                    })
            } else {
                $scope.Topic = $stateParams.name;
                TestService.getTestQuestions("http://conceptowl.com/apis/practice_session_retake.json?paperid=" + $scope.paperid + "&studentid=" + $scope.stud_id).success(function(successData) {
                        if (successData !== null) {
                            $timeout.cancel($scope.loaderTimer);
                            $scope.dataFound = true;
                            $scope.overlay = false;
                            console.log(successData);
                            $scope.paper_id = successData.paper_id;
                            $scope.groupedQuestions = successData.questions;
                            //  //////console.log($scope.groupedQuestions);
                            if ($scope.groupedQuestions.length == 0) {
                                $scope.showEndpop();
                            }
                            // $scope.timeInSeconds = $scope.groupedQuestions.length * 2 * 60;
                            $scope.timeToEndTest = $scope.groupedQuestions.length * 2 * 1000 * 60;

                            $scope.startTimer();
                            $scope.addAnswerField();
                            $scope.currentQuestion = 0;
                            $scope.changeQuestionTimer();
                            //////console.log($scope.groupedQuestions);
                            for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                                if ($scope.groupedQuestions[i].hint === "" && $scope.groupedQuestions[i].hint_2 === "" && $scope.groupedQuestions[i].hint_3 === "") {
                                    $scope.groupedQuestions[i].no_hint = true;
                                } else {
                                    $scope.groupedQuestions[i].no_hint = false;
                                }
                                /*if ($scope.groupedQuestions[i].solution == "") {
                                    $scope.groupedQuestions[i].solution = "No Solution Available!!";
                                }*/
                                if ($scope.groupedQuestions[i].type == 'multiple_correct_choice_type') {
                                    //////console.log("multiple");
                                } else {
                                    //////console.log("single");
                                }

                            };
                            $scope.groupedQuestionsFilter = $scope.groupedQuestions;
                            //  $scope.chan$scope.groupedQuestionsgeQuestionTimer();
                        }
                    })
                    .error(function(e) {
                        //////console.log(e);
                    })
            }

        };
        $scope.getQuestions();
        $scope.processFilterQuestions = function() {

            if ($scope.groupedQuestions.length == 0) {
                $scope.question_space1 = true;
                $scope.question_space = true;
                $interval.cancel($scope.questionTimer);
            } else {
                $scope.question_space1 = false;
                $scope.question_space = false;
                $scope.currentQuestion = 0;
                $scope.changeQuestionTimer();
            }

        }
        $scope.processAnswers = function(quesnum) {
            for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                if ($scope.groupedQuestions[quesnum].type === 'single_correct_choice_type') {
                    if (parseInt($scope.groupedQuestions[quesnum].submitted_answer[0].opt) === $scope.groupedQuestions[quesnum].answer[0].opt_id) {
                        $scope.groupedQuestions[quesnum].correctAns = true;
                        for (var k = 0; k < $scope.groupedQuestions[quesnum].options.length; k++) {
                            if ($scope.groupedQuestions[quesnum].options[k].opt_id === $scope.groupedQuestions[quesnum].answer[0].opt_id) {
                                $scope.groupedQuestions[quesnum].options[k].correct = true;
                            }
                        };
                    } else {
                        $scope.groupedQuestions[quesnum].incorrectAns = true;
                        for (var k = 0; k < $scope.groupedQuestions[quesnum].options.length; k++) {
                            if ($scope.groupedQuestions[quesnum].options[k].opt_id === parseInt($scope.groupedQuestions[quesnum].submitted_answer[0].opt)) {
                                $scope.groupedQuestions[quesnum].options[k].incorrect = true;
                                //$scope.groupedQuestions[quesnum].incorrectAnswer = $scope.groupedQuestions[quesnum].incorrectAnswer + 1;
                            }
                            if ($scope.groupedQuestions[quesnum].options[k].opt_id === $scope.groupedQuestions[quesnum].answer[0].opt_id) {
                                $scope.groupedQuestions[quesnum].options[k].skipped = true;
                            }
                        };
                    }
                } else if ($scope.groupedQuestions[quesnum].type === 'multiple_correct_choice_type') {
                    $scope.correct_count = 0;
                    $scope.incorrect_count = 0;
                    if ($scope.groupedQuestions[quesnum].submitted_answer.length === $scope.groupedQuestions[quesnum].answer.length) {
                        var multiple_correct = 0;
                        for (var l = 0; l < $scope.groupedQuestions[quesnum].submitted_answer.length; l++) {
                            var tmp = 0;
                            for (var m = 0; m < $scope.groupedQuestions[quesnum].answer.length; m++) {
                                if ($scope.groupedQuestions[quesnum].submitted_answer[l].opt === $scope.groupedQuestions[quesnum].answer[m].opt_id.toString()) {
                                    multiple_correct++;
                                    tmp++;
                                    for (var k = 0; k < $scope.groupedQuestions[quesnum].options.length; k++) {
                                        if ($scope.groupedQuestions[quesnum].options[k].opt_id.toString() === $scope.groupedQuestions[quesnum].submitted_answer[l].opt) {
                                            $scope.groupedQuestions[quesnum].options[k].correct = true;
                                            break;
                                        }
                                    };
                                }

                            };
                            if (tmp === 0) {
                                for (var k = 0; k < $scope.groupedQuestions[quesnum].options.length; k++) {
                                    if ($scope.groupedQuestions[quesnum].options[k].opt_id.toString() === $scope.groupedQuestions[quesnum].submitted_answer[l].opt) {
                                        $scope.groupedQuestions[quesnum].options[k].incorrect = true;
                                        // ////console.log("Incorrect");
                                    }
                                };
                            }

                        }
                        if (multiple_correct === $scope.groupedQuestions[quesnum].answer.length) {
                            $scope.groupedQuestions[quesnum].correctAns = true;
                            //$scope.groupedQuestions[quesnum].correctAnswer = $scope.groupedQuestions[quesnum].correctAnswer + 1;
                        } else {
                            $scope.groupedQuestions[quesnum].incorrectAns = true;
                            //$scope.groupedQuestions[quesnum].incorrectAnswer = $scope.groupedQuestions[quesnum].incorrectAnswer + 1;
                        }
                        for (var x = 0; x < $scope.groupedQuestions[quesnum].answer.length; x++) {
                            for (var y = 0; y < $scope.groupedQuestions[quesnum].options.length; y++) {
                                if ($scope.groupedQuestions[quesnum].options[y].opt_id == $scope.groupedQuestions[quesnum].answer[x].opt_id) {
                                    $scope.groupedQuestions[quesnum].options[y].skipped = true;
                                }
                            };
                        };
                    } else if ($scope.groupedQuestions[quesnum].submitted_answer.length < $scope.groupedQuestions[quesnum].answer.length) {
                        //////console.log("less answers");
                        $scope.groupedQuestions[quesnum].incorrectAns = true;
                        var tmp = false;
                        // var diff=$scope.groupedQuestions[quesnum].submitted_answer.length - $scope.groupedQuestions[quesnum].answer.length;
                        //$scope.groupedQuestions[quesnum].incorrectAnswer = $scope.groupedQuestions[quesnum].incorrectAnswer + 1;
                        for (var l = 0; l < $scope.groupedQuestions[quesnum].submitted_answer.length; l++) {
                            for (var m = 0; m < $scope.groupedQuestions[quesnum].answer.length; m++) {
                                if ($scope.groupedQuestions[quesnum].submitted_answer[l].opt == $scope.groupedQuestions[quesnum].answer[m].opt_id) {
                                    tmp = true;
                                    for (var k = 0; k < $scope.groupedQuestions[quesnum].options.length; k++) {
                                        if ($scope.groupedQuestions[quesnum].options[k].opt_id == $scope.groupedQuestions[quesnum].submitted_answer[l].opt) {
                                            $scope.groupedQuestions[quesnum].options[k].correct = true;
                                        }
                                    };
                                }
                            };
                            if (tmp == false) {
                                for (var k = 0; k < $scope.groupedQuestions[quesnum].options.length; k++) {
                                    if (tmp == 0) {
                                        if ($scope.groupedQuestions[quesnum].options[k].opt_id == $scope.groupedQuestions[quesnum].submitted_answer[l].opt) {
                                            $scope.groupedQuestions[quesnum].options[k].incorrect = true;
                                        }
                                    }
                                };
                            }
                            tmp = false;
                        };
                        for (var m = 0; m < $scope.groupedQuestions[quesnum].answer.length; m++) {
                            for (var l = 0; l < $scope.groupedQuestions[quesnum].submitted_answer.length; l++) {
                                if ($scope.groupedQuestions[quesnum].submitted_answer[l].opt != $scope.groupedQuestions[quesnum].answer[m].opt_id) {
                                    for (var k = 0; k < $scope.groupedQuestions[quesnum].options.length; k++) {
                                        if ($scope.groupedQuestions[quesnum].options[k].opt_id == $scope.groupedQuestions[quesnum].answer[m].opt_id) {
                                            $scope.groupedQuestions[quesnum].options[k].skipped = true;
                                        }
                                    };
                                }
                            };
                        };
                    } else if ($scope.groupedQuestions[quesnum].submitted_answer.length > $scope.groupedQuestions[quesnum].answer.length) {
                        //////console.log("more answers");
                        $scope.groupedQuestions[quesnum].incorrectAns = true;
                        var tmp = false;
                        // var diff=$scope.groupedQuestions[quesnum].submitted_answer.length - $scope.groupedQuestions[quesnum].answer.length;
                        // $scope.groupedQuestions[quesnum].incorrectAnswer = $scope.groupedQuestions[quesnum].incorrectAnswer + 1;
                        for (var l = 0; l < $scope.groupedQuestions[quesnum].submitted_answer.length; l++) {
                            for (var m = 0; m < $scope.groupedQuestions[quesnum].answer.length; m++) {
                                if ($scope.groupedQuestions[quesnum].submitted_answer[l].opt == $scope.groupedQuestions[quesnum].answer[m].opt_id) {
                                    tmp = true;
                                    for (var k = 0; k < $scope.groupedQuestions[quesnum].options.length; k++) {
                                        if ($scope.groupedQuestions[quesnum].options[k].opt_id == $scope.groupedQuestions[quesnum].submitted_answer[l].opt) {
                                            $scope.groupedQuestions[quesnum].options[k].correct = true;
                                        }
                                    };
                                }
                            };
                            if (tmp == false) {
                                for (var k = 0; k < $scope.groupedQuestions[quesnum].options.length; k++) {
                                    if (tmp == 0) {
                                        if ($scope.groupedQuestions[quesnum].options[k].opt_id == $scope.groupedQuestions[quesnum].submitted_answer[l].opt) {
                                            $scope.groupedQuestions[quesnum].options[k].incorrect = true;
                                        }
                                    }
                                };
                            }
                            tmp = false;
                        };
                    }
                } else if ($scope.groupedQuestions[quesnum].type === 'integer_type') {
                    //if ($scope.groupedQuestions[i].answer_type === "attempted") {
                    if (parseInt($scope.groupedQuestions[quesnum].integer_answer) === parseInt($scope.groupedQuestions[quesnum].submitted_answer[0].opt)) {
                        $scope.groupedQuestions[quesnum].correctAns = true;
                        //$scope.groupedQuestions[i].correctAnswer = $scope.groupedQuestions[i].correctAnswer + 1;
                    } else {
                        $scope.groupedQuestions[quesnum].incorrectAns = true;
                        //$scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                    }
                    /*} else if ($scope.groupedQuestions[i].answer_type === "skipped") {
                        $scope.groupedQuestions[i].skipped = true;
                        $scope.groupedQuestions[i].skippedAnswer = $scope.groupedQuestions[i].skippedAnswer + 1;
                    }*/
                } else if ($scope.groupedQuestions[quesnum].type === 'matrix_type') {
                    if ($scope.groupedQuestions[quesnum].answer_type === 'attempted') {
                        for (var m = 0; m < $scope.ObjectLength($scope.groupedQuestions[quesnum].submitted_answer); m++) {
                            if ($scope.groupedQuestions[quesnum].submitted_answer[m] !== null) {
                                for (var k = 0; k < $scope.groupedQuestions[quesnum].submitted_answer[m].length; k++) {
                                    for (var l = 0; l < $scope.groupedQuestions[quesnum].matrix_option_params.length; l++) {
                                        if ($scope.groupedQuestions[quesnum].submitted_answer[m][k] === $scope.groupedQuestions[quesnum].matrix_option_params[l]) {
                                            $scope.groupedQuestions[quesnum].matrixarray[m][l] = true;
                                        }
                                    };
                                };
                            }
                        };
                        for (var w = 0; w < $scope.groupedQuestions[quesnum].answer.length; w++) {
                            for (var x = 0; x < $scope.groupedQuestions[quesnum].options.length; x++) {
                                if ($scope.groupedQuestions[quesnum].answer[w].opt_id === $scope.groupedQuestions[quesnum].options[x].opt_id) {
                                    var options = $scope.groupedQuestions[quesnum].options[x].opt.split(',');
                                    for (var y = 0; y < options.length; y++) {
                                        for (var z = 0; z < $scope.groupedQuestions[quesnum].matrix_option_params.length; z++) {
                                            if (options[y] === $scope.groupedQuestions[quesnum].matrix_option_params[z]) {
                                                $scope.groupedQuestions[quesnum].ans_matrixarray[w][z] = true;
                                            }
                                        };
                                    }
                                }
                            };
                        };
                        var correct = 0;
                        for (var k = 0; k < $scope.groupedQuestions[quesnum].matrix_answer_params.length; k++) {
                            for (var l = 0; l < $scope.groupedQuestions[quesnum].matrix_option_params.length; l++) {
                                if ($scope.groupedQuestions[quesnum].ans_matrixarray[k][l] !== $scope.groupedQuestions[quesnum].matrixarray[k][l]) {
                                    correct = 1;
                                }
                            };
                        };
                        if (correct === 0) {
                            $scope.groupedQuestions[quesnum].correctAns = true;
                            //$scope.groupedQuestions[quesnum].correctAnswer = $scope.groupedQuestions[quesnum].correctAnswer + 1;
                        } else {
                            $scope.groupedQuestions[quesnum].incorrectAns = true;
                            //$scope.groupedQuestions[quesnum].incorrectAnswer = $scope.groupedQuestions[quesnum].incorrectAnswer + 1;
                        }
                    } else if ($scope.groupedQuestions[quesnum].answer_type === 'skipped') {
                        $scope.groupedQuestions[quesnum].skipped = true;
                        $scope.groupedQuestions[quesnum].skippedAnswer = $scope.groupedQuestions[quesnum].skippedAnswer + 1;
                        for (var w = 0; w < $scope.groupedQuestions[quesnum].answer.length; w++) {
                            //  ////console.log($scope.groupedQuestions[quesnum].answer);
                            for (var x = 0; x < $scope.groupedQuestions[quesnum].options.length; x++) {
                                if ($scope.groupedQuestions[quesnum].answer[w].opt === $scope.groupedQuestions[quesnum].options[x].opt_id) {
                                    var options = $scope.groupedQuestions[quesnum].options[x].opt.split(',');
                                    ////console.log(options);
                                    for (var y = 0; y < options.length; y++) {
                                        for (var z = 0; z < $scope.groupedQuestions[quesnum].matrix_option_params.length; z++) {
                                            if (options[y] === $scope.groupedQuestions[quesnum].matrix_option_params[z]) {
                                                $scope.groupedQuestions[quesnum].ans_matrixarray[w][z] = true;
                                            }
                                        };
                                    }
                                }
                            };
                        };
                    }
                }

            };
        }
        $scope.addAnswerField = function() {
            for (var i = 0; i < $scope.groupedQuestions.length; i++) {

                $scope.groupedQuestions[i].submitted = false;
                $scope.groupedQuestions[i].attempted = false;
                if ($scope.Booked === "true") {
                    $scope.groupedQuestions[i].bookmarked = true;
                } else {
                    $scope.groupedQuestions[i].bookmarked = false;
                }
                $scope.groupedQuestions[i].time = 0;
                $scope.groupedQuestions[i].skipped = false;
                $scope.groupedQuestions[i].submittedAns = [];
                $scope.groupedQuestions[i].correctAns = false;
                $scope.groupedQuestions[i].incorrectAns = false;
                $scope.groupedQuestions[i].integer_choice = null;

                $scope.groupedQuestions[i].time = 0;
                for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                    $scope.groupedQuestions[i].options[j].chosen = true;
                    $scope.groupedQuestions[i].options[j].correct = false;
                    $scope.groupedQuestions[i].options[j].incorrect = false;
                }

                if ($scope.groupedQuestions[i].type === "matrix_type") {
                    if ($scope.groupedQuestions[i].matrix_answer_params !== null && $scope.groupedQuestions[i].matrix_option_params !== null) {
                        $scope.quest_options = $scope.groupedQuestions[i].matrix_answer_params;
                        $scope.answer_options = $scope.groupedQuestions[i].matrix_option_params;

                        //console.log($scope.groupedQuestions[i].matrix_answer_params +' '+$scope.groupedQuestions[i].matrix_option_params);

                        $scope.groupedQuestions[i].ans_matrixarray = new Array($scope.quest_options.length)
                        for (j = 0; j < $scope.quest_options.length; j++)
                            $scope.groupedQuestions[i].ans_matrixarray[j] = new Array($scope.answer_options.length)

                        for (var j = 0; j < $scope.quest_options.length; j++) {
                            for (var k = 0; k < $scope.answer_options.length; k++) {
                                $scope.groupedQuestions[i].ans_matrixarray[j][k] = false;
                            };
                        };

                        $scope.groupedQuestions[i].matrixarray = new Array($scope.quest_options.length)
                            //var matrixarray = new Array($scope.quest_options.length)
                        for (j = 0; j < $scope.quest_options.length; j++)
                            $scope.groupedQuestions[i].matrixarray[j] = new Array($scope.answer_options.length)

                        for (var j = 0; j < $scope.quest_options.length; j++) {
                            for (var k = 0; k < $scope.answer_options.length; k++) {
                                $scope.groupedQuestions[i].matrixarray[j][k] = false;
                            };
                        };
                    }

                }
                if ($scope.groupedQuestions[i].answer_type === "attempted") {
                    $scope.groupedQuestions[i].submitted = true;
                    $scope.groupedQuestions[i].review = true;
                } else {
                    $scope.groupedQuestions[i].submitted = false;
                    $scope.groupedQuestions[i].review = false;
                }
                // //////console.log($scope.groupedQuestions);
            };
        }

        $scope.showNextQuestion = function() {

            if ($scope.groupedQuestions[$scope.currentQuestion].review === false) {
                if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                    if ($scope.groupedQuestions[$scope.currentQuestion].submitted) {
                        // $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                    } else {
                        if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                            $scope.skippedQues = $scope.skippedQues + 1;
                        }
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                        $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                        for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                            for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                                $scope.groupedQuestions[i].options[j].chosen = true;
                            }
                        };
                    }
                } else {
                    if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                        $scope.skippedQues = $scope.skippedQues + 1;
                    }
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                }
            }

            // Check if the question has reached the end
            if ($scope.currentQuestion === $scope.groupedQuestions.length - 1) {
                $scope.QuestionsEnd();
            } else {
                // change question
                $scope.changeQuestionForward($scope.currentQuestion + 1);
            }
        };
        $scope.showPrevQuestion = function() {

            if ($scope.groupedQuestions[$scope.currentQuestion].review === false) {
                if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                    if ($scope.groupedQuestions[$scope.currentQuestion].submitted) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                    } else {
                        if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                            $scope.skippedQues = $scope.skippedQues + 1;
                        }
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                        $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                        for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                            for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                                $scope.groupedQuestions[i].options[j].chosen = true;
                            }
                        };
                    }
                } else {
                    if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                        $scope.skippedQues = $scope.skippedQues + 1;
                    }
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                }
            }

            // Check if the question has reached the first question , then switch the subject
            if ($scope.currentQuestion === 0) {
                $scope.changeQuestionBackward($scope.groupedQuestions.length - 1)
            } else {
                // change question
                $scope.changeQuestionBackward($scope.currentQuestion - 1);
            }
        };


        $scope.changeQuestion = function(num) {
            $scope.subjective_sol = false;
            $scope.single = -1;
            $scope.counts = 0;
            $scope.matrix_counts = 0;
            if (num != -1) {
                //////console.log(num)
                //  $scope.currentQuestion = num;
                $scope.top = (num - 7) * 50;
                $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                //  $scope.changeQuestionTimer();
                $scope.hintForm = false;
                if ($scope.groupedQuestions[$scope.currentQuestion].review === false) {
                    if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                        if ($scope.groupedQuestions[$scope.currentQuestion].submitted) {
                            $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                        } else {
                            if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                                $scope.skippedQues = $scope.skippedQues + 1;
                            }
                            $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                            $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                            for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                                for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                                    $scope.groupedQuestions[i].options[j].chosen = true;
                                }
                            };
                        }
                    } else {
                        if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                            $scope.skippedQues = $scope.skippedQues + 1;
                        }
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                    }
                }

                //////console.log(num)
                $scope.currentQuestion = num;
                $scope.integer_ans = $scope.groupedQuestions[$scope.currentQuestion].integer_choice;
                $scope.changeQuestionTimer();
                // //////console.log($scope.groupedQuestions);
            }
        }
        $scope.changeQuestionForward = function(num) {
            $scope.single = -1;
            $scope.counts = 0;
            $scope.matrix_counts = 0;
            $scope.subjective_sol = false;
            if (num != -1) {
                //////console.log(num)
                //  $scope.currentQuestion = num;
                if ($scope.currentQuestion > 7) {
                    $scope.top += 50;
                    $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                } else {
                    $scope.top = 0;
                }
                $scope.hintForm = false;
                /*if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                    if ($scope.groupedQuestions[$scope.currentQuestion].submitted) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                    } else {
                        if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                            $scope.skippedQues = $scope.skippedQues + 1;
                        }
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                        $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                        for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                            for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                                $scope.groupedQuestions[i].options[j].chosen = true;
                            }
                        };
                    }
                } else {
                    if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                        $scope.skippedQues = $scope.skippedQues + 1;
                    }
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                }*/

                if ($scope.currentQuestion === $scope.groupedQuestions.length - 1) {
                    $scope.currentQuestion = $scope.groupedQuestions.length - 1;
                    $scope.QuestionsEnd();
                } else {
                    //////console.log(num)
                    $scope.currentQuestion = num;
                    $scope.integer_ans = $scope.groupedQuestions[$scope.currentQuestion].integer_choice;
                    $scope.changeQuestionTimer();
                }

                // //////console.log($scope.groupedQuestions);
            }
        }
        $scope.changeQuestionBackward = function(num) {
            $scope.single = -1;
            $scope.counts = 0;
            $scope.matrix_counts = 0;
            $scope.subjective_sol = false;
            if (num != -1) {
                //////console.log(num)
                //  $scope.currentQuestion = num;
                if ($scope.currentQuestion != 0) {
                    $scope.top -= 50;
                    $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                } else {
                    $scope.top = 0;
                }
                $scope.hintForm = false;
                /* if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                     if ($scope.groupedQuestions[$scope.currentQuestion].submitted) {
                         $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                     } else {
                         if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                             $scope.skippedQues = $scope.skippedQues + 1;
                         }
                         $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                         $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                         for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                             for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                                 $scope.groupedQuestions[i].options[j].chosen = true;
                             }
                         };
                     }
                 } else {
                     if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                         $scope.skippedQues = $scope.skippedQues + 1;
                     }
                     $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                 }*/
                //////console.log(num)
                $scope.currentQuestion = num;
                $scope.integer_ans = $scope.groupedQuestions[$scope.currentQuestion].integer_choice;
                $scope.changeQuestionTimer();
                // //////console.log($scope.groupedQuestions);
            }
        }

        $scope.markAnswer = function(quesNo, optionIndex) {

            //////console.log(optionIndex);

            if ($scope.groupedQuestions[quesNo].submitted == false) {
                if ($scope.groupedQuestions[quesNo].type === 'single_correct_choice_type') {
                    console.log($scope.single, optionIndex);
                    if ($scope.single == optionIndex) {
                        $scope.groupedQuestions[quesNo].options[optionIndex].chosen = !$scope.groupedQuestions[quesNo].options[optionIndex].chosen;;
                        $scope.groupedQuestions[quesNo].attempted = !$scope.groupedQuestions[quesNo].attempted;
                    } else {
                        $scope.single = optionIndex;
                        $scope.groupedQuestions[quesNo].options[optionIndex].chosen = false;
                        $scope.groupedQuestions[quesNo].attempted = true;
                        for (var k = 0; k < $scope.groupedQuestions[quesNo].options.length; k++) {
                            if (k !== optionIndex) {
                                $scope.groupedQuestions[quesNo].options[k].chosen = true;
                            }
                        };
                    }
                }
                if ($scope.groupedQuestions[quesNo].type === 'multiple_correct_choice_type') {

                    if ($scope.groupedQuestions[quesNo].options[optionIndex].chosen) {
                        $scope.counts++;
                        //console.log('first', $scope.counts);
                        $scope.groupedQuestions[quesNo].options[optionIndex].chosen = false;
                        $scope.groupedQuestions[quesNo].attempted = true;
                    } else {
                        $scope.counts--;
                        //console.log('second', $scope.counts);
                        $scope.groupedQuestions[quesNo].options[optionIndex].chosen = true;
                        if ($scope.counts == 0) {
                            $scope.groupedQuestions[quesNo].attempted = false;
                        }
                    }

                }

            }

        }
        $scope.showSubmitted = function() {

            var correct_test = 0;
            var incorrect_test = 0;

            $scope.groupedQuestions[$scope.currentQuestion].submitted = true;
            if ($scope.groupedQuestions[$scope.currentQuestion].solution == "No Solution Available!!") {
                $scope.no_solution = false;
            } else {
                $scope.no_solution = true;
            }
            if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                $scope.answered = $scope.answered + 1;
            }

            //////console.log($scope.groupedQuestions);
            if (($scope.groupedQuestions[$scope.currentQuestion].skipped == true) && ($scope.groupedQuestions[$scope.currentQuestion].attempted)) {
                $scope.skippedQues = $scope.skippedQues - 1;
            }
            if ($scope.groupedQuestions[$scope.currentQuestion].type === 'single_correct_choice_type') {
                for (var i = 0; i < $scope.groupedQuestions[$scope.currentQuestion].options.length; i++) {
                    if (!$scope.groupedQuestions[$scope.currentQuestion].options[i].chosen) {
                        $scope.groupedQuestions[$scope.currentQuestion].submittedAns.push($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id);
                        if ($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id == $scope.groupedQuestions[$scope.currentQuestion].answer[0].opt_id) {
                            $scope.groupedQuestions[$scope.currentQuestion].options[i].correct = true;
                            $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                            $scope.groupedQuestions[$scope.currentQuestion].correctAns = true;
                        } else {
                            $scope.groupedQuestions[$scope.currentQuestion].options[i].incorrect = true;
                            $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                            $scope.groupedQuestions[$scope.currentQuestion].incorrectAns = true;
                        }
                    } else {
                        if ($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id == $scope.groupedQuestions[$scope.currentQuestion].answer[0].opt_id) {
                            $scope.groupedQuestions[$scope.currentQuestion].options[i].skipped = true;
                        }
                    }
                };
            } else if ($scope.groupedQuestions[$scope.currentQuestion].type === 'multiple_correct_choice_type') {

                if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                    var tmp = false;
                    for (var i = 0; i < $scope.groupedQuestions[$scope.currentQuestion].options.length; i++) {
                        if (!$scope.groupedQuestions[$scope.currentQuestion].options[i].chosen) {
                            $scope.groupedQuestions[$scope.currentQuestion].submittedAns.push($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id);
                            for (var j = 0; j < $scope.groupedQuestions[$scope.currentQuestion].answer.length; j++) {
                                if ($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id == $scope.groupedQuestions[$scope.currentQuestion].answer[j].opt_id) {
                                    $scope.groupedQuestions[$scope.currentQuestion].options[i].correct = true;
                                    correct_test++;
                                    tmp = true;
                                    break;
                                } else {
                                    tmp = false;
                                }
                            };
                            if (tmp == false) {
                                incorrect_test++;
                                $scope.groupedQuestions[$scope.currentQuestion].options[i].incorrect = true;
                            }
                        } else {
                            //////console.log("Not attempted");
                            for (var j = 0; j < $scope.groupedQuestions[$scope.currentQuestion].answer.length; j++) {
                                if ($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id == $scope.groupedQuestions[$scope.currentQuestion].answer[j].opt_id) {
                                    $scope.groupedQuestions[$scope.currentQuestion].options[i].skipped = true;
                                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                                }
                            }
                        }
                    };
                    if (correct_test == $scope.groupedQuestions[$scope.currentQuestion].answer.length) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                        $scope.groupedQuestions[$scope.currentQuestion].correctAns = true;
                    }
                    if (correct_test < $scope.groupedQuestions[$scope.currentQuestion].answer.length) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                        $scope.groupedQuestions[$scope.currentQuestion].incorrectAns = true;
                    }
                    if (incorrect_test > 0) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                        $scope.groupedQuestions[$scope.currentQuestion].incorrectAns = true;
                    }
                } else {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                    $scope.skippedQues = $scope.skippedQues + 1;
                    for (var i = 0; i < $scope.groupedQuestions[$scope.currentQuestion].answer.length; i++) {
                        for (var j = 0; j < $scope.groupedQuestions[$scope.currentQuestion].options.length; j++) {
                            if ($scope.groupedQuestions[$scope.currentQuestion].options[j].opt_id == $scope.groupedQuestions[$scope.currentQuestion].answer[i].opt_id) {
                                $scope.groupedQuestions[$scope.currentQuestion].options[j].skipped = true;
                                $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                            }
                        };

                    }
                }
            } else if ($scope.groupedQuestions[$scope.currentQuestion].type === 'integer_type') {
                //console.log($scope.integer_ans);
                $scope.groupedQuestions[$scope.currentQuestion].submittedAns.push($scope.integer_ans);
                if ($scope.integer_ans !== undefined && $scope.integer_ans !== null) {
                    $scope.answered = $scope.answered + 1;
                    $scope.groupedQuestions[$scope.currentQuestion].integer_choice = $scope.integer_ans;
                    $scope.groupedQuestions[$scope.currentQuestion].attempted = true;
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = false;

                    if (parseInt($scope.groupedQuestions[$scope.currentQuestion].integer_choice) === parseInt($scope.groupedQuestions[$scope.currentQuestion].integer_answer)) {
                        $scope.groupedQuestions[$scope.currentQuestion].correctAns = true;
                    } else {
                        $scope.groupedQuestions[$scope.currentQuestion].incorrectAns = true;
                    }
                } else {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                    $scope.skippedQues = $scope.skippedQues + 1;
                }
                // $scope.groupedQuestions[$scope.currentQuestion].integer_answer = parseInt($scope.groupedQuestions[$scope.currentQuestion].integer_answer)
            } else if ($scope.groupedQuestions[$scope.currentQuestion].type === 'matrix_type') {
                //////console.log($scope.groupedQuestions[$scope.currentQuestion].matrixarray);
                if ($scope.groupedQuestions[$scope.currentQuestion].attempted && $scope.groupedQuestions[$scope.currentQuestion].submitted) {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                } else {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                    $scope.skippedQues = $scope.skippedQues + 1;
                }

                /*To create answer matrix*/
                for (var i = 0; i < $scope.groupedQuestions[$scope.currentQuestion].answer.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[$scope.currentQuestion].options.length; j++) {
                        if ($scope.groupedQuestions[$scope.currentQuestion].answer[i].opt_id === $scope.groupedQuestions[$scope.currentQuestion].options[j].opt_id) {
                            var options = $scope.groupedQuestions[$scope.currentQuestion].options[j].opt.split(',');
                            for (var k = 0; k < options.length; k++) {
                                for (var l = 0; l < $scope.groupedQuestions[$scope.currentQuestion].matrix_option_params.length; l++) {
                                    if (options[k] === $scope.groupedQuestions[$scope.currentQuestion].matrix_option_params[l]) {
                                        $scope.groupedQuestions[$scope.currentQuestion].ans_matrixarray[i][l] = true;
                                    }
                                };
                            }
                        }
                    };
                };

                /*Comapre answer mtrix and answered matrix*/
                var correct = 0;
                if (!$scope.groupedQuestions[$scope.currentQuestion].skipped) {
                    for (var i = 0; i < $scope.groupedQuestions[$scope.currentQuestion].matrix_answer_params.length; i++) {
                        for (var j = 0; j < $scope.groupedQuestions[$scope.currentQuestion].matrix_option_params.length; j++) {
                            if ($scope.groupedQuestions[$scope.currentQuestion].ans_matrixarray[i][j] !== $scope.groupedQuestions[$scope.currentQuestion].matrixarray[i][j]) {
                                correct = 1;
                            }
                        };
                    };
                    //////console.log(correct);
                    if (correct === 0) {
                        $scope.groupedQuestions[$scope.currentQuestion].correctAns = true;
                    } else {
                        $scope.groupedQuestions[$scope.currentQuestion].incorrectAns = true;
                    }
                }

                /*Create answer json for submission*/
                var obj = {};
                var empty = 0;
                for (var i = 0; i < $scope.groupedQuestions[$scope.currentQuestion].matrix_answer_params.length; i++) {
                    var temp = [];
                    for (var j = 0; j < $scope.groupedQuestions[$scope.currentQuestion].matrix_option_params.length; j++) {
                        if ($scope.groupedQuestions[$scope.currentQuestion].matrixarray[i][j] == true) {
                            var opt = $scope.groupedQuestions[$scope.currentQuestion].matrix_option_params[j];
                            temp.push(opt);
                        }
                    };
                    if (temp.length == 0) {
                        empty++;
                        temp.push("");
                    }
                    obj["" + i + ""] = temp;
                };
                if (empty !== $scope.groupedQuestions[$scope.currentQuestion].matrix_answer_params.length) {
                    $scope.groupedQuestions[$scope.currentQuestion].MatrixAns = obj;
                } else {
                    $scope.groupedQuestions[$scope.currentQuestion].MatrixAns = "";
                }
                //////console.log(obj);
            }
        }
        $scope.matrix_index = function(row, col) {
            $scope.groupedQuestions[$scope.currentQuestion].attempted = true;
            if (!$scope.groupedQuestions[$scope.currentQuestion].submitted) {
                //$scope.groupedQuestions[$scope.currentQuestion].matrixarray[row][col] = !$scope.groupedQuestions[$scope.currentQuestion].matrixarray[row][col];
                if (!$scope.groupedQuestions[$scope.currentQuestion].matrixarray[row][col]) {
                    $scope.matrix_counts++;
                    console.log($scope.matrix_counts);
                    $scope.groupedQuestions[$scope.currentQuestion].matrixarray[row][col] = true;
                    $scope.groupedQuestions[$scope.currentQuestion].attempted = true;
                } else {
                    $scope.groupedQuestions[$scope.currentQuestion].matrixarray[row][col] = false;
                    $scope.matrix_counts--;
                    console.log($scope.matrix_counts);
                    if ($scope.matrix_counts == 0) {
                        $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                    }
                }
            }

        }

        $scope.bookmarkQuestion = function(ques) {
            $scope.groupedQuestions[ques].bookmarked = !$scope.groupedQuestions[ques].bookmarked;
        }
        $scope.getBookmarkedQuestions = function() {
            $scope.bookmarkCount = 0;
            if ($scope.groupedQuestions === undefined) {
                return;
            }
            $scope.groupedQuestions.forEach(function(element) {
                if (element.bookmarked) {
                    $scope.bookmarkCount++;
                }
            });
            return $scope.bookmarkCount;
        };

        $scope.watchInteger = function() {
            if ($scope.integer_ans.length === 0) {
                $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
            } else {
                $scope.groupedQuestions[$scope.currentQuestion].attempted = true;
            }

        }

        $scope.showConfirm = function() {
            $scope.testSubmit = true;
        }
        $scope.showEndpop = function() {
            $scope.endPopup = true;
        }
        $scope.closePop = function() {
            $scope.practiceCompleted = true;
            $scope.endPopup = false;
            $state.go('heatmap');
        }
        $scope.Practicepop = function() {
            $scope.endPracice = true;
        }
        $scope.endPracticepop = function() {
            $scope.endPracice = false;
        }

        $scope.QuestionsEnd = function() {
            $scope.endQuestions = true;
        }
        $scope.CloseQuestionsEnd = function() {
            $scope.endQuestions = false;
        }


        $scope.stopLoader = function() {
            $scope.dataFound = false;

            $timeout(function() {
                if ($scope.dataFound === false) {
                    $scope.overlay = false;
                    $scope.NetworkSubmitError = true;
                }
            }, 30000);

        }
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $ionicHistory.goBack();
        }
        $scope.goToHeatmap = function() {
            $scope.ServerSubmitError = false;
            $scope.NetworkSubmitError = false;
            $state.go('heatmap');
        }
        $scope.goToCompletion = function() {
            $scope.stopLoader();
            // $scope.practiceCompleted = true;
            $scope.ServerSubmitError = false;
            $scope.NetworkSubmitError = false;
            $scope.endQuestions = false;
            $scope.overlay = true;
            $scope.testSubmit = false;
            localstorage.set("bookmark", false);
            localstorage.set('topic', "");
            $scope.answerJsonArray = [];
            for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                $scope.answerArray = [];
                $scope.answerJson = {
                    "topicid": 7
                };
                if ($scope.groupedQuestions[i].submitted === true && $scope.groupedQuestions[i].review === false) {
                    if ($scope.groupedQuestions[i].type === 'matrix_type') {

                        $scope.answerJson.quesnum = $scope.groupedQuestions[i].ques_id;
                        $scope.answerJson.attempted = $scope.groupedQuestions[i].attempted;
                        $scope.answerJson.submitted = $scope.groupedQuestions[i].MatrixAns;
                        $scope.answerJson.time = $scope.groupedQuestions[i].time.toString();
                        $scope.answerJson.bookmarked = $scope.groupedQuestions[i].bookmarked;
                        $scope.answerJsonArray.push($scope.answerJson);
                    } else {

                        $scope.answerJson.quesnum = $scope.groupedQuestions[i].ques_id;
                        $scope.answerJson.attempted = $scope.groupedQuestions[i].attempted;
                        $scope.answerJson.submitted = $scope.groupedQuestions[i].submittedAns.toString();
                        $scope.answerJson.time = $scope.groupedQuestions[i].time.toString();
                        $scope.answerJson.bookmarked = $scope.groupedQuestions[i].bookmarked;
                        $scope.answerJsonArray.push($scope.answerJson);
                    }
                }


            };
            $scope.finalAnswerJson = {
                "paperid": $scope.paper_id,
                "submission": $scope.answerJsonArray
            };
            console.log($scope.finalAnswerJson);

            TestService.postTestAnswers("/apis/practice_answer_submission.json", $scope.finalAnswerJson).success(function(successData) {
                    if (successData !== null) {
                        //////console.log(successData);
                        if (successData.status == 1) {
                            $scope.dataFound = true;
                            $scope.overlay = false;
                            $scope.practiceCompleted = true;
                            //$location.path('/practiceSet/' + $scope.Topic + '/' + $scope.topic_subject);
                            $ionicHistory.goBack();
                        }
                    }
                })
                .error(function(e) {
                    //////console.log(e);
                    $scope.dataFound = true;
                    $scope.overlay = false;
                    $scope.ServerSubmitError = true;
                })
        }

        $scope.closeConfirmPopup = function() {
            $scope.testSubmit = false;
        }
        $scope.showHint = function() {
                $scope.hintForm = !$scope.hintForm;
            }
            /**** Filters ****/

        $scope.allQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.correctCheck = false;
            $scope.incorrectCheck = false;
            $scope.skippedCheck = false;
            $scope.bookmarkCheck = false;
            $scope.AllQuestion = [];
            for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                $scope.AllQuestion.push($scope.groupedQuestionsFilter[i]);
            };
            $scope.groupedQuestions = $scope.AllQuestion;
            $scope.processFilterQuestions();
        }
        $scope.correctQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "correct";
            $scope.allCheck = false;
            $scope.incorrectCheck = false;
            $scope.skippedCheck = false;
            $scope.answerFilter = [];
            if ($scope.correctCheck && !$scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].correctAns == true) {
                        $scope.answerFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.answerFilter);
                $scope.groupedQuestions = $scope.answerFilter;
                $scope.processFilterQuestions();
            } else if ($scope.correctCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].correctAns == true && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.answerFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.answerFilter);
                $scope.groupedQuestions = $scope.answerFilter;
                $scope.processFilterQuestions();
            } else if ($scope.bookmarkCheck && !$scope.correctCheck) {
                $scope.bookmarkedQuestions();
            } else {
                $scope.allQuestions();
            }

        }
        $scope.incorrectQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "incorrect";
            $scope.allCheck = false;
            $scope.correctCheck = false;
            $scope.skippedCheck = false;
            $scope.incorrectFilt = [];
            if ($scope.incorrectCheck && !$scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].incorrectAns == true) {
                        $scope.incorrectFilt.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.incorrectFilt);
                $scope.groupedQuestions = $scope.incorrectFilt;
                $scope.processFilterQuestions();
            } else if ($scope.incorrectCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].incorrectAns == true && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.incorrectFilt.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.incorrectFilt);
                $scope.groupedQuestions = $scope.incorrectFilt;
                $scope.processFilterQuestions();
            } else if ($scope.bookmarkCheck && !$scope.incorrectCheck) {
                $scope.bookmarkedQuestions();
            } else {
                $scope.allQuestions();
            }

        }
        $scope.skippedQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "unattempted";
            $scope.allCheck = false;
            $scope.correctCheck = false;
            $scope.incorrectCheck = false;
            $scope.notAttemptedFilter = [];
            if ($scope.skippedCheck && !$scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].submitted == false) {
                        $scope.notAttemptedFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.notAttemptedFilter);
                $scope.groupedQuestions = $scope.notAttemptedFilter;
                $scope.processFilterQuestions();
            } else if ($scope.skippedCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].submitted == false && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.notAttemptedFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.notAttemptedFilter);
                $scope.groupedQuestions = $scope.notAttemptedFilter;
                $scope.processFilterQuestions();
            } else if ($scope.bookmarkCheck && !$scope.skippedCheck) {
                $scope.bookmarkedQuestions();
            } else {
                $scope.allQuestions();
            }

        }
        $scope.bookmarkedQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "bookmarked";
            $scope.allCheck = false;
            $scope.bookmarkFilter = [];
            if ($scope.bookmarkCheck && !$scope.correctCheck && !$scope.incorrectCheck && !$scope.skippedCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.bookmarkFilter);
                $scope.groupedQuestions = $scope.bookmarkFilter;
                $scope.processFilterQuestions();
            } else if ($scope.correctCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].correctAns == true && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.bookmarkFilter);
                $scope.groupedQuestions = $scope.bookmarkFilter;
                $scope.processFilterQuestions();
            } else if ($scope.incorrectCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].incorrectAns == true && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.bookmarkFilter);
                $scope.groupedQuestions = $scope.bookmarkFilter;
                $scope.processFilterQuestions();
            } else if ($scope.skippedCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].submitted == false && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.bookmarkFilter);
                $scope.groupedQuestions = $scope.bookmarkFilter;
                $scope.processFilterQuestions();
            } else if ($scope.skippedCheck && !$scope.bookmarkCheck && !$scope.correctCheck && !$scope.incorrectCheck) {
                $scope.skippedQuestions();
            } else if (!$scope.skippedCheck && !$scope.bookmarkCheck && !$scope.correctCheck && $scope.incorrectCheck) {
                $scope.incorrectQuestions();
            } else if (!$scope.skippedCheck && !$scope.bookmarkCheck && $scope.correctCheck && !$scope.incorrectCheck) {
                $scope.correctQuestions();
            } else {
                $scope.allQuestions();
            }

        }

        /*****/
    })
    .controller('alltestCtrl', function($scope, $ionicHistory, directiveService, HardwareBackButtonManager, $ionicSideMenuDelegate, $stateParams, $location, $sce, $http, TestService, $ionicLoading, localstorage, $rootScope, $timeout, $ionicScrollDelegate, $ionicPopup, $state, $interval, $interpolate, appConfig) {
        /**
         * MathJax Conf
         */
        $scope.dataFound = false;

        $scope.loaderTimer = $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 10000);
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $ionicHistory.goBack();
        }

        $ionicHistory.nextViewOptions({
            disableAnimate: true
        });
        $scope.top = 0;
        $scope.overlay = true;
        MathJax.Hub.Config({
            skipStartupTypeset: true,
            messageStyle: "none",
            "HTML-CSS": {
                showMathMenu: false
            }
        });
        MathJax.Hub.Configured();

        $scope.answeredCheck = false;
        $scope.skippedCheck = false;
        $scope.bookmarkCheck = false;
        HardwareBackButtonManager.disable();
        $scope.close_side_bar = function() {
            $scope.sidebarCollapse = true;
        }
        $scope.show_side = function() {
                $scope.sidebarCollapse = false;
            }
            /**
             * [contains checks whether the testname contains corresponding subject name]
             * @param  {[string]} sub [sunject name]
             * @return {[boolean]}     [true or false]
             */
        $scope.contains = function(sub) {
            if ($scope.testName.indexOf(sub) > -1) {
                return true;
            }
            return false;
        }
        $scope.testName = $stateParams.name || '';
        $scope.showTestButton = {
            'Physics': false,
            'Chemistry': false,
            'Maths': false
        };
        // Logic to identify the test
        if ($scope.contains('Physics')) {
            $scope.showTestButton.Chemistry = $scope.showTestButton.Maths = true;
        } else if ($scope.contains('Chemistry')) {
            $scope.showTestButton.Physics = $scope.showTestButton.Maths = true;
        } else if ($scope.contains('Math')) {
            $scope.showTestButton.Chemistry = $scope.showTestButton.Physics = true;
        }
        /**
         * Calculate time , show time 
         * @type {Number}
         */
        $scope.timeInSeconds = $stateParams.time * 60;
        $scope.readableTime = {
            hours: 0,
            minutes: 0,
            seconds: 0
        };
        // show 0:0 as 00:00
        $scope.addZero = function(i) {
            if (i < 10) {
                i = "0" + i
            };
            return i;
        }
        $scope.startTimer = function() {
            $scope.testTimer = $interval(function() {
                $scope.timeInSeconds--;
                $scope.readableTime.seconds = $scope.addZero($scope.timeInSeconds % 60);
                $scope.readableTime.minutes = $scope.addZero(Math.floor(($scope.timeInSeconds / 60) % 60));
                $scope.readableTime.hours = $scope.addZero(Math.floor($scope.timeInSeconds / 3600));
            }, 1000);
            $scope.timeToEndTest = parseInt($stateParams.time) || 0;
            // End test after specified time 
            $timeout(function() {
                $scope.readableTime.seconds = 00;
                $scope.readableTime.minutes = 00;
                $scope.readableTime.hours = 00;
                $interval.cancel($scope.testTimer);
                $interval.cancel($scope.questionTimer);
                $scope.showEndPopup();
            }, $scope.timeToEndTest * 1000 * 60);
        };
        // Capture the time take for each question
        $scope.changeQuestionTimer = function() {
            if ($scope.questionTimer) {
                $interval.cancel($scope.questionTimer);
            }
            $scope.questionTimer = $interval(function() {
                $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].time++;
            }, 1000);
        };
        /**
         * Get question from API
         */
        $scope.testId = $stateParams.test || '';
        $scope.paperId = $stateParams.paper || '';
        $scope.addAnswerField = function() {
            for (var i = $scope.groupedQuestions.length - 1; i >= 0; i--) {
                for (var j = $scope.groupedQuestions[i].length - 1; j >= 0; j--) {
                    $scope.groupedQuestions[i][j].answerList = [];
                    $scope.groupedQuestions[i][j].attempted = false;
                    $scope.groupedQuestions[i][j].bookmarked = false;
                    $scope.groupedQuestions[i][j].skipped = false;
                    $scope.groupedQuestions[i][j].time = 0;
                    $scope.groupedQuestions[i][j].integer_answer = "";
                    // //////console.log($scope.groupedQuestions);
                    for (var k = $scope.groupedQuestions[i][j].options.length - 1; k >= 0; k--) {
                        $scope.groupedQuestions[i][j].options[k].chosen = true;
                        $scope.groupedQuestions[i][j].answerList.push(-1);
                    };
                    if ($scope.groupedQuestions[i][j].type === "matrix_type") {
                        // console.log("matrix_type:"+i+" : "+j);
                        if ($scope.groupedQuestions[i][j].matrix_answer_params !== null && $scope.groupedQuestions[i][j].matrix_option_params !== null) {
                            $scope.quest_options = $scope.groupedQuestions[i][j].matrix_answer_params;
                            $scope.answer_options = $scope.groupedQuestions[i][j].matrix_option_params;

                            $scope.groupedQuestions[i][j].matrixarray = new Array($scope.quest_options.length)
                                //var matrixarray = new Array($scope.quest_options.length)
                            for (k = 0; k < $scope.quest_options.length; k++)
                                $scope.groupedQuestions[i][j].matrixarray[k] = new Array($scope.answer_options.length)

                            for (var l = 0; l < $scope.quest_options.length; l++) {
                                for (var m = 0; m < $scope.answer_options.length; m++) {
                                    $scope.groupedQuestions[i][j].matrixarray[l][m] = false;
                                };
                            };
                        }

                    }
                };
            };
            //////console.log($scope.groupedQuestions);
        }
        $scope.getQuestions = function() {
            TestService.getQuestions($scope.testId, $scope.paperId)
                .success(function(successData) {
                    if (successData.length !== 0) {
                        $timeout.cancel($scope.loaderTimer);
                        $scope.dataFound = true;
                        //$scope.glued = true;
                        $scope.overlay = false;
                        $scope.questionsList = successData;
                        console.log(successData)
                        if ($scope.questionsList.length == 0) {
                            $scope.showConfirm();
                        }
                        $scope.startTimer();
                        //////console.log($scope.questionsList)
                        for (var i = 0; i < $scope.questionsList.length; i++) {
                            ////console.log(successData[i].type)
                            if ($scope.questionsList[i].hint === "" && $scope.questionsList[i].hint_2 === "" && $scope.questionsList[i].hint_3 === "") {
                                $scope.questionsList[i].no_hint = true;
                            } else {
                                $scope.questionsList[i].no_hint = false;
                                console.log(i);
                            }
                            /*if ($scope.questionsList[i].type === 'multiple_correct_choice_type') {
                                // ////console.log("multiple");
                            } else {
                                //////console.log("single");
                            }*/
                            $scope.questionsList[i].quesNo = i + 1;
                        };
                        $scope.processQuestions();
                    } else {
                        $scope.dataFound = true;
                        $scope.overlay = false;
                        $scope.testCompleted = true;
                        $state.go('heatmap');
                    }
                });
        };
        $scope.getQuestions();
        /**
         * Group functions according to topic
         */
        $scope.groupBy = function(array, f) {
            var groups = {};
            array.forEach(function(o) {
                var group = JSON.stringify(f(o));
                groups[group] = groups[group] || [];
                groups[group].push(o);
            });
            return Object.keys(groups).map(function(group) {
                return groups[group];
            })
        };
        $scope.processQuestions = function() {
            $scope.groupedQuestions = $scope.groupBy($scope.questionsList, function(item) {
                return [item.topic];
            });
            $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            $scope.addAnswerField();
            $scope.currentView = 0;
            $scope.currentQuestion = 0;
            $scope.changeQuestionTimer();
        };
        $scope.processFilterQuestions = function(view) {
            $scope.top = 0;
            $ionicScrollDelegate.scrollTop();
            $scope.groupedQuestions = $scope.groupBy($scope.questionsList, function(item) {
                return [item.topic];
            });
            $scope.newQustionList = [];
            var phy = 0;
            var che = 0;
            var mat = 0;
            for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                    if ($scope.groupedQuestions[i][j].subject == "physics") {
                        phy = 1;
                    } else if ($scope.groupedQuestions[i][j].subject == "chemistry") {
                        che = 1;
                    } else if ($scope.groupedQuestions[i][j].subject == "math") {
                        mat = 1;
                    }
                };
            };
            if (phy == 0) {
                //////console.log("No physics");
                $scope.newQustionList.push({
                    "datas": 1000,
                    "topic": "Physics"
                });

            } else {
                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].subject == "physics") {
                            $scope.newQustionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };
            }
            if (che == 0) {

                //////console.log("No chemistry");

                $scope.newQustionList.push({
                    "datas": 1000,
                    "topic": "Chemistry"
                });

            } else {

                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].subject == "chemistry") {
                            $scope.newQustionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };
            }
            if (mat == 0) {
                //////console.log("No maths");
                $scope.newQustionList.push({
                    "datas": 1000,
                    "topic": "Mathematics"
                });
            } else {
                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].subject == "math") {
                            $scope.newQustionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };
            }
            //  //////console.log("Test :" + $scope.newQustionList);
            $scope.groupedQuestions = $scope.groupBy($scope.newQustionList, function(item) {
                return [item.topic];
            });
            if ($scope.groupedQuestions[$scope.currentView][0].datas == 1000) {
                $scope.question_space = true;
                $scope.question_space1 = true;
            } else {
                $scope.question_space = false;
                $scope.question_space1 = false;
            }
            //////console.log($scope.groupedQuestions);

            $scope.currentView = view;
            $scope.currentQuestion = 0;
            $scope.changeQuestionTimer();
        };
        $scope.changeSub = function(num, id) {
            // //////console.log($scope.groupedQuestions[num]);
            if ($scope.groupedQuestions[num][0].datas == 1000) {
                $scope.question_space = true;
                $scope.question_space1 = true;
            } else {
                $scope.question_space = false;
                $scope.question_space1 = false;
            }
            if (num !== $scope.currentView) {
                if (num > $scope.currentView) {
                    $ionicScrollDelegate.scrollTop();
                    $scope.currentView = num;
                    $scope.currentQuestion = 0;
                    $scope.integer_ans = $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].integer_answer;
                    $scope.top = 0;
                } else if (num < $scope.currentView) {
                    if (id == 10) {
                        $ionicScrollDelegate.scrollTop();
                        $scope.currentView = num;
                        $scope.currentQuestion = 0;
                        $scope.integer_ans = $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].integer_answer;
                    } else {
                        $scope.currentView = num;
                        $scope.currentQuestion = 0;
                        $scope.integer_ans = $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].integer_answer;
                        $ionicScrollDelegate.scrollBottom();
                    }
                }

            }
            $scope.changeQuestionTimer();
            // $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            //////console.log($scope.groupedQuestions);
        };
        $scope.changeQuestion = function(num) {
            if (num != -1) {
                //////console.log(num)
                if ($scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].type === 'matrix_type') {
                    $scope.matrixAnswer($scope.currentView, $scope.currentQuestion);
                }

                $scope.currentQuestion = num;
                $scope.integer_ans = $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].integer_answer;

                $scope.top = (num - 7) * 50;
                $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                $scope.changeQuestionTimer();
                //////console.log($scope.groupedQuestions);
                // $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            }
        }
        $scope.changeQuestionForward = function(num) {
            if (num != -1) {
                //////console.log(num)
                $scope.currentQuestion = num;
                $scope.integer_ans = $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].integer_answer;

                if ($scope.currentQuestion > 7) {
                    $scope.top += 50;
                    $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                } else {
                    $scope.top = 0;
                }
                $scope.changeQuestionTimer();
                //////console.log($scope.groupedQuestions);
                // $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            }
        }
        $scope.changeQuestionBackward = function(num) {
            if (num != -1) {
                //////console.log(num)
                $scope.currentQuestion = num;
                $scope.integer_ans = $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].integer_answer;

                if ($scope.currentQuestion != 0) {
                    $scope.top -= 50;
                    $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                } else {
                    $scope.top = 0;
                }
                $scope.changeQuestionTimer();
                //////console.log($scope.groupedQuestions);
                // $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            }
        }
        $scope.getNumber = function(num) {
            return new Array(num);
        }
        $scope.showConfirm = function() {
            $scope.testSubmit = true;
        };
        $scope.closeConfirmPopup = function() {
            $scope.testSubmit = false;
        };
        $scope.stopLoader = function() {
            $scope.dataFound = false;

            $timeout(function() {
                if ($scope.dataFound === false) {
                    $scope.overlay = false;
                    $scope.NetworkSubmitError = true;
                }
            }, 30000);
        }
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $state.go('heatmap');
        }
        $scope.goToHeatmap = function() {
            $scope.NetworkSubmitError = false;
            $scope.ServerSubmitError = false;
            $state.go('heatmap');
        }
        $scope.goToCompletion = function() {
            $scope.overlay = true;
            $scope.ServerSubmitError = false;
            $scope.NetworkSubmitError = false;
            var testObj = {};
            testObj.bookmarked = $scope.getBookmarkedQuestions();
            testObj.questions = 0;
            testObj.testId = $scope.testId;
            testObj.paperId = $scope.paperId;
            testObj.paperName = $scope.testName;
            testObj.totTime = $stateParams.time;
            if ($stateParams.time == 180) {
                testObj.questions = 30;
            } else {
                testObj.questions = 10;
            }
            testObj.timeTaken = $scope.readableTime;
            localstorage.setObject('lastTestDetails', testObj);
            //$scope.testCompleted = true;
            $scope.testSubmit = false;
            $scope.endReached = false;
            $scope.answerJsonArray = [];

            $scope.stud_id = localstorage.get("stud_id");

            $scope.totQuestions = 1;
            for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                    $scope.answerArray = [];
                    $scope.answerJson = {
                        "topicid": 7
                    };
                    if ($scope.groupedQuestions[i][j].type === "integer_type") {
                        $scope.answerJson.quesnum = $scope.groupedQuestions[i][j].question_id;
                        if ($scope.answerJson.submitted = $scope.groupedQuestions[i][j].integer_answer === null) {
                            $scope.groupedQuestions[i][j].integer_answer = "";
                            $scope.answerJson.submitted = $scope.groupedQuestions[i][j].integer_answer.toString();
                        } else {
                            $scope.answerJson.submitted = $scope.groupedQuestions[i][j].integer_answer.toString();
                        }
                        $scope.answerJson.time = $scope.groupedQuestions[i][j].time.toString();
                        $scope.answerJsonArray.push($scope.answerJson);
                        $scope.totQuestions = $scope.totQuestions + 1;
                    } else if ($scope.groupedQuestions[i][j].type === 'matrix_type') {
                        $scope.answerJson.quesnum = $scope.groupedQuestions[i][j].question_id;
                        $scope.answerJson.submitted = $scope.groupedQuestions[i][j].MatrixAns;
                        $scope.answerJson.time = $scope.groupedQuestions[i][j].time.toString();
                        $scope.answerJsonArray.push($scope.answerJson);
                        $scope.totQuestions = $scope.totQuestions + 1;
                    } else {
                        $scope.answerJson.quesnum = $scope.groupedQuestions[i][j].question_id;

                        for (k = 0; k < $scope.groupedQuestions[i][j].answerList.length; k++) {
                            if ($scope.groupedQuestions[i][j].answerList[k] != -1) {
                                //////console.log($scope.groupedQuestions[i][j].options[k].answer_id);
                                $scope.answerArray.push($scope.groupedQuestions[i][j].options[k].answer_id);
                            }
                        }
                        //  //////console.log($scope.groupedQuestions[i][j].answerList);
                        $scope.answerJson.submitted = $scope.answerArray.toString();
                        $scope.answerJson.time = $scope.groupedQuestions[i][j].time.toString();
                        $scope.answerJsonArray.push($scope.answerJson);
                        $scope.totQuestions = $scope.totQuestions + 1;
                    }

                };
            };
            $scope.finalAnswerJson = {
                "paperid": parseInt($scope.paperId),
                "submission": $scope.answerJsonArray
            };
            console.log($scope.finalAnswerJson);
            ////console.log($scope.groupedQuestions);
            $scope.stopLoader();
            TestService.postTestAnswers("/apis/test_series_answer_submission.json", $scope.finalAnswerJson).success(function(successData) {
                    if (successData !== null) {
                        //////console.log(successData);
                        if (successData.status == 1) {
                            $scope.dataFound = true;
                            $scope.overlay = false;
                            $location.path('/test-details/' + $scope.paperId);
                        }
                    }
                })
                .error(function() {
                    $scope.dataFound = true;
                    $scope.overlay = false;
                    $scope.ServerSubmitError = true;
                })
        };

        $scope.showHint = function() {
            $scope.hintForm = !$scope.hintForm;
        }

        $scope.showEndPopup = function() {
            $scope.endReached = true;
        };
        $scope.integerAnswer = function(view, quesnum) {
            $scope.groupedQuestions[view][quesnum].integer_answer = $scope.integer_ans;
            if ($scope.groupedQuestions[view][quesnum].integer_answer !== "") {
                $scope.groupedQuestions[view][quesnum].attempted = true;
            } else {
                $scope.groupedQuestions[view][quesnum].attempted = false;
            }
        }
        $scope.matrix_index = function(row, col) {
            $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].attempted = true;
            //////console.log(row + '-' + col);
            //////console.log($scope.quest_options.length);
            // //////console.log(matrixarray);
            $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].matrixarray[row][col] = !$scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].matrixarray[row][col];
        }
        $scope.matrixAnswer = function(view, quesnum) {

            var obj = {};
            var empty = 0;
            for (var i = 0; i < $scope.groupedQuestions[view][quesnum].matrix_answer_params.length; i++) {
                var temp = [];
                for (var j = 0; j < $scope.groupedQuestions[view][quesnum].matrix_option_params.length; j++) {
                    if ($scope.groupedQuestions[view][quesnum].matrixarray[i][j] == true) {
                        var opt = $scope.groupedQuestions[view][quesnum].matrix_option_params[j];
                        temp.push(opt);
                    }
                };
                if (temp.length == 0) {
                    empty++;
                    temp.push("");
                }
                obj["" + i + ""] = temp;
            };
            if (empty !== $scope.groupedQuestions[view][quesnum].matrix_answer_params.length) {
                $scope.groupedQuestions[view][quesnum].MatrixAns = obj;
            } else {
                $scope.groupedQuestions[view][quesnum].MatrixAns = "";
            }
        }
        $scope.markAnswer = function(viewNo, quesNo, optionIndex) {
            if ($scope.groupedQuestions[viewNo][quesNo].type === 'single_correct_choice_type') {
                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[viewNo].length; j++) {
                        for (var k = 0; k < $scope.groupedQuestions[viewNo][quesNo].options.length; k++) {
                            if (k == optionIndex) {
                                $scope.groupedQuestions[viewNo][quesNo].options[optionIndex].chosen = false;
                            } else {
                                $scope.groupedQuestions[viewNo][quesNo].options[k].chosen = true;
                            }
                        };
                    };
                };
                // //////console.log($scope.groupedQuestions);
                // //////console.log(viewNo,quesNo,optionIndex)
                if ($scope.groupedQuestions[viewNo][quesNo].type === 'single_correct_choice_type' && $scope.groupedQuestions[viewNo][quesNo].attempted) {
                    // If other option clicked in single choice , then switch the choice
                    if ($scope.groupedQuestions[viewNo][quesNo].answerList[optionIndex] === -1) {
                        for (var i = $scope.groupedQuestions[viewNo][quesNo].answerList.length - 1; i >= 0; i--) {
                            $scope.groupedQuestions[viewNo][quesNo].answerList[i] = -1;
                        };
                        $scope.groupedQuestions[viewNo][quesNo].answerList[optionIndex] = optionIndex + 1;
                        return;
                    } else {
                        $scope.groupedQuestions[viewNo][quesNo].answerList[optionIndex] = -1;
                        $scope.groupedQuestions[viewNo][quesNo].attempted = false;
                        return;
                    }
                }
                if ($scope.groupedQuestions[viewNo][quesNo].attempted) {
                    // User already clicked , second click , reset the color
                    if ($scope.groupedQuestions[viewNo][quesNo].answerList[optionIndex] !== -1) {
                        $scope.groupedQuestions[viewNo][quesNo].answerList[optionIndex] = -1;
                        for (var i = $scope.groupedQuestions[viewNo][quesNo].answerList.length - 1; i >= 0; i--) {
                            if ($scope.groupedQuestions[viewNo][quesNo].answerList[i] !== -1) {
                                break;
                            }
                        };
                        if (i < 0) {
                            $scope.groupedQuestions[viewNo][quesNo].attempted = false;
                            $scope.groupedQuestions[viewNo][quesNo].skipped = true;
                        }
                    } else {
                        // User clicks on an item which is not yet clicked
                        $scope.groupedQuestions[viewNo][quesNo].answerList[optionIndex] = optionIndex + 1;
                    }
                } else {
                    $scope.groupedQuestions[viewNo][quesNo].answerList[optionIndex] = optionIndex + 1;
                    $scope.groupedQuestions[viewNo][quesNo].attempted = true;
                    $scope.groupedQuestions[viewNo][quesNo].skipped = false;

                };
            } else if ($scope.groupedQuestions[viewNo][quesNo].type === 'multiple_correct_choice_type') {
                //////console.log(quesNo);
                $scope.groupedQuestions[viewNo][quesNo].options[optionIndex].chosen = !$scope.groupedQuestions[viewNo][quesNo].options[optionIndex].chosen;
                if ($scope.groupedQuestions[viewNo][quesNo].options[optionIndex].chosen === false) {
                    $scope.groupedQuestions[viewNo][quesNo].answerList[optionIndex] = 1;
                } else {
                    $scope.groupedQuestions[viewNo][quesNo].answerList[optionIndex] = -1;
                }
                $scope.groupedQuestions[viewNo][quesNo].attempted = true;
            } else if ($scope.groupedQuestions[viewNo][quesNo].type === 'matrix_type') {

            }
        };

        $scope.showNextQuestion = function() {

            // Logic for skipped
            if ($scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].type === 'integer_type') {
                $scope.integerAnswer($scope.currentView, $scope.currentQuestion);
                $scope.integer_ans = $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].integer_answer;
            }
            if ($scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].type === 'matrix_type') {
                $scope.matrixAnswer($scope.currentView, $scope.currentQuestion);
            }

            if ($scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].attempted) {
                $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].skipped = false;
            } else {
                $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].skipped = true;
            }
            // Check if the question has reached the end
            if (($scope.currentView === $scope.groupedQuestions.length - 1) && ($scope.currentQuestion === $scope.groupedQuestions[$scope.currentView].length - 1)) {
                $scope.showConfirm();
            } else if ($scope.currentQuestion === $scope.groupedQuestions[$scope.currentView].length - 1) {
                // End of the subject is reached , switch the subject
                $scope.changeSub($scope.currentView + 1, 11);
            } else {
                // change question
                $scope.changeQuestionForward($scope.currentQuestion + 1);
            }
        };
        $scope.showPrevQuestion = function() {
            if ($scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].type === 'integer_type') {
                $scope.integerAnswer($scope.currentView, $scope.currentQuestion);
                $scope.integer_ans = $scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].integer_answer;
            }
            if ($scope.groupedQuestions[$scope.currentView][$scope.currentQuestion].type === 'matrix_type') {
                $scope.matrixAnswer($scope.currentView, $scope.currentQuestion);
            }
            // Check if the question has reached the first question , then switch the subject
            if ($scope.currentQuestion === 0) {
                $scope.changeSub($scope.currentView - 1, 9);
                $scope.changeQuestion($scope.groupedQuestions[$scope.currentView].length - 1)
            } else {
                // change question
                $scope.changeQuestionBackward($scope.currentQuestion - 1);
            }
        };
        $scope.getAnsweredNumber = function() {
            $scope.answeredCount = 0;
            if ($scope.groupedQuestions === undefined) {
                return;
            }
            $scope.groupedQuestions[$scope.currentView].forEach(function(element) {
                if (element.attempted) {
                    $scope.answeredCount++;
                }
            });
            return $scope.answeredCount;
        };
        $scope.getSkippedCount = function() {
            $scope.skipCount = 0;
            if ($scope.groupedQuestions === undefined) {
                return;
            }
            $scope.groupedQuestions[$scope.currentView].forEach(function(element) {
                if (element.skipped) {
                    $scope.skipCount++;
                }
            });
            return $scope.skipCount;
        };
        $scope.bookmarkQuestion = function(viewNo, quesNo) {
            // //////console.log(viewNo, quesNo)
            $scope.groupedQuestions[viewNo][quesNo].bookmarked = !$scope.groupedQuestions[viewNo][quesNo].bookmarked;
            // //////console.log($scope.groupedQuestions[viewNo][quesNo].bookmarked);
        };
        $scope.getBookmarkedQuestions = function() {
            $scope.bookmarkCount = 0;
            if ($scope.groupedQuestions === undefined) {
                return;
            }
            $scope.groupedQuestions[$scope.currentView].forEach(function(element) {
                if (element.bookmarked) {
                    $scope.bookmarkCount++;
                }

            });
            return $scope.bookmarkCount;
        };

        /** Filters **/

        $scope.allQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.answeredCheck = false;
            $scope.skippedCheck = false;
            $scope.bookmarkCheck = false;
            //////console.log("All Questions");
            $scope.AllQuestion = [];
            //////console.log($scope.groupedQuestionsFilter);
            for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                    $scope.AllQuestion.push($scope.groupedQuestionsFilter[i][j]);
                };
            };
            //////console.log($scope.AllQuestion);
            $scope.questionsList = $scope.AllQuestion;
            $scope.processFilterQuestions($scope.currentView);
        }
        $scope.answeredQuestions = function() {
            //////console.log($scope.currentView);
            $scope.filter_text = "answered";
            $scope.sidebarCollapse = true;
            //////console.log("Answered Questions");
            $scope.allCheck = false;
            $scope.skippedCheck = false;
            // $scope.bookmarkCheck = false;
            $scope.answerFilter = [];
            if ($scope.answeredCheck && !$scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].attempted == true) {
                            $scope.answerFilter.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                //////console.log($scope.answerFilter);
                $scope.questionsList = $scope.answerFilter;
                $scope.processFilterQuestions($scope.currentView);
            } else if ($scope.bookmarkCheck && $scope.answeredCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].bookmarked == true && $scope.groupedQuestionsFilter[i][j].attempted == true) {
                            $scope.answerFilter.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                //////console.log($scope.answerFilter);
                $scope.questionsList = $scope.answerFilter;
                $scope.processFilterQuestions($scope.currentView);
            } else if ($scope.bookmarkCheck && !$scope.answeredCheck) {
                $scope.answeredQuestions();
            } else {
                $scope.allQuestions();
            }


        }
        $scope.skippedQuestions = function() {
            $scope.filter_text = "skipped";
            $scope.sidebarCollapse = true;
            //////console.log("Skipped Questions");
            $scope.allCheck = false;
            $scope.answeredCheck = false;
            // $scope.bookmarkCheck = false;
            $scope.skippedFilter = [];
            if ($scope.skippedCheck && !$scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].skipped == true) {
                            $scope.skippedFilter.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                //////console.log($scope.skippedFilter);
                $scope.questionsList = $scope.skippedFilter;
                $scope.processFilterQuestions($scope.currentView);
            } else if ($scope.bookmarkCheck && $scope.skippedCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].bookmarked == true && $scope.groupedQuestionsFilter[i][j].skipped == true) {

                            $scope.skippedFilter.push($scope.groupedQuestionsFilter[i][j]);

                        }

                    };
                };
                //////console.log($scope.skippedFilter);
                $scope.questionsList = $scope.skippedFilter;
                $scope.processFilterQuestions($scope.currentView);
            } else if ($scope.bookmarkCheck && !$scope.skippedCheck) {
                $scope.bookmarkedQuestions();
            } else {
                $scope.allQuestions();
            }


        }
        $scope.bookmarkedQuestions = function() {
            $scope.filter_text = "bookmarked";
            $scope.sidebarCollapse = true;
            //////console.log("Bookmarked Questions");
            $scope.allCheck = false;
            // $scope.answeredCheck = false;
            // $scope.skippedCheck = false;
            $scope.bookmarkFilter = [];
            if ($scope.bookmarkCheck && !$scope.answeredCheck && !$scope.skippedCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].bookmarked == true) {
                            $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                //////console.log($scope.bookmarkFilter);
                $scope.questionsList = $scope.bookmarkFilter;
                $scope.processFilterQuestions($scope.currentView);
            } else if ($scope.answeredCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].bookmarked == true && $scope.groupedQuestionsFilter[i][j].attempted == true) {
                            $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                //////console.log($scope.bookmarkFilter);
                $scope.questionsList = $scope.bookmarkFilter;
                $scope.processFilterQuestions($scope.currentView);
            } else if ($scope.skippedCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].bookmarked == true && $scope.groupedQuestionsFilter[i][j].skipped == true) {
                            $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                //////console.log($scope.bookmarkFilter);
                $scope.questionsList = $scope.bookmarkFilter;
                $scope.processFilterQuestions($scope.currentView);
            } else if ($scope.skippedCheck && !$scope.bookmarkCheck && !$scope.answeredCheck) {
                $scope.skippedQuestions();
            } else if (!$scope.skippedCheck && !$scope.bookmarkCheck && $scope.answeredCheck) {
                $scope.answeredQuestions();
            } else {
                $scope.allQuestions();
            }

        }

        /**  **/
    })
    .controller('analysisCtrl', function($scope, $ionicHistory, directiveService, HardwareBackButtonManager, $ionicSideMenuDelegate, $location, $stateParams, $sce, $http, TestService, $ionicLoading, localstorage, $rootScope, $timeout, $ionicScrollDelegate, $ionicPopup, $state, appConfig) {

        $scope.overlay = true;
        $scope.top = 0;
        $scope.count = 0;
        $scope.dataFound = false;

        $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 10000);
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $ionicHistory.goBack();
        }
        $scope.ObjectLength = function(object) {
            var length = 0;
            for (var key in object) {
                if (object.hasOwnProperty(key)) {
                    ++length;
                }
            }
            return length;
        };
        $ionicHistory.nextViewOptions({
            disableAnimate: true
        });
        //////console.log($stateParams.name);
        MathJax.Hub.Config({
            skipStartupTypeset: true,
            messageStyle: "none",
            "HTML-CSS": {
                showMathMenu: false
            }
        });

        MathJax.Hub.Configured();
        HardwareBackButtonManager.disable();
        $scope.close_side_bar = function() {
            $scope.sidebarCollapse = true;
        }
        $scope.show_side = function() {
            $scope.sidebarCollapse = false;
        }

        $scope.contains = function(sub) {
            if ($scope.testName.indexOf(sub) > -1) {
                return true;
            }
            return false;
        }
        $scope.testName = $stateParams.name || '';
        $scope.showTestButton = {
            'Physics': false,
            'Chemistry': false,
            'Maths': false
        };
        // Logic to identify the test
        if ($scope.contains('Physics')) {
            $scope.currentView = 0;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Maths = true;
        } else if ($scope.contains('physics')) {
            $scope.currentView = 0;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Maths = true;
        }
        if ($scope.contains('Chemistry')) {
            $scope.currentView = 1;
            $scope.chem = true;
            $scope.showTestButton.Physics = $scope.showTestButton.Maths = true;
        } else if ($scope.contains('chemistry')) {
            $scope.currentView = 1;
            $scope.showTestButton.Physics = $scope.showTestButton.Maths = true;
        } else if ($scope.contains('Math')) {
            $scope.currentView = 2;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Physics = true;
        } else if ($scope.contains('math')) {
            $scope.currentView = 2;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Physics = true;
        }



        $scope.groupBy = function(array, f) {
            var groups = {};
            array.forEach(function(o) {
                var group = JSON.stringify(f(o));
                groups[group] = groups[group] || [];
                groups[group].push(o);
            });
            return Object.keys(groups).map(function(group) {
                return groups[group];
            })
        };

        $scope.getNumber = function(num) {
            return new Array(num);
        }

        $scope.getQuestions = function() {

            TestService.getTestQuestions("http://conceptowl.com/apis/get_detailed_analytics.json?paper_id=" + $stateParams.num).success(function(successData) {

                if (successData != null) {
                    console.log(successData);
                    $scope.dataFound = true;
                    $scope.overlay = false;
                    $scope.questionsList = successData;
                    $scope.filterdata = successData;
                    //////console.log($scope.questionsList);
                    $scope.processQuestions(0);
                    //////console.log($scope.groupedQuestions);
                    if ($scope.groupedQuestions.length == 3) {
                        $scope.count = 1;
                    } else {
                        $scope.count = 0;
                    }
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                    $scope.groupedQuestionsFilter = $scope.groupedQuestions;
                    //////console.log($scope.groupedQuestions);
                }
            });
        }


        $scope.getQuestions();
        $scope.processQuestions = function(view) {

            $scope.top = 0;
            $ionicScrollDelegate.scrollTop();

            $scope.groupedQuestions = $scope.groupBy($scope.questionsList, function(item) {
                return [item.topic];
            });

            $scope.newQustionList = [];
            var phy = 0;
            var che = 0;
            var mat = 0;
            for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                    if ($scope.groupedQuestions[i][j].subject == "physics") {
                        phy = 1;
                    } else if ($scope.groupedQuestions[i][j].subject == "chemistry") {
                        che = 1;
                    } else if ($scope.groupedQuestions[i][j].subject == "math") {
                        mat = 1;
                    }
                };
            };
            if (phy == 0) {
                //////console.log("No physics");
                $scope.newQustionList.push({
                    "datas": 1000,
                    "topic": "Physics"
                });

            } else {
                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].subject == "physics") {
                            $scope.groupedQuestions[i][j].datas = 1001;
                            $scope.newQustionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };
            }
            if (che == 0) {

                //////console.log("No chemistry");

                $scope.newQustionList.push({
                    "datas": 1000,
                    "topic": "Chemistry"
                });

            } else {

                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].subject == "chemistry") {
                            $scope.groupedQuestions[i][j].datas = 1001;
                            $scope.newQustionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };
            }
            if (mat == 0) {
                //////console.log("No maths");
                $scope.newQustionList.push({
                    "datas": 1000,
                    "topic": "Mathematics"
                });
            } else {
                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].subject == "math") {
                            $scope.groupedQuestions[i][j].datas = 1001;
                            $scope.newQustionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };
            }
            //  //////console.log("Test :" + $scope.newQustionList);
            $scope.modiQuestionList = [];
            $scope.groupedQuestions = $scope.groupBy($scope.newQustionList, function(item) {
                return [item.topic];
            });

            //  //////console.log("dfgfd");
            if ($scope.groupedQuestions[view][0].datas == 1000) {
                $scope.question_space = true;
                $scope.question_space1 = true;

            } else {
                $scope.question_space = false;
                $scope.question_space1 = false;
            }


            if ($scope.count == 0) {
                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].datas == 1001) {
                            $scope.modiQuestionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };

                $scope.groupedQuestions = $scope.groupBy($scope.modiQuestionList, function(item) {
                    return [item.topic];
                });
            }


            //////console.log($scope.groupedQuestions);
            $scope.currentView = view;
            $scope.currentQuestion = 0;
        };

        $scope.processAnswers = function() {
            for (var i = $scope.groupedQuestions.length - 1; i >= 0; i--) {
                for (var j = $scope.groupedQuestions[i].length - 1; j >= 0; j--) {
                    if ($scope.groupedQuestions[i][j].type === 'single_correct_choice_type') {
                        if ($scope.groupedQuestions[i][j].answer_type === 'skipped') {
                            $scope.groupedQuestions[i].skippedAnswer = $scope.groupedQuestions[i].skippedAnswer + 1;
                            $scope.groupedQuestions[i][j].skipped = true;
                            for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].answer[0].opt) {
                                    $scope.groupedQuestions[i][j].options[k].skipped = true;
                                } else {
                                    $scope.groupedQuestions[i][j].options[k].skipped = false;
                                }
                            };
                        }
                        if ($scope.groupedQuestions[i][j].answer_type === 'attempted') {
                            if ($scope.groupedQuestions[i][j].submitted[0].opt == $scope.groupedQuestions[i][j].answer[0].opt) {
                                $scope.groupedQuestions[i][j].correct = true;
                                $scope.groupedQuestions[i].correctAnswer = $scope.groupedQuestions[i].correctAnswer + 1;
                                for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                    if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].answer[0].opt) {
                                        $scope.groupedQuestions[i][j].options[k].correct = true;
                                    }
                                };
                            } else {
                                $scope.groupedQuestions[i][j].incorrect = true;
                                for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                    if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].submitted[0].opt) {
                                        $scope.groupedQuestions[i][j].options[k].incorrect = true;
                                        $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                                    }
                                    if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].answer[0].opt) {
                                        $scope.groupedQuestions[i][j].options[k].correct = true;
                                    }
                                };
                            }
                        }
                    } else if ($scope.groupedQuestions[i][j].type === 'integer_type') {
                        if ($scope.groupedQuestions[i][j].answer_type === "attempted") {
                            if (parseInt($scope.groupedQuestions[i][j].integer_answer) === parseInt($scope.groupedQuestions[i][j].submitted[0].opt)) {
                                $scope.groupedQuestions[i][j].correct = true;
                                $scope.groupedQuestions[i].correctAnswer = $scope.groupedQuestions[i].correctAnswer + 1;
                            } else {
                                $scope.groupedQuestions[i][j].incorrect = true;
                                $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                            }
                        } else if ($scope.groupedQuestions[i][j].answer_type === "skipped") {
                            $scope.groupedQuestions[i][j].skipped = true;
                            $scope.groupedQuestions[i].skippedAnswer = $scope.groupedQuestions[i].skippedAnswer + 1;
                        }
                        // $scope.groupedQuestions[i][j].integer_answer = parseInt($scope.groupedQuestions[i][j].integer_answer);
                    } else if ($scope.groupedQuestions[i][j].type === 'multiple_correct_choice_type') {
                        $scope.correct_count = 0;
                        $scope.incorrect_count = 0;
                        if ($scope.groupedQuestions[i][j].answer_type === 'skipped') {
                            // //////console.log("hooo");
                            $scope.groupedQuestions[i].skippedAnswer = $scope.groupedQuestions[i].skippedAnswer + 1;
                            $scope.groupedQuestions[i][j].skipped = true;
                            for (var l = 0; l < $scope.groupedQuestions[i][j].answer.length; l++) {
                                for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                    if ($scope.groupedQuestions[i][j].answer[l].opt == $scope.groupedQuestions[i][j].options[k].opt_id) {
                                        $scope.groupedQuestions[i][j].options[k].skipped = true;
                                    }

                                };
                            };
                        }
                        if ($scope.groupedQuestions[i][j].answer_type === 'attempted') {
                            // //////console.log("hiiii");
                            if ($scope.groupedQuestions[i][j].submitted.length === $scope.groupedQuestions[i][j].answer.length) {

                                var multiple_correct = 0;
                                for (var l = 0; l < $scope.groupedQuestions[i][j].submitted.length; l++) {
                                    var tmp = 0;
                                    for (var m = 0; m < $scope.groupedQuestions[i][j].answer.length; m++) {
                                        if ($scope.groupedQuestions[i][j].submitted[l].opt === $scope.groupedQuestions[i][j].answer[m].opt.toString()) {
                                            multiple_correct++;
                                            tmp++;
                                            for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id.toString() === $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].correct = true;
                                                    //  ////console.log("correct");
                                                    break;
                                                }
                                            };
                                        }

                                    };
                                    if (tmp === 0) {
                                        for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                            if ($scope.groupedQuestions[i][j].options[k].opt_id.toString() === $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                $scope.groupedQuestions[i][j].options[k].incorrect = true;
                                                // ////console.log("Incorrect");
                                            }
                                        };
                                    }

                                }
                                if (multiple_correct === $scope.groupedQuestions[i][j].answer.length) {
                                    $scope.groupedQuestions[i][j].correct = true;
                                    $scope.groupedQuestions[i].correctAnswer = $scope.groupedQuestions[i].correctAnswer + 1;
                                } else {
                                    $scope.groupedQuestions[i][j].incorrect = true;
                                    $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                                }
                                for (var x = 0; x < $scope.groupedQuestions[i][j].answer.length; x++) {
                                    for (var y = 0; y < $scope.groupedQuestions[i][j].options.length; y++) {
                                        if ($scope.groupedQuestions[i][j].options[y].opt_id == $scope.groupedQuestions[i][j].answer[x].opt) {
                                            $scope.groupedQuestions[i][j].options[y].skipped = true;
                                        }
                                    };
                                };
                            } else if ($scope.groupedQuestions[i][j].submitted.length < $scope.groupedQuestions[i][j].answer.length) {
                                //////console.log("less answers");
                                $scope.groupedQuestions[i][j].incorrect = true;
                                $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                                var tmp = false;
                                // var diff=$scope.groupedQuestions[i][j].submitted.length - $scope.groupedQuestions[i][j].answer.length;
                                for (var l = 0; l < $scope.groupedQuestions[i][j].submitted.length; l++) {
                                    for (var m = 0; m < $scope.groupedQuestions[i][j].answer.length; m++) {
                                        if ($scope.groupedQuestions[i][j].submitted[l].opt == $scope.groupedQuestions[i][j].answer[m].opt) {
                                            tmp = true;
                                            for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].correct = true;
                                                }
                                            };
                                        }
                                    };
                                    if (tmp == false) {
                                        for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                            if (tmp == 0) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].incorrect = true;
                                                }
                                            }
                                        };
                                    }
                                    tmp = false;
                                };
                                for (var m = 0; m < $scope.groupedQuestions[i][j].answer.length; m++) {
                                    for (var l = 0; l < $scope.groupedQuestions[i][j].submitted.length; l++) {
                                        if ($scope.groupedQuestions[i][j].submitted[l].opt != $scope.groupedQuestions[i][j].answer[m].opt) {
                                            for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].answer[m].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].skipped = true;
                                                }
                                            };
                                        }
                                    };
                                };
                            } else if ($scope.groupedQuestions[i][j].submitted.length > $scope.groupedQuestions[i][j].answer.length) {
                                //////console.log("more answers");
                                $scope.groupedQuestions[i][j].incorrect = true;
                                $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                                var tmp = false;
                                // var diff=$scope.groupedQuestions[i][j].submitted.length - $scope.groupedQuestions[i][j].answer.length;
                                for (var l = 0; l < $scope.groupedQuestions[i][j].submitted.length; l++) {
                                    for (var m = 0; m < $scope.groupedQuestions[i][j].answer.length; m++) {
                                        if ($scope.groupedQuestions[i][j].submitted[l].opt == $scope.groupedQuestions[i][j].answer[m].opt) {
                                            tmp = true;
                                            for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].correct = true;
                                                }
                                            };
                                        }
                                    };
                                    if (tmp == false) {
                                        for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                            if (tmp == 0) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].incorrect = true;
                                                }
                                            }
                                        };
                                    }
                                    tmp = false;
                                };
                                /*for (var m = $scope.groupedQuestions[i][j].answer.length; m < $scope.groupedQuestions[i][j].submitted.length; m++) {
                                    var c = $scope.groupedQuestions[i][j].answer[m].opt;
                                    for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                        if ($scope.groupedQuestions[i][j].options[k].opt_id == c) {
                                            $scope.groupedQuestions[i][j].options[k].skipped = true;
                                        }
                                    };
                                };*/
                            }
                        }
                    } else if ($scope.groupedQuestions[i][j].type === 'matrix_type') {
                        if ($scope.groupedQuestions[i][j].matrix_answer_params !== null && $scope.groupedQuestions[i][j].matrix_option_params !== null) {
                            if ($scope.groupedQuestions[i][j].answer_type === 'attempted') {

                                for (var m = 0; m < $scope.ObjectLength($scope.groupedQuestions[i][j].submitted); m++) {
                                    if ($scope.groupedQuestions[i][j].submitted[m] !== null) {
                                        //   ////console.log($scope.groupedQuestions[i][j].submitted[m].length);
                                        for (var k = 0; k < $scope.groupedQuestions[i][j].submitted[m].length; k++) {
                                            //   ////console.log($scope.groupedQuestions[i][j].submitted[m]);
                                            for (var l = 0; l < $scope.groupedQuestions[i][j].matrix_option_params.length; l++) {
                                                // ////console.log($scope.groupedQuestions[i][j].submitted[k][l]);
                                                if ($scope.groupedQuestions[i][j].submitted[m][k] === $scope.groupedQuestions[i][j].matrix_option_params[l]) {
                                                    $scope.groupedQuestions[i][j].matrixarray[m][l] = true;
                                                }
                                            };
                                        };
                                    }
                                };
                                for (var w = 0; w < $scope.groupedQuestions[i][j].answer.length; w++) {
                                    //  ////console.log($scope.groupedQuestions[i][j].answer);
                                    for (var x = 0; x < $scope.groupedQuestions[i][j].options.length; x++) {
                                        if ($scope.groupedQuestions[i][j].answer[w].opt === $scope.groupedQuestions[i][j].options[x].opt_id) {
                                            var options = $scope.groupedQuestions[i][j].options[x].opt.split(',');
                                            // ////console.log(options);
                                            for (var y = 0; y < options.length; y++) {
                                                for (var z = 0; z < $scope.groupedQuestions[i][j].matrix_option_params.length; z++) {
                                                    if (options[y] === $scope.groupedQuestions[i][j].matrix_option_params[z]) {
                                                        $scope.groupedQuestions[i][j].ans_matrixarray[w][z] = true;
                                                    }
                                                };
                                            }
                                        }
                                    };
                                };

                                /*Comapre answer mtrix and answered matrix*/
                                var correct = 0;
                                for (var k = 0; k < $scope.groupedQuestions[i][j].matrix_answer_params.length; k++) {
                                    for (var l = 0; l < $scope.groupedQuestions[i][j].matrix_option_params.length; l++) {
                                        if ($scope.groupedQuestions[i][j].ans_matrixarray[k][l] !== $scope.groupedQuestions[i][j].matrixarray[k][l]) {
                                            correct = 1;
                                        }
                                    };
                                };
                                //////console.log(correct);
                                if (correct === 0) {
                                    $scope.groupedQuestions[i][j].correct = true;
                                    $scope.groupedQuestions[i].correctAnswer = $scope.groupedQuestions[i].correctAnswer + 1;
                                } else {
                                    $scope.groupedQuestions[i][j].incorrect = true;
                                    $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                                }
                            } else if ($scope.groupedQuestions[i][j].answer_type === 'skipped') {
                                $scope.groupedQuestions[i][j].skipped = true;
                                $scope.groupedQuestions[i].skippedAnswer = $scope.groupedQuestions[i].skippedAnswer + 1;
                                for (var w = 0; w < $scope.groupedQuestions[i][j].answer.length; w++) {
                                    //  ////console.log($scope.groupedQuestions[i][j].answer);
                                    for (var x = 0; x < $scope.groupedQuestions[i][j].options.length; x++) {
                                        if ($scope.groupedQuestions[i][j].answer[w].opt === $scope.groupedQuestions[i][j].options[x].opt_id) {
                                            var options = $scope.groupedQuestions[i][j].options[x].opt.split(',');
                                            //  ////console.log(options);
                                            for (var y = 0; y < options.length; y++) {
                                                for (var z = 0; z < $scope.groupedQuestions[i][j].matrix_option_params.length; z++) {
                                                    if (options[y] === $scope.groupedQuestions[i][j].matrix_option_params[z]) {
                                                        $scope.groupedQuestions[i][j].ans_matrixarray[w][z] = true;
                                                    }
                                                };
                                            }
                                        }
                                    };
                                };
                            }
                        }

                    }
                };
            };
        }

        $scope.addAnswerField = function() {

            for (var i = $scope.groupedQuestions.length - 1; i >= 0; i--) {
                for (var j = $scope.groupedQuestions[i].length - 1; j >= 0; j--) {
                    $scope.groupedQuestions[i][j].min = Math.floor($scope.groupedQuestions[i][j].time_spent / 60);
                    $scope.groupedQuestions[i][j].sec = $scope.groupedQuestions[i][j].time_spent % 60;
                    $scope.groupedQuestions[i][j].correct = false;
                    $scope.groupedQuestions[i][j].incorrect = false;
                    $scope.groupedQuestions[i][j].skipped = false;
                    if ($scope.groupedQuestions[i][j].datas != 1000) {
                        for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                            $scope.groupedQuestions[i][j].options[k].correct = false;
                            $scope.groupedQuestions[i][j].options[k].incorrect = false;
                        };
                    }
                    if ($scope.groupedQuestions[i][j].type === "matrix_type") {
                        if ($scope.groupedQuestions[i][j].matrix_answer_params !== null && $scope.groupedQuestions[i][j].matrix_option_params !== null) {
                            $scope.quest_options = $scope.groupedQuestions[i][j].matrix_answer_params;
                            $scope.answer_options = $scope.groupedQuestions[i][j].matrix_option_params;

                            $scope.groupedQuestions[i][j].ans_matrixarray = new Array($scope.quest_options.length)
                            for (k = 0; k < $scope.quest_options.length; k++)
                                $scope.groupedQuestions[i][j].ans_matrixarray[k] = new Array($scope.answer_options.length)

                            for (var l = 0; l < $scope.quest_options.length; l++) {
                                for (var m = 0; m < $scope.answer_options.length; m++) {
                                    $scope.groupedQuestions[i][j].ans_matrixarray[l][m] = false;
                                };
                            };

                            $scope.groupedQuestions[i][j].matrixarray = new Array($scope.quest_options.length)
                                //var matrixarray = new Array($scope.quest_options.length)
                            for (k = 0; k < $scope.quest_options.length; k++)
                                $scope.groupedQuestions[i][j].matrixarray[k] = new Array($scope.answer_options.length)

                            for (var l = 0; l < $scope.quest_options.length; l++) {
                                for (var m = 0; m < $scope.answer_options.length; m++) {
                                    $scope.groupedQuestions[i][j].matrixarray[l][m] = false;
                                };
                            };
                        }

                    }
                    if ($scope.groupedQuestions[i][j].hint === "" && $scope.groupedQuestions[i][j].hint_2 === "" && $scope.groupedQuestions[i][j].hint_3 === "") {
                        $scope.groupedQuestions[i][j].no_hint = true;
                    } else {
                        $scope.groupedQuestions[i][j].no_hint = false;
                    }
                };
                $scope.groupedQuestions[i].correctAnswer = 0;
                $scope.groupedQuestions[i].incorrectAnswer = 0;
                $scope.groupedQuestions[i].skippedAnswer = 0;

            };
        }

        $scope.changeSub = function(num, id) {
            $scope.hintForm = false;
            if ($scope.groupedQuestions[num][0].datas == 1000) {
                $scope.question_space = true;
                $scope.question_space1 = true;

            } else {
                $scope.question_space = false;
                $scope.question_space1 = false;
            }
            if (num !== $scope.currentView) {
                if (num > $scope.currentView) {
                    $ionicScrollDelegate.scrollTop();
                    $scope.currentView = num;
                    $scope.currentQuestion = 0;
                    $scope.top = 0;
                } else if (num < $scope.currentView) {
                    if (id == 10) {
                        $ionicScrollDelegate.scrollTop();
                        $scope.currentView = num;
                        $scope.currentQuestion = 0;
                    } else {
                        $scope.currentView = num;
                        $scope.currentQuestion = 0;
                        $ionicScrollDelegate.scrollBottom();
                    }
                }

            }


        }

        $scope.changeQuestion = function(num) {
            $scope.hintForm = false;
            if (num != -1) {
                //////console.log(num)
                $scope.currentQuestion = num;
                $scope.top = (num - 7) * 50;
                $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                //////console.log($scope.groupedQuestions);
                // $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            }

        }


        $scope.changeQuestionForward = function(num) {
            $scope.hintForm = false;
            if (num != -1) {
                //////console.log(num)
                $scope.currentQuestion = num;
                if ($scope.currentQuestion > 7) {
                    $scope.top += 50;
                    $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                } else {
                    $scope.top = 0;
                }
                //////console.log($scope.groupedQuestions);
                // $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            }

        }

        $scope.changeQuestionBackward = function(num) {
            $scope.hintForm = false;
            if (num != -1) {
                //////console.log(num)
                $scope.currentQuestion = num;
                if ($scope.currentQuestion != 0) {
                    $scope.top -= 50;
                    $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                } else {
                    $scope.top = 0;
                }
                //////console.log($scope.groupedQuestions);
                // $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            }

        }

        $scope.setQuestions = function() {
            $scope.quest = $scope.groupedQuestions[$scope.currentView];
        }
        $scope.showNextQuestion = function() {

            if (($scope.currentView === $scope.groupedQuestions.length - 1) && ($scope.currentQuestion === $scope.groupedQuestions[$scope.currentView].length - 1)) {
                $scope.showConfirm();
            } else if ($scope.currentQuestion === $scope.groupedQuestions[$scope.currentView].length - 1) {
                // End of the subject is reached , switch the subject
                $scope.changeSub($scope.currentView + 1, 11);
            } else {
                // change question
                $scope.changeQuestionForward($scope.currentQuestion + 1);
            }
        };
        $scope.showPrevQuestion = function() {
            // Check if the question has reached the first question , then switch the subject
            if ($scope.currentQuestion === 0) {
                $scope.changeSub($scope.currentView - 1, 9);
                $scope.changeQuestion($scope.groupedQuestions[$scope.currentView].length - 1)
            } else {
                // change question
                $scope.changeQuestionBackward($scope.currentQuestion - 1);
            }
        };

        $scope.showConfirm = function() {
            $scope.testSubmit = true;
        };
        $scope.closeConfirmPopup = function() {
            $scope.testSubmit = false;
        }
        $scope.goToCompletion = function() {
            $scope.analyCompleted = true;
            $location.path('/prev');
        }
        $scope.showHint = function() {
            $scope.hintForm = !$scope.hintForm;
        }

        $scope.allQuestions = function() {
            $scope.sidebarCollapse = true;
            //////console.log("All Questions");
            $scope.question_space = false;
            $scope.question_space1 = false;

            $scope.correctCheck = false;
            $scope.incorrectCheck = false;
            $scope.skippedCheck = false;
            $scope.allCheck = true;

            $scope.questionsList = $scope.filterdata;
            $scope.processQuestions($scope.currentView);
            // //////console.log($scope.groupedQuestions);
            $scope.addAnswerField();
            $scope.processAnswers();
            $scope.setQuestions();
        }
        $scope.correctQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "corret answered";
            //////console.log("Answered Questions");
            $scope.allCheck = false;
            $scope.incorrectCheck = false;
            $scope.skippedCheck = false;
            $scope.question_space = false;
            $scope.question_space1 = false;
            // $scope.correctCheck = true;

            $scope.correctAnsFilt = [];

            // $scope.questionsList=[];
            if ($scope.correctCheck) {
                //////console.log($scope.groupedQuestionsFilter);
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].correct == true) {
                            $scope.correctAnsFilt.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                if ($scope.correctAnsFilt.length == 0) {
                    $scope.question_space = true;
                    $scope.question_space1 = true;
                    $scope.questionsList = $scope.correctAnsFilt;
                    //////console.log($scope.questionsList);
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                } else {
                    $scope.questionsList = $scope.correctAnsFilt;
                    //////console.log($scope.questionsList);
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                }

            } else {
                $scope.correctCheck = false;
                $scope.allQuestions();
            }

        }
        $scope.skippedQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "skipped";
            //////console.log("Skipped Questions");
            $scope.correctCheck = false;
            $scope.incorrectCheck = false;
            $scope.allCheck = false;
            $scope.question_space = false;
            $scope.question_space1 = false;
            // $scope.skippedCheck = true;

            $scope.skippedFilt = [];
            if ($scope.skippedCheck) {
                //////console.log($scope.groupedQuestionsFilter);
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].skipped == true) {
                            $scope.skippedFilt.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                if ($scope.skippedFilt.length == 0) {
                    $scope.questionsList = $scope.skippedFilt;
                    //////console.log($scope.questionsList);
                    $scope.question_space = true;
                    $scope.question_space1 = true;
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                } else {
                    $scope.questionsList = $scope.skippedFilt;
                    //////console.log($scope.questionsList);
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                }

            } else {
                $scope.skippedCheck = false;
                $scope.allQuestions();
            }

        }
        $scope.incorrectQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "incorret answered";
            //////console.log("Incorrect Questions");
            $scope.correctCheck = false;
            $scope.allCheck = false;
            $scope.skippedCheck = false;

            $scope.question_space1 = false;
            $scope.question_space = false;
            //  $scope.incorrectCheck = true;

            $scope.incorrectFilt = [];
            if ($scope.incorrectCheck) {
                //////console.log($scope.groupedQuestionsFilter);
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].incorrect == true) {
                            $scope.incorrectFilt.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                if ($scope.incorrectFilt.length == 0) {
                    $scope.questionsList = $scope.incorrectFilt;
                    //////console.log($scope.questionsList);
                    $scope.question_space = true;
                    $scope.question_space1 = true;
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                } else {
                    $scope.questionsList = $scope.incorrectFilt;
                    //////console.log($scope.questionsList);
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                }
            } else {
                $scope.incorrectCheck = false;
                $scope.allQuestions();
            }
        }

        /**  **/
    })
    /*.controller('DashCtrl', function($scope, $rootScope, localstorage, TestService, $ionicLoading) {

        $rootScope.navhider = false;
        $scope.overlay = true;
        //////console.log($rootScope.navhider);
        $rootScope.mock = true;
        $rootScope.doubt = false;
        $scope.butt_text = "Register";
        $scope.stud_id = localstorage.get("stud_id");

        TestService.getAllTest("http://conceptowl.com/apis/get_all_tests.json?studentid=" + $scope.stud_id).success(function(successData) {
            if (successData !== null) {
                $scope.overlay = false;
                //////console.log(successData);
                $scope.results = successData;

            }
        });

        $rootScope.practice = false;
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.navClick = function(bol) {


            $rootScope.navhider = !$rootScope.navhider;

            //////console.log($rootScope.navhider);

        };
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };

        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
        $scope.dhm = function(ms) {
            days = Math.floor(ms / (24 * 60 * 60 * 1000));
            daysms = ms % (24 * 60 * 60 * 1000);
            hours = Math.floor((daysms) / (60 * 60 * 1000));
            hoursms = ms % (60 * 60 * 1000);
            minutes = Math.floor((hoursms) / (60 * 1000));
            minutesms = ms % (60 * 1000);
            sec = Math.floor((minutesms) / (1000));
            return days;
            // return days + ":" + hours + ":" + minutes + ":" + sec;
        };
        $scope.registerExam = function() {
            $scope.butt_text = "Start the test";
            $scope.register = true;
        }
        $scope.popup_register_button = "REGISTER";
        $scope.front_page = 'all tests';
        $scope.title_test_name = "Name of all India JEE Mock Test";
        $scope.title_test_date = "21-07-2015";
        $scope.title_test_time = "10:12:25";
        $scope.average_speed = 4;
        $scope.total_test = 10;
        $scope.test_taken = 3;
        $scope.total_score = 10;
        $scope.score_obtained = 7;
        $scope.registered = false;
        $scope.test_date = $scope.title_test_date.split('-');
        $scope.test_time = $scope.title_test_time.split(':');
        var test_date_format = new Date($scope.test_date[2], $scope.test_date[1] - 1, $scope.test_date[0], $scope.test_time[0], $scope.test_time[1], $scope.test_time[2], 0);
        var date_now_format = new Date();
        $scope.days_remaining = $scope.dhm(test_date_format - date_now_format);

        $scope.previous_tests = [{
            "name": "test 1 name",
            "date": "23-07-2015",
            "your_score": 157,
            "top_score": 253,
            "total_score": 360
        }, {
            "name": "test 2 name",
            "date": "24-07-2015",
            "your_score": 114,
            "top_score": 278,
            "total_score": 360
        }, {
            "name": "test 3 name",
            "date": "25-07-2015",
            "your_score": 203,
            "top_score": 211,
            "total_score": 360
        }];


        $scope.upcoming_tests = [{
            "name": "test 1 name",
            "date": "23-07-2015"
        }, {
            "name": "test 2 name",
            "date": "24-07-2015"
        }, {
            "name": "test 3 name",
            "date": "25-07-2015"
        }];
        $scope.upcoming_tests_button = [];
        for (i = 0; i < $scope.upcoming_tests.length; i++) {
            $scope.upcoming_tests_button[i] = "REGISTER";
        }
        $scope.registerNow = function() {
            $scope.registered = true;
            $scope.popup_register_button = "DONE";
            var confirmPopup = $ionicPopup.alert({

                template: 'Thank You. You have registered for the test.'
            });
            confirmPopup.then(function(res) {

            });
        };
        $scope.viewDetails = function() {


            var confirmPopup = $ionicPopup.confirm({
                buttons: [{
                    text: 'Cancel'
                }, {
                    text: $scope.popup_register_button,
                    type: 'button-positive',
                    onTap: function(e) {
                        if (!$scope.registered) {}
                        $scope.registered = true;
                        $scope.popup_register_button = "DONE";
                    }
                }],
                templateUrl: 'popup.html'
            });
            confirmPopup.then(function(res) {
                if (res) {

                } else {

                }
            });

        };
        $scope.upcomingRegister = function(index) {
            $scope.upcoming_tests_button[index] = "REGISTERED";
        };
        $rootScope.navhider = false;
    })*/
    /*.controller('homeCtrl', function($ionicSideMenuDelegate, $scope, localstorage, $ionicPopup, $rootScope, TestService, $location) {
        $scope.overlay = true;
        $rootScope.navhider = false;
        $rootScope.doubt = false;
        $rootScope.mock = false;
        $rootScope.practice = false;

        //////console.log($rootScope.navhider);
        $scope.three_testname = "Select One";
        $scope.twenty_testname = "Select One";

        $scope.stud_id = localstorage.get("stud_id");
        // var totaltests = {};
        var j = 0;
        $scope.twenty_min = [];
        $scope.three_hour = [];
        TestService.getAllTest("http://conceptowl.com/apis/get_all_test_series.json").success(function(successData) {
            if (successData !== null) {
                $scope.overlay = false;
                //////console.log(successData);
                $scope.tests = successData;
                //  totaltests = successData;

                $scope.twenty_min[0] = successData[5];
                $scope.twenty_min[1] = successData[6];

                //  //////console.log($scope.twenty_min);
                $scope.three_tests = $scope.twenty_min;

                for (i = 7; i < 25; i++) {
                    $scope.three_hour[j] = successData[i];
                    j++;
                }
                $scope.twenty_tests = $scope.three_hour;
                //  //////console.log($scope.three_hour);

            }
        });

        $scope.three_Test = function() {
            $scope.overlay = true;
            //////console.log($scope.three_testname);
            //////console.log($scope.three_tests);
            $scope.test_time = 0;
            $scope.test_name = null;
            var testjson = {
                "studentid": parseInt($scope.stud_id)
            }
            for (var i = 0; i < $scope.three_tests.length; i++) {
                if ($scope.three_tests[i].testname == $scope.three_testname) {
                    //////console.log($scope.three_tests[i].testid);
                    testjson.testid = $scope.three_tests[i].testid;
                    $scope.test_time = $scope.three_tests[i].test_time;
                    $scope.test_name = $scope.three_tests[i].testname;
                }
            };
            TestService.postTestAnswers("/apis/create_paper.json", testjson).success(function(successData) {
                if (successData !== null) {
                    $scope.overlay = false;
                    //////console.log(successData);
                    $location.path('/test/' + successData.testid + '/' + successData.paperid + '/' + $scope.test_time + '/' + $scope.test_name);

                }
            });
        }

        $scope.twenty_Test = function() {
            $scope.overlay = true;
            //////console.log($scope.twenty_testname);
            //////console.log($scope.twenty_tests);
            $scope.test_time = 0;
            $scope.test_name = null;
            var testjson = {
                "studentid": parseInt($scope.stud_id)
            }
            for (var i = 0; i < $scope.twenty_tests.length; i++) {
                if ($scope.twenty_tests[i].testname == $scope.twenty_testname) {
                    //////console.log($scope.twenty_tests[i].testid);
                    testjson.testid = $scope.twenty_tests[i].testid;
                    $scope.test_time = $scope.twenty_tests[i].test_time;
                    $scope.test_name = $scope.twenty_tests[i].testname;
                }
            };
            TestService.postTestAnswers("apis/create_paper.json", testjson).success(function(successData) {
                if (successData !== null) {
                    $scope.overlay = false;
                    //////console.log(successData);
                    $location.path('/test/' + successData.testid + '/' + successData.paperid + '/' + $scope.test_time + '/' + $scope.test_name);

                }
            });
        }

        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.navClick = function(bol) {


            $rootScope.navhider = !$rootScope.navhider;

            //////console.log($rootScope.navhider);

        };
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };

        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
    })*/
    .controller('prevCtrl', function($ionicSideMenuDelegate, $timeout, $state, HardwareBackButtonManager, $scope, $ionicPopup, $ionicLoading, $rootScope, TestService, localstorage, appConfig) {
        $scope.overScore = false;
        $scope.overlay = true;
        $rootScope.navhider = false;
        $scope.prev_page = true;
        //////console.log($rootScope.navhider);
        $rootScope.mock = true;
        $rootScope.doubt = false;

        $scope.dataFound = false;

        $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 20000);
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $scope.serverError = false;
            $state.go('heatmap');
        }

        HardwareBackButtonManager.enable();
        /*//////console.log($rootScope.stud_id);*/
        $scope.stud_id = localstorage.get("stud_id");
        //////console.log($scope.stud_id);
        $scope.test_name = '';
        $scope.test_details = localstorage.getObject('lastTestDetails');
        //////console.log($scope.test_details);
        TestService.getAllTest(appConfig.baseUrl + "get_previous_tests.json?student_id=" + $scope.stud_id).success(function(successData) {
                if (successData !== null) {
                    $scope.dataFound = true;
                    $scope.overlay = false;
                    $scope.test_name = successData.name;
                    //////console.log(successData);
                    $scope.results = successData.previous_tests;
                    //////console.log($scope.results);
                    if (successData.previous_tests.length == 0) {
                        $scope.prev_page = false;
                    }

                    for (var i = 0; i < $scope.results.length; i++) {
                        for (var j = 0; j < $scope.results[i].categories.length; j++) {
                            $scope.results[i].categories[j].hou = Math.floor($scope.results[i].categories[j].total_time / 3600);
                            $scope.results[i].categories[j].min = Math.floor(($scope.results[i].categories[j].total_time / 60) % 60);
                            $scope.results[i].categories[j].sec = $scope.results[i].categories[j].total_time % 60;
                        };

                    };

                }
            })
            .error(function(err) {
                $scope.overlay = false;
                $scope.serverError = true;
                $scope.dataFound = true;
            })

        $rootScope.practice = false;
        $scope.hideview = function() {
            $scope.nav_views = true;
        }
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.navClick = function(bol) {


            $rootScope.navhider = !$rootScope.navhider;

            //////console.log($rootScope.navhider);

        };
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };

        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
        $scope.hide_nav = function() {
                $rootScope.navhider = false;
            }
            /*$scope.log_out = function() {
                $scope.nav_views = true;
                localstorage.set('logged', false);
            }*/
    })
    .controller('upCtrl', function($ionicSideMenuDelegate, HardwareBackButtonManager, PayService, $timeout, $state, $ionicHistory, $scope, $ionicPopup, $location, $ionicLoading, $rootScope, TestService, localstorage, appConfig, $interval) {
        $scope.overScore = false;
        $rootScope.navhider = false;
        $scope.overlay = true;
        $scope.dataFound = false;

        $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 10000);
        $scope.goToLogin = function() {
                $scope.timeOut = false;
                $state.go('heatmap');
            }
            //////console.log($rootScope.navhider);
        $rootScope.mock = true;
        $rootScope.doubt = false;
        $scope.recent_test = false;
        $scope.no_recent_test = false;

        HardwareBackButtonManager.enable();
        $scope.hide_nav = function() {
            $rootScope.navhider = false;
        }
        $ionicHistory.nextViewOptions({
            disableAnimate: true
        });

        /*$interval(function(){
            $state.go($state.current, {}, {reload: true});
        },2000)*/

        /*$scope.log_out = function() {
            $scope.nav_views = true;
            localstorage.set('logged', false);
        }*/

        /*$timeout(function() {
            if ($scope.tests !== null) {

            } else {
                $scope.overlay = false;
                $ionicHistory.goBack();
            }
        }, 2000);*/
        $scope.stud_id = localstorage.get("stud_id");

        /* TestService.getUpTest("http://conceptowl.com/apis/get_recent_upcoming_tests.json?student_id=" + $scope.stud_id).success(function(successData) {
             if (successData !== null) {
               //  $scope.overlay = false;
                 //////console.log(successData);

                 if (Object.keys(successData.upcoming_test).length === 0) {
                     $scope.no_recent_test = true;
                     $scope.recent_test = false;
                 } else {
                     $scope.recent_test = true;
                     $scope.no_recent_test = false;
                     $scope.upcoming = successData.upcoming_test;
                 }
             }
         });*/

        /*  TestService.getUpTest("http://conceptowl.com/apis/get_upcoming_tests.json?student_id=" + $scope.stud_id).success(function(successData) {
              if (successData !== null) {
                  $scope.overlay = false;

                  $scope.results = successData;
                  //////console.log($scope.results);
              }
          });*/

        TestService.getAllTest("http://conceptowl.com/apis/get_all_test_series.json?studentid=" + $scope.stud_id).success(function(successData) {
                if (successData !== null) {
                    $scope.overlay = false;
                    $scope.dataFound = true;
                    console.log(successData);
                    $scope.tests = successData;
                    $scope.primar_test = {};
                    $scope.maintest = [];

                    for (var i = 0; i < successData.length; i++) {
                        if (successData[i].testtype === "main_test") {
                            $scope.primary_test = successData[i];
                        }
                    };
                    var j = 0;
                    for (var i = 0; i < successData.length; i++) {
                        if (successData[i].testtype !== "main_test") {
                            $scope.maintest[j] = successData[i];
                            j++;
                        }
                    };
                    //////console.log($scope.primary_test);

                    //////console.log($scope.maintest);
                    $scope.results = $scope.maintest;
                }

            })
            .error(function(e) {
                $timeout(function() {
                    $scope.overlay = false;
                    $ionicHistory.goBack();
                }, 2000);
            })

        $scope.testpop = function(id) {
            //////console.log(id);
            $scope.ind = id;
        };

        /*  $scope.registerExam = function() {
            $scope.butt_text = "Start the test";
            $scope.register = true;
        }

        */

        /* $scope.registerExam = function() {
             $scope.butt_text='Start the Test';
             $scope.register=true;
             var answerJson = {
                 "studentid": "406",
                 "id": "6"
             };
             TestService.postTestAnswers("/apis/register_for_test.json", answerJson).success(function(successData) {
                 if (successData !== null) {
                     //////console.log(successData);
                 }
             });
         }*/

        $scope.stopLoader = function() {
            $scope.dataFound = false;

            $timeout(function() {
                if ($scope.dataFound === false) {
                    $scope.overlay = false;
                    $scope.timeOut = true;
                }
            }, 10000);
            $scope.goToLogin = function() {
                $scope.timeOut = false;
            }
        }
        $scope.registerExam = function() {
            //  $scope.nav_views=true;
            $scope.stopLoader();
            $scope.overlay = true;
            var testjson = {
                "studentid": parseInt($scope.stud_id)
            }
            testjson.testid = $scope.primary_test.testid;
            TestService.postTestAnswers("/apis/create_paper.json", testjson).success(function(successData) {
                if (successData !== null) {
                    $scope.dataFound = true;
                    $scope.overlay = false;
                    $scope.overlay = false;
                    //////console.log(successData);
                    $location.path('/test/' + successData.testid + '/' + successData.paperid + '/' + $scope.primary_test.test_time + '/' + $scope.primary_test.testname);

                }
            });
        }


        $scope.reg_test = function(id) {
            // $scope.nav_views=true;
            $scope.stopLoader();
            $scope.overlay = true;
            var testjson = {
                "studentid": parseInt($scope.stud_id)
            }
            testjson.testid = $scope.results[id].testid;
            TestService.postTestAnswers("/apis/create_paper.json", testjson).success(function(successData) {
                if (successData !== null) {
                    $scope.dataFound = true;
                    $scope.overlay = false;
                    //////console.log(successData);
                    $location.path('/test/' + successData.testid + '/' + successData.paperid + '/' + $scope.results[id].test_time + '/' + $scope.results[id].testname);

                }
            });

        }

        $scope.getOrderStatus = function(win) {
            $scope.orderTimer = $interval(function() {
                //console.log("test");
                PayService.getStatus(616, $scope.stud_id).success(function(successData) {
                    //console.log(successData);
                    if (successData.status === "0") {
                        $state.go($state.current, {}, {
                            reload: true
                        });
                    }
                });
            }, 10000)

            $timeout(function() {
                $interval.cancel($scope.orderTimer);
                win.close();
            }, 60000 * 10)
        }

        $scope.unlock = function() {
            //$scope.payment = true;
            var openedWindow = window.open("", "_system", "location=yes");
            openedWindow.document.body.innerHTML = "<html><body><p style='color: white;margin: auto;bottom: 30px;position: absolute;top: 0;max-height:55px;font-size: 30px;max-width: 140px;right: 0;left: 0;padding-top: 20px;padding-left: 15px;border-radius: 25px;background-color:rgb(71, 72, 79);'>Loading....</p></body></html>";
            $scope.getOrderStatus(openedWindow);

            $scope.overlay = true;
            var payObj = {};
            payObj.product_id = 616;
            payObj.product_type = "AllFeatures";
            payObj.user_id = $scope.stud_id;
            PayService.setPayment("/apis/create_order.json", payObj).success(function(successData) {
                console.log(successData);
                if (successData.status === "success") {
                    // $location.path('/pay/' + successData.order_id);
                    var amountObj = {};
                    amountObj.amount = 4499;
                    amountObj.order_id = successData.order_id;
                    PayService.proceedPayment("/apis/Pay", amountObj).success(function(paymentpage) {
                        $scope.overlay = false;
                        console.log(paymentpage);
                        $scope.paynow(openedWindow, paymentpage);

                        /*openedWindow.addEventListener("load", function() {
                            //console.log("received load event");
                            openedWindow.resizeTo(250, 250);
                            
                            //openedWindow.close();
                        }, false);*/
                        //$ionicHistory.goBack();
                    });
                }
            });
        }
        $scope.cancel = function() {
            //$scope.payment = false;
        }
        $scope.paynow = function(windows, page) {

            windows.document.body.innerHTML = page;

        }
        $scope.log_out = function() {
            localstorage.set('logged', false);

        }

        $rootScope.practice = false;
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.navClick = function(bol) {


            $rootScope.navhider = !$rootScope.navhider;

            //////console.log($rootScope.navhider);

        };
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };

        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
    })
    .controller('thankCtrl', function($scope, $rootScope, $timeout, HardwareBackButtonManager, $stateParams, localstorage, TestService, appConfig) {

        //////console.log($stateParams.paper_id);

        $scope.paper_id = $stateParams.paper_id;
        $scope.test_details = localstorage.getObject('lastTestDetails');
        $scope.stud_id = localstorage.get('stud_id');
        $scope.tot_time = 0;
        $scope.min = 00;
        $scope.sec = 00;
        var hour;
        var min;
        var sec;
        $scope.overlay = true;
        HardwareBackButtonManager.disable();

        $scope.dataFound = false;

        $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 10000);
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $state.go('heatmap');
        }

        $scope.hide_nav = function() {
                $rootScope.navhider = false;
            }
            /*$scope.log_out = function() {
                $scope.nav_views = true;
                localstorage.set('logged', false);
            }*/
        TestService.getAllTest("http://conceptowl.com/apis/get_test_result.json?student_id=" + $scope.stud_id + "&paper_id=" + $scope.paper_id).success(function(successData) {
            if (successData !== null) {
                $scope.dataFound = true;
                $scope.overlay = false;
                console.log(successData);

                for (var j = 0; j < successData.categories.length; j++) {
                    if (successData.categories[j].subject === 'Math') {
                        successData.categories[j].subject = 'Mathematics';
                    }
                    hour = Math.floor(successData.categories[j].total_time / 3600);
                    if (hour < 10) {
                        hour = "0" + hour;
                        //////console.log(hour);
                    }
                    min = Math.floor((successData.categories[j].total_time / 60) % 60);
                    if (min < 10) {
                        min = "0" + min;
                        //////console.log(min);
                    }
                    sec = successData.categories[j].total_time % 60;
                    if (sec < 10) {
                        sec = "0" + sec;
                        //////console.log(sec);
                    }
                    successData.categories[j].hou = hour;
                    successData.categories[j].min = min;
                    successData.categories[j].sec = sec;
                };
                //////console.log(successData);
                $scope.results = successData
                $scope.hou = Math.floor(successData.test_time / 3600);
                $scope.min = Math.floor((successData.test_time / 60) % 60);
                $scope.sec = successData.test_time % 60;
            }
        });

        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
        $scope.navClick = function(bol) {
            $rootScope.navhider = !$rootScope.navhider;
            //////console.log($rootScope.navhider);
        };
        $scope.testDetails = localstorage.getObject('lastTestDetails');
    })
    .controller('practViewCtrl', function($scope, $rootScope, $timeout, HardwareBackButtonManager, $state, PracticeService, localstorage, appConfig) {


        $rootScope.navhider = false;
        $scope.overlay = true;
        $rootScope.practice = true;
        $scope.stud_id = localstorage.get("stud_id");


        $scope.dataFound = false;

        $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 10000);
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $state.go('heatmap');
        }

        HardwareBackButtonManager.enable();
        // //////console.log($scope.stud_id);
        PracticeService.getPracticeTest($scope.stud_id).success(function(successData) {
                if (successData !== null) {
                    $scope.dataFound = true;
                    $scope.overlay = false;
                    console.log(successData);
                    for (var i = 0; i < successData.length; i++) {
                        if (successData[i].subject == 'physics') {
                            successData[i].subject = 'PHYSICS';
                        } else if (successData[i].subject == 'chemistry') {
                            successData[i].subject = 'CHEMISTRY';
                        } else if (successData[i].subject == 'math') {
                            successData[i].subject = 'MATH';
                        }
                    };
                    /*successData.sort(function(a, b) {
                        return a.date - b.date
                    });*/
                    //successData.reverse();
                    //////console.log(successData);
                    $scope.tests = successData;
                }
            })
            .error(function() {
                //////console.log("dsfsdf");
            })

        $scope.hideview = function() {
            $scope.nav_views = true;
        }

        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
        $scope.navClick = function(bol) {
            $rootScope.navhider = !$rootScope.navhider;
            //////console.log($rootScope.navhider);
        };
        $scope.hide_nav = function() {
            $rootScope.navhider = false;
        }
    })
    /* .controller('loginCtrl', function($scope, LoginService, RegisterService, $location, $rootScope, localstorage) {

        $scope.logging = localstorage.get('logged');
        //////console.log($scope.logging);
        if ($scope.logging == "true") {
            //////console.log("done");
            $location.path('/upcoming');
        }
        $scope.login_reg = function() {
            $scope.user_log = {
                username: "",
                password: "",
            };
            $scope.user_reg = {
                name: "",
                email: "",
                phone: null,
                password: "",
            };
        }
        $scope.register = true;
        $scope.login = false;
        $scope.reg_concept = function() {
            $scope.register = false;
            $scope.login = true;
            $scope.user_reg.name = "";
            $scope.user_reg.email = "";
            $scope.user_reg.phone = null;
            $scope.user_reg.password = null;
        }
        $scope.login_concept = function() {
            $scope.login = false;
            $scope.register = true;
        }
        $scope.submit_login = function() {
            $scope.overlay = true;
            $scope.loginJson = {};
            $rootScope.stud_id = 0;
            $scope.loginJson.username = $scope.user_log.username;
            $scope.loginJson.password = $scope.user_log.password;

            LoginService.postLogin("/apis/student_login.json", $scope.loginJson).success(function(successData) {
                    if (successData !== null) {
                        $scope.overlay = false;
                        //////console.log(successData);

                        localstorage.set("stud_id", successData.studentid);
                        localstorage.set("logged", true);
                        $scope.loginview = true;
                        $location.path('/upcoming');

                    }
                })
                .error(function() {
                    $scope.logConfirm();
                })

        }
        $scope.submit_register = function() {


            $scope.overlay = true;
            $scope.registerJson = {};
 

            $scope.registerJson.name = $scope.user_reg.name;
            $scope.registerJson.email = $scope.user_reg.email;
            $scope.registerJson.phone = $scope.user_reg.phone;
            $scope.registerJson.password = $scope.user_reg.password;

            RegisterService.postRegister("apis/student_registration.json", $scope.registerJson).success(function(successData) {
                    if (successData !== null) {

                        $scope.overlay = false;

                        //////console.log(successData);
                        if (successData.status == 0) {
                            $scope.regConfirm();
                        } else if (successData.status == 1) {
                            $scope.finishreg();
                        }
                        $location.path('/login');
                    }
                })
                .error(function() {
                    // //////console.log("dsfsdf");
                    $scope.regConfirm();
                })
        }
        $scope.regConfirm = function() {

            $scope.overlay = false;
            $scope.regSubmit = true;
        };
        $scope.closeregConfirm = function() {
            $scope.overlay = false;
            $scope.regSubmit = false;
        };
        $scope.logConfirm = function() {
            $scope.overlay = false;
            $scope.logSubmit = true;
        };
        $scope.closelogConfirm = function() {
            $scope.overlay = false;
            $scope.logSubmit = false;
        };
        $scope.finishreg = function() {
            $scope.overlay = false;
            $scope.finishSubmit = true;
        }
        $scope.closefinishreg = function() {
            $scope.overlay = false;
            $scope.finishSubmit = false;
        }
    })*/
    .controller('newLoginCtrl', function($scope, $stateParams, ngFB, GooglePlus, $http, $state, $window, $q, $rootScope, $timeout, FACEBOOK_APP_ID, $ionicScrollDelegate, HardwareBackButtonManager, LoginService, localstorage, $location, TestService, appConfig) {
        // $ionicScrollDelegate.scrollTop();
        $scope.input_type = "password";

        // HardwareBackButtonManager.disable();
        $scope.topic = $stateParams.name;
        $scope.subject = $stateParams.sub;
        $scope.mode = $stateParams.mode;
        $scope.TestName = $stateParams.name;
        $scope.TestTime = $stateParams.sub;
        $scope.TestId = $stateParams.id;



        $scope.errors = false;
        $scope.user_log = {
            username: "",
            password: "",
        };

        $scope.hideview = function() {
            $scope.login_view = true;
        }
        $scope.stopLoader = function() {
            $scope.dataFound = false;

            $scope.loaderTimer = $timeout(function() {
                if ($scope.dataFound === false) {
                    $scope.overlay = false;
                    $scope.timeOut = true;
                }
            }, 10000);

        }
        $scope.goToLogin = function() {
            $scope.timeOut = false;
        }
        $scope.selectInput = function() {
            if ($scope.showPassword == true) {
                $scope.input_type = "text";
            } else {
                $scope.input_type = "password";
            }
        }
        $scope.submit_login = function() {

            $scope.overlay = true;
            $scope.loginJson = {};
            $scope.prof_data = {};
            $scope.loginJson.email = $scope.user_log.username;
            $scope.loginJson.password = $scope.user_log.password;

            $scope.email_error = false;
            $scope.pass_error = false;

            if ($scope.user_log.username === "" || $scope.user_log.username === undefined) {
                $scope.email_error = true;
                $scope.overlay = false;
            }
            if ($scope.user_log.password === "" || $scope.user_log.password === undefined) {
                $scope.pass_error = true;
                $scope.overlay = false;
            }
            if ($scope.user_log.username !== "" && $scope.user_log.username !== undefined && $scope.user_log.password !== "" && $scope.user_log.password !== undefined) {
                $scope.stopLoader();
                LoginService.postLogin("/apis/student_login.json", $scope.loginJson).success(function(successData) {
                        if (successData !== null) {
                            $timeout.cancel($scope.loaderTimer);
                            $scope.dataFound = true;
                            ////console.log(successData);
                            $rootScope.logg_type = 0;
                            localstorage.set("stud_id", successData.studentid);
                            $rootScope.prof_imag = 'images/profile.png';
                            $rootScope.prof_name = successData.first_name;
                            localstorage.set("logg_type", 0);
                            localstorage.set("logged", true);
                            $scope.loginview = true;
                            $scope.prof_data.prof_imag = 'images/profile.png';
                            $scope.prof_data.prof_name = successData.first_name;;
                            localstorage.set("profile", JSON.stringify($scope.prof_data));
                            if ($scope.mode === 'preview') {
                                $location.path('/practice/' + $scope.topic + '/' + $scope.subject + '/' + '0000/practice');
                            } else if ($scope.mode === 'test_preview') {
                                var testjson = {
                                    "studentid": successData.studentid
                                }
                                testjson.testid = $scope.TestId;
                                TestService.postTestAnswers("/apis/create_paper.json", testjson).success(function(successData) {
                                    if (successData !== null) {
                                        $scope.overlay = false;
                                        $scope.dataFound = true;
                                        ////console.log(successData);
                                        $location.path('/test/' + successData.testid + '/' + successData.paperid + '/' + $scope.TestTime + '/' + $scope.TestName);
                                    }
                                });
                            } else {
                                $scope.overlay = false;
                                $location.path('/heatmap');
                            }
                        }
                    })
                    .error(function(err) {
                        $timeout.cancel($scope.loaderTimer);
                        $scope.dataFound = true;
                        if (err.status === "500") {
                            $scope.user_log.username = "";
                            $scope.user_log.password = "";
                            $scope.login_error = "Wrong username or password";
                            $scope.overlay = false;
                            ////console.log(err);
                            $scope.logSubmit = true;

                        } else {
                            $scope.user_log.password = "";
                            $scope.pass_error = true;
                            $scope.overlay = false;
                            ////console.log(err);
                        }
                    })
            }
            //////console.log($scope.loginJson);


        }

        $scope.closelogConfirm = function() {
            $scope.overlay = false;
            $scope.logSubmit = false;
        };

        $scope.fb_login = function() {
            ngFB.login({
                scope: 'email'
            }).then(
                function(response) {
                    if (response.status === 'connected') {
                        // console.log(response);
                        $scope.init(response.authResponse.accessToken);
                        //////console.log('Facebook login succeeded');
                        // $scope.closeLogin();
                    } else {
                        alert('Facebook login failed');
                    }
                });
        };
        $scope.init = function(tok) {
            $scope.overlay = true;
            $http.get("https://graph.facebook.com/v2.2/me", {
                params: {
                    access_token: tok,
                    fields: "id,name,gender,location,picture,email",
                    format: "json"
                }
            }).then(function(result) {
                //////console.log(result);
                $scope.face_loginJson = {};
                $scope.prof_data = {};
                $scope.face_loginJson.name = result.data.name;
                $scope.face_loginJson.email = result.data.email;
                $scope.face_loginJson.social_login = "facebook";
                $scope.face_loginJson.fbid = result.data.id;
                $scope.prof_data.prof_imag = result.data.picture.data.url;
                $scope.prof_data.prof_name = result.data.name;
                localstorage.set("profile", JSON.stringify($scope.prof_data));
                $rootScope.prof_imag = result.data.picture.data.url;
                $rootScope.prof_name = result.data.name;
                //////console.log($scope.face_loginJson);
                $scope.stopLoader();
                LoginService.postLogin("/apis/student_login.json", $scope.face_loginJson).success(function(successData) {
                        if (successData !== null) {
                            $scope.dataFound = true;
                            $timeout.cancel($scope.loaderTimer);
                            //////console.log(successData);

                            localstorage.set("stud_id", successData.studentid);
                            localstorage.set("logged", true);
                            localstorage.set("logg_type", 1);
                            $rootScope.logg_type = 1;
                            $scope.loginview = true;
                            if ($scope.mode === 'preview') {
                                $scope.overlay = false;
                                $location.path('/practice/' + $scope.topic + '/' + $scope.subject + '/' + '0000/practice');
                            } else if ($scope.mode === 'test_preview') {
                                var testjson = {
                                    "studentid": successData.studentid
                                }
                                testjson.testid = $scope.TestId;
                                TestService.postTestAnswers("/apis/create_paper.json", testjson).success(function(successData) {
                                    if (successData !== null) {
                                        $scope.overlay = false;
                                        $scope.dataFound = true;
                                        ////console.log(successData);
                                        $location.path('/test/' + successData.testid + '/' + successData.paperid + '/' + $scope.TestTime + '/' + $scope.TestName);
                                    }
                                });
                            } else {
                                $scope.overlay = false;
                                $location.path('/heatmap');
                            }

                        }
                    })
                    .error(function() {
                        $scope.overlay = false;
                        $scope.dataFound = true;
                        $timeout.cancel($scope.loaderTimer);
                    })
                    //  $scope.profileData = result.data;
            }, function(error) {
                alert("There was a problem getting your profile.  Check the logs for details.");
                //////console.log(error);
            });
        };

        /*  var fbLoginSuccess = function(response) {
            if (!response.authResponse) {
                fbLoginError("Cannot find the authResponse");
                return;
            }
            //////console.log('fbLoginSuccess');

            var authResponse = response.authResponse;

            getFacebookProfileInfo(authResponse)
                .then(function(profileInfo) {

                    //////console.log('profile info success', profileInfo);


                    $scope.face_loginJson = {};
                    $scope.prof_data = {};
                    $scope.face_loginJson.name = profileInfo.name;
                    $scope.face_loginJson.email = profileInfo.email;
                    $scope.face_loginJson.social_login = "facebook";
                    $scope.face_loginJson.fbid = profileInfo.id;
                    $scope.prof_data.prof_imag = "http://graph.facebook.com/" + authResponse.userID + "/picture?type=small";
                    $scope.prof_data.prof_name = profileInfo.name;
                    localstorage.set("profile", JSON.stringify($scope.prof_data));
                    $rootScope.prof_imag = "http://graph.facebook.com/" + authResponse.userID + "/picture?type=small";
                    $rootScope.prof_name = profileInfo.name;
                    //////console.log($scope.face_loginJson);
                    LoginService.postLogin("/apis/student_login.json", $scope.face_loginJson).success(function(successData) {
                            if (successData !== null) {
                                $scope.overlay = false;
                                //////console.log(successData);
                                $rootScope.logg_type = 1;
                                localstorage.set("stud_id", successData.studentid);
                                localstorage.set("logg_type", 1);
                                localstorage.set("logged", true);
                                $scope.loginview = true;
                                $location.path('/heatmap');

                            }
                        })
                        .error(function() {

                            $scope.overlay = false;

                        })
                }, function(fail) {
                    //fail get profile info
                    //////console.log('profile info fail', fail);
                });
        };


        var fbLoginReadySuccess = function(response) {
            if (!response.authResponse) {
                fbLoginError("Cannot find the authResponse");
                return;
            }
            //////console.log('fbLoginSuccess');

            var authResponse = response.authResponse;

            getFacebookProfileInfo(authResponse)
                .then(function(profileInfo) {
                    //////console.log('profile info success', profileInfo);
                    $scope.face_loginJson = {};
                    $scope.prof_data = {};
                    $scope.face_loginJson.name = profileInfo.name;
                    $scope.face_loginJson.email = profileInfo.email;
                    $scope.face_loginJson.social_login = "facebook";
                    $scope.face_loginJson.fbid = profileInfo.id;
                    $scope.prof_data.prof_imag = "http://graph.facebook.com/" + authResponse.userID + "/picture?type=small";
                    $scope.prof_data.prof_name = profileInfo.name;
                    localstorage.set("profile", JSON.stringify($scope.prof_data));
                    $rootScope.prof_imag = "http://graph.facebook.com/" + authResponse.userID + "/picture?type=small";
                    $rootScope.prof_name = profileInfo.name;
                    //////console.log($scope.face_loginJson);
                    LoginService.postLogin("/apis/student_login.json", $scope.face_loginJson).success(function(successData) {
                            if (successData !== null) {
                                $scope.overlay = false;
                                //////console.log(successData);
                                $rootScope.logg_type = 1;
                                localstorage.set("stud_id", successData.studentid);
                                localstorage.set("logg_type", 1);
                                localstorage.set("logged", true);
                                $scope.loginview = true;
                                $location.path('/heatmap');

                            }
                        })
                        .error(function() {

                            $scope.overlay = false;

                        })
                }, function(fail) {

                    //////console.log('profile info fail', fail);
                });
        };


        var fbLoginError = function(error) {
            //////console.log('fbLoginError');
        };

        var getFacebookProfileInfo = function(authResponse) {
            var info = $q.defer();

            facebookConnectPlugin.api('/me?fields=about,bio,birthday,email,name&access_token=' + authResponse.accessToken, null,
                function(response) {
                    info.resolve(response);
                },
                function(response) {
                    info.reject(response);
                }
            );
            return info.promise;
        }

        $scope.fb_login = function() {
            $scope.overlay = true;
            if (!window.cordova) {
                $scope.overlay = false;
                facebookConnectPlugin.browserInit(FACEBOOK_APP_ID);
            }

            facebookConnectPlugin.getLoginStatus(function(success) {
                if (success.status === 'connected') {
                    //////console.log('getLoginStatus', success.status);
                    facebookConnectPlugin.login(['email', 'public_profile', 'user_about_me', 'user_likes', 'user_location', 'user_posts', 'user_status', 'user_birthday', 'user_photos'], fbLoginReadySuccess, fbLoginError);
                } else {
                    //////console.log('getLoginStatus', success.status);
                    facebookConnectPlugin.login(['email', 'public_profile', 'user_about_me', 'user_likes', 'user_location', 'user_posts', 'user_status', 'user_birthday', 'user_photos'], fbLoginSuccess, fbLoginError);
                }
            });
        }*/
        $scope.google_login = function() {
            GooglePlus.login().then(function(authResult) {
                ////console.log(authResult);

                GooglePlus.getUser().then(function(user) {
                    ////console.log(user);
                    $scope.overlay = true;
                    $scope.goog_loginJson = {};
                    $scope.prof_data = {};
                    $scope.prof_data.prof_imag = user.picture;
                    $scope.prof_data.prof_name = user.name;
                    localstorage.set("profile", JSON.stringify($scope.prof_data));
                    $scope.goog_loginJson.email = user.email;
                    $scope.goog_loginJson.googleid = user.id;
                    $scope.goog_loginJson.name = user.name;
                    $scope.goog_loginJson.social_login = "google";

                    $scope.stopLoader();
                    LoginService.postLogin("/apis/student_login.json", $scope.goog_loginJson).success(function(successData) {
                            if (successData !== null) {
                                $scope.dataFound = true;
                                $timeout.cancel($scope.loaderTimer);
                                $rootScope.prof_imag = user.picture;
                                $rootScope.prof_name = user.name;
                                localstorage.set("stud_id", successData.studentid);
                                localstorage.set("logged", true);
                                localstorage.set("logg_type", 2);
                                $rootScope.logg_type = 2;
                                $scope.loginview = true;
                                if ($scope.mode === 'preview') {
                                    $scope.overlay = false;
                                    $location.path('/practice/' + $scope.topic + '/' + $scope.subject + '/' + '0000/practice');
                                } else if ($scope.mode === 'test_preview') {
                                    var testjson = {
                                        "studentid": successData.studentid
                                    }
                                    testjson.testid = $scope.TestId;
                                    TestService.postTestAnswers("/apis/create_paper.json", testjson).success(function(successData) {
                                        if (successData !== null) {
                                            $scope.overlay = false;
                                            $scope.dataFound = true;
                                            ////console.log(successData);
                                            $location.path('/test/' + successData.testid + '/' + successData.paperid + '/' + $scope.TestTime + '/' + $scope.TestName);
                                        }
                                    });
                                } else {
                                    $scope.overlay = false;
                                    $location.path('/heatmap');
                                }
                            }
                        })
                        .error(function() {
                            $scope.dataFound = true;
                            $timeout.cancel($scope.loaderTimer);
                            $scope.overlay = false;
                        })
                });
            }, function(err) {
                ////console.log(err);
            });
        }

        /* $scope.google_login = function() {
            
             window.plugins.googleplus.login({},
                 function(user_data) {
                     //////console.log(user_data);
                     $scope.overlay = true;
                     $scope.goog_loginJson = {};
                     $scope.prof_data = {};
                     $scope.prof_data.prof_imag = user_data.imageUrl;
                     $scope.prof_data.prof_name = user_data.displayName;
                     localstorage.set("profile", JSON.stringify($scope.prof_data));
                     $scope.goog_loginJson.email = user_data.email;
                     $scope.goog_loginJson.googleid = user_data.userId;
                     $scope.goog_loginJson.name = user_data.displayName;
                     $scope.goog_loginJson.social_login = "google";

                     //////console.log($scope.goog_loginJson);
                     LoginService.postLogin("http://conceptowl.com/apis/student_login.json", $scope.goog_loginJson).success(function(successData) {
                             if (successData !== null) {
                                 $scope.overlay = false;
                                 //////console.log(successData);
                                 $rootScope.prof_imag = user_data.imageUrl;
                                 $rootScope.prof_name = user_data.displayName;
                                 localstorage.set("stud_id", successData.studentid);
                                 $rootScope.logg_type = 2;
                                 localstorage.set("logg_type", 2);
                                 localstorage.set("logged", true);
                                 $scope.loginview = true;
                                 $location.path('/heatmap');
                             }
                         })
                         .error(function() {
                             $scope.overlay = false;
                         })
                 },
                 function(msg) {
                     //////console.log(msg);
                 }
             );
         };*/
    })
    .controller('newRegisterCtrl', function($scope, $http, $timeout, ngFB, GooglePlus, $state, $window, $q, $rootScope, $ionicScrollDelegate, HardwareBackButtonManager, FACEBOOK_APP_ID, RegisterService, localstorage, $location, $interval, LoginService, appConfig) {
        $scope.match = true;
        $scope.errors = false;
        $scope.initial = true;
        /*$scope.not_match = false;*/
        $scope.input_type = "password";
        $scope.user_reg = {
            name: "",
            email: "",
            username: "",
            phone: null,
            password: "",
            repassword: ""
        };
        $scope.stopLoader = function() {
            $scope.dataFound = false;

            $scope.loaderTimer = $timeout(function() {
                if ($scope.dataFound === false) {
                    $scope.overlay = false;
                    $scope.timeOut = true;
                }
            }, 10000);

        }
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $state.go('newregister');
        }
        $scope.selectInput = function() {
            if ($scope.showPassword === true) {
                $scope.input_type = "text";
            } else {
                $scope.input_type = "password";
            }
        }
        HardwareBackButtonManager.enable();
        /* $scope.checkpass = function() {
             $scope.checkpassword = $interval(function() {
                 if ($scope.user_reg.password != "" && $scope.user_reg.repassword != "") {
                     if ($scope.user_reg.password == $scope.user_reg.repassword) {
                         $scope.match = false;
                         $interval.cancel($scope.checkpassword);
                         $scope.checkpass();
                     } else {
                        
                         $scope.match = true;
                         
                     }
                 }
             }, 100);
         }
         $scope.checkpass();*/

        $scope.fb_login = function() {
            ngFB.login({
                scope: 'email'
            }).then(
                function(response) {
                    if (response.status === 'connected') {
                        //////console.log(response);
                        $scope.init(response.authResponse.accessToken);
                        //////console.log('Facebook login succeeded');
                        // $scope.closeLogin();
                    } else {
                        alert('Facebook login failed');
                    }
                });
        };
        $scope.init = function(tok) {
            $scope.overlay = true;
            $http.get("https://graph.facebook.com/v2.2/me", {
                params: {
                    access_token: tok,
                    fields: "id,name,gender,location,picture,email",
                    format: "json"
                }
            }).then(function(result) {
                //////console.log(result);
                $scope.face_loginJson = {};
                $scope.prof_data = {};
                $scope.face_loginJson.name = result.data.name;
                $scope.face_loginJson.email = result.data.email;
                $scope.face_loginJson.social_login = "facebook";
                $scope.face_loginJson.platform = "website";
                $scope.face_loginJson.fbid = result.data.id;
                $scope.prof_data.prof_imag = result.data.picture.data.url;
                $scope.prof_data.prof_name = result.data.name;
                localstorage.set("profile", JSON.stringify($scope.prof_data));
                $rootScope.prof_imag = result.data.picture.data.url;
                $rootScope.prof_name = result.data.name;
                //////console.log($scope.face_loginJson);
                $scope.stopLoader();
                LoginService.postLogin("/apis/student_login.json", $scope.face_loginJson).success(function(successData) {
                        if (successData !== null) {
                            $scope.dataFound = true;
                            $timeout.cancel($scope.loaderTimer);
                            //////console.log(successData);

                            localstorage.set("stud_id", successData.studentid);
                            localstorage.set("logged", true);
                            localstorage.set("logg_type", 1);
                            $rootScope.logg_type = 1;
                            $scope.loginview = true;
                            if ($scope.mode === 'preview') {
                                $location.path('/practice/' + $scope.topic + '/' + $scope.subject + '/' + '0000/practice');
                            } else if ($scope.mode === 'test_preview') {
                                var testjson = {
                                    "studentid": successData.studentid
                                }
                                testjson.testid = $scope.TestId;
                                TestService.postTestAnswers("/apis/create_paper.json", testjson).success(function(successData) {
                                    if (successData !== null) {
                                        $scope.overlay = false;
                                        $scope.dataFound = true;
                                        ////console.log(successData);
                                        $location.path('/test/' + successData.testid + '/' + successData.paperid + '/' + $scope.TestTime + '/' + $scope.TestName);
                                    }
                                });
                            } else {
                                $scope.overlay = false;
                                $location.path('/heatmap');
                            }

                        }
                    })
                    .error(function() {
                        // $scope.errors = true;
                        $scope.overlay = false;
                        //$scope.logConfirm();
                    })
                    //  $scope.profileData = result.data;
            }, function(error) {
                alert("There was a problem getting your profile.  Check the logs for details.");
                //////console.log(error);
            });
        };


        $scope.google_login = function() {
            GooglePlus.login().then(function(authResult) {
                ////console.log(authResult);

                GooglePlus.getUser().then(function(user) {
                    ////console.log(user);
                    $scope.overlay = true;
                    $scope.goog_loginJson = {};
                    $scope.prof_data = {};
                    $scope.prof_data.prof_imag = user.picture;
                    $scope.prof_data.prof_name = user.name;
                    localstorage.set("profile", JSON.stringify($scope.prof_data));
                    $scope.goog_loginJson.email = user.email;
                    $scope.goog_loginJson.googleid = user.id;
                    $scope.goog_loginJson.name = user.name;
                    $scope.goog_loginJson.social_login = "google";
                    $scope.goog_loginJson.platform = "website";

                    $scope.stopLoader();
                    LoginService.postLogin("/apis/student_login.json", $scope.goog_loginJson).success(function(successData) {
                            if (successData !== null) {
                                $scope.dataFound = true;
                                $timeout.cancel($scope.loaderTimer);
                                $rootScope.prof_imag = user.picture;
                                $rootScope.prof_name = user.name;
                                localstorage.set("stud_id", successData.studentid);
                                localstorage.set("logged", true);
                                localstorage.set("logg_type", 2);
                                $rootScope.logg_type = 2;
                                $scope.loginview = true;
                                if ($scope.mode === 'preview') {
                                    $location.path('/practice/' + $scope.topic + '/' + $scope.subject + '/' + '0000/practice');
                                } else if ($scope.mode === 'test_preview') {
                                    var testjson = {
                                        "studentid": successData.studentid
                                    }
                                    testjson.testid = $scope.TestId;
                                    TestService.postTestAnswers("/apis/create_paper.json", testjson).success(function(successData) {
                                        if (successData !== null) {
                                            $scope.overlay = false;
                                            $scope.dataFound = true;
                                            ////console.log(successData);
                                            $location.path('/test/' + successData.testid + '/' + successData.paperid + '/' + $scope.TestTime + '/' + $scope.TestName);
                                        }
                                    });
                                } else {
                                    $scope.overlay = false;
                                    $location.path('/heatmap');
                                }
                            }
                        })
                        .error(function() {
                            $scope.overlay = false;
                        })
                });
            }, function(err) {
                ////console.log(err);
            });
        }


        $scope.submit_register = function() {


            $scope.overlay = true;
            $scope.errors = false;
            $scope.initial = true;
            $scope.email_error = false;
            $scope.name_error = false;
            $scope.pass_error = false;
            $scope.mobile_error = false;

            $scope.registerJson = {};
            $scope.mainregisterJson = {};
            $scope.prof_data = {};

            $scope.mainregisterJson.name = $scope.user_reg.name;
            $scope.mainregisterJson.email = $scope.user_reg.email;
            // $scope.registerJson.username = $scope.user_reg.username;
            $scope.mainregisterJson.phone = $scope.user_reg.phone;
            $scope.mainregisterJson.password = $scope.user_reg.password;
            $scope.mainregisterJson.platform = "website";
            // $scope.registerJson.repassword = $scope.user_reg.repassword;

            //console.log($scope.mainregisterJson);

            if ($scope.user_reg.name === "" && $scope.user_reg.email === "" && $scope.user_reg.phone === null && $scope.user_reg.password === "") {
                $scope.overlay = false;
                $scope.errors = true;
                $scope.initial = false;
            }
            if (($scope.user_reg.name === "" || $scope.user_reg.name === undefined) && ($scope.user_reg.email === "" || $scope.user_reg.email === undefined)) {
                $scope.email_error = true;
                $scope.overlay = false;
                $scope.name_error = true;
                $scope.initial = false;
            }
            if ($scope.user_reg.email !== "" && $scope.user_reg.email !== undefined) {
                $scope.overlay = false;
                $scope.name_error = true;
                $scope.initial = false;
                $scope.pass_error = true;
                if ($scope.user_reg.name !== "") {
                    $scope.name_error = false;
                    $scope.initial = false;
                    $scope.pass_error = true;
                    if ($scope.user_reg.password !== "" || $scope.user_reg.password !== undefined) {
                        $scope.pass_error = false;
                    }
                }
            }
            if ($scope.user_reg.name !== "") {
                if ($scope.user_reg.email === "" || $scope.user_reg.email === undefined) {
                    $scope.email_error = true;
                    $scope.overlay = false;
                    $scope.pass_error = true;
                    $scope.initial = false;
                    if ($scope.user_reg.password !== "" && $scope.user_reg.password !== undefined) {
                        $scope.pass_error = false;
                        $scope.initial = true;
                    }
                }
            }
            // if ($scope.user_reg.phone === undefined) {
            //     $scope.mobile_error = true;
            // }
            if ($scope.user_reg.name !== "" && $scope.user_reg.email !== "" && $scope.user_reg.email !== undefined) {
                if ($scope.user_reg.password === "" || $scope.user_reg.password === undefined) {
                    $scope.pass_error = true;
                    $scope.initial = false;
                } else {
                    $scope.overlay = true;
                    $scope.pass_error = false;
                    // $scope.mobile_error = false;
                    $scope.stopLoader();
                    RegisterService.postRegister("/apis/student_registration.json", $scope.mainregisterJson).success(function(successData) {
                            if (successData !== null) {
                                $scope.overlay = false;
                                $scope.dataFound = true;
                                $timeout.cancel($scope.loaderTimer);
                                ////console.log(successData);
                                if (successData.status == 0) {
                                    //$scope.regConfirm();
                                    $scope.errors = true;
                                } else if (successData.status == 1) {
                                    $scope.user_reg.name = "";
                                    $scope.user_reg.email = "";
                                    $scope.user_reg.phone = null;
                                    $scope.user_reg.password = null;
                                    localstorage.set("stud_id", successData.studentid);
                                    $rootScope.prof_imag = 'images/profile.png';
                                    $rootScope.prof_name = successData.first_name;
                                    localstorage.set("logg_type", 0);
                                    localstorage.set("logged", true);
                                    $scope.loginview = true;
                                    $scope.prof_data.prof_imag = 'images/profile.png';
                                    $scope.prof_data.prof_name = successData.first_name;;
                                    localstorage.set("profile", JSON.stringify($scope.prof_data));
                                    $scope.finishreg();
                                    $scope.errors = false;
                                }
                            }
                        })
                        .error(function(err) {
                            // 
                            $scope.dataFound = true;
                            $timeout.cancel($scope.loaderTimer);
                            ////console.log(err);
                            var email_error = 0;
                            $scope.errors = false;
                            if (err.hasOwnProperty('error')) {
                                for (var i = 0; i < err.error.length; i++) {
                                    if (err.error[i] === "email") {
                                        $scope.regSubmit = true;
                                        email_error = 1;

                                    }
                                }
                                if (email_error === 0) {
                                    for (var i = 0; i < err.error.length; i++) {
                                        if (err.error[i] === "password") {
                                            $scope.regSubmit = true;
                                            $scope.error_message = "Registration Failed : Password can't be blank";
                                        }
                                    }
                                    for (var i = 0; i < err.error.length; i++) {
                                        if (err.error[i] === "phone_no") {
                                            $scope.regSubmit = true;
                                            $scope.user_reg.phone = null;
                                            $scope.mobile_error = true;
                                            $scope.error_message = "Registration Failed : Phone number not in proper format";
                                        }
                                    }
                                }
                                if (email_error === 1) {
                                    for (var i = 0; i < err.error.length; i++) {
                                        if (err.error[i] === "phone_no") {
                                            $scope.regSubmit = true;
                                            $scope.user_reg.phone = null;
                                            $scope.mobile_error = true;
                                            $scope.error_message = "Registration Failed : Email has already been taken";
                                        } else {
                                            $scope.error_message = "Registration Failed : Email has already been taken";
                                        }
                                    }
                                }

                            }
                            $scope.overlay = false;
                        })
                }
            }
        }

        $scope.closeregConfirm = function() {
            $scope.overlay = false;
            $scope.regSubmit = false;
        };
        $scope.finishreg = function() {
            $scope.overlay = false;
            $scope.finishSubmit = true;
        }
        $scope.closefinishreg = function() {
            $scope.overlay = false;
            $scope.finishSubmit = false;
            $state.go('heatmap');
        }
    })
    .controller('practAnalysisCtrl', function($scope, $ionicHistory, directiveService, HardwareBackButtonManager, $ionicSideMenuDelegate, $location, $stateParams, $sce, $http, TestService, $ionicLoading, localstorage, $rootScope, $timeout, $ionicScrollDelegate, $ionicPopup, $state, appConfig) {

        $scope.subjective_sol = false;
        $scope.overlay = true;
        $scope.top = 0;
        $scope.count = 0;

        $scope.dataFound = false;

        $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 30000);
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $ionicHistory.goBack();
        }
        $scope.ObjectLength = function(object) {
            var length = 0;
            for (var key in object) {
                if (object.hasOwnProperty(key)) {
                    ++length;
                }
            }
            return length;
        };
        $ionicHistory.nextViewOptions({
            disableAnimate: true
        });
        //////console.log($stateParams.name);
        MathJax.Hub.Config({
            skipStartupTypeset: true,
            messageStyle: "none",
            "HTML-CSS": {
                showMathMenu: false
            }
        });

        MathJax.Hub.Configured();
        HardwareBackButtonManager.disable();

        $scope.subj_solu = function() {
            $scope.subjective_sol = !$scope.subjective_sol;
        }

        $scope.close_side_bar = function() {
            $scope.sidebarCollapse = true;
        }
        $scope.show_side = function() {
            $scope.sidebarCollapse = false;
        }

        $scope.contains = function(sub) {
            if ($scope.testName.indexOf(sub) > -1) {
                return true;
            }
            return false;
        }
        $scope.testName = $stateParams.name || '';
        $scope.showTestButton = {
            'Physics': false,
            'Chemistry': false,
            'Maths': false
        };
        // Logic to identify the test
        if ($scope.contains('PHYSICS')) {
            $scope.currentView = 0;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Maths = true;
        } else if ($scope.contains('physics')) {
            $scope.currentView = 0;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Maths = true;
        } else if ($scope.contains('CHEMISTRY')) {
            $scope.currentView = 1;
            $scope.showTestButton.Physics = $scope.showTestButton.Maths = true;
        } else if ($scope.contains('chemistry')) {
            $scope.currentView = 1;
            $scope.showTestButton.Physics = $scope.showTestButton.Maths = true;
        } else if ($scope.contains('MATH')) {
            $scope.currentView = 2;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Physics = true;
        } else if ($scope.contains('math')) {
            $scope.currentView = 2;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Physics = true;
        }



        $scope.groupBy = function(array, f) {
            var groups = {};
            array.forEach(function(o) {
                var group = JSON.stringify(f(o));
                groups[group] = groups[group] || [];
                groups[group].push(o);
            });
            return Object.keys(groups).map(function(group) {
                return groups[group];
            })
        };

        $scope.getNumber = function(num) {
            return new Array(num);
        }

        $scope.getQuestions = function() {

            TestService.getTestQuestions("http://conceptowl.com/apis/get_detailed_analytics.json?paper_id=" + $stateParams.num).success(function(successData) {
                console.log(successData);
                if (successData != null) {
                    $scope.dataFound = true;
                    $scope.overlay = false;
                    for (var i = 0; i < successData.length; i++) {
                        ////console.log(successData[i].type);
                    };
                    $scope.questionsList = successData;
                    $scope.filterdata = successData;
                    //////console.log($scope.questionsList);
                    $scope.processQuestions(0);
                    console.log($scope.groupedQuestions);
                    if ($scope.groupedQuestions.length == 3) {
                        $scope.count = 1;
                    } else {
                        $scope.count = 0;
                    }
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                    $scope.groupedQuestionsFilter = $scope.groupedQuestions;
                    //////console.log($scope.groupedQuestions);
                }
            });
        }


        $scope.getQuestions();
        $scope.processQuestions = function(view) {


            $scope.groupedQuestions = $scope.groupBy($scope.questionsList, function(item) {
                return [item.topic];
            });

            $scope.newQustionList = [];
            var phy = 0;
            var che = 0;
            var mat = 0;
            for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                    if ($scope.groupedQuestions[i][j].subject == "physics") {
                        phy = 1;
                    } else if ($scope.groupedQuestions[i][j].subject == "chemistry") {
                        che = 1;
                    } else if ($scope.groupedQuestions[i][j].subject == "math") {
                        mat = 1;
                    }
                };
            };
            if (phy == 0) {
                //////console.log("No physics");
                $scope.newQustionList.push({
                    "datas": 1000,
                    "topic": "Physics"
                });

            } else {
                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].subject == "physics") {
                            $scope.groupedQuestions[i][j].datas = 1001;
                            $scope.newQustionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };
            }
            if (che == 0) {

                //////console.log("No chemistry");

                $scope.newQustionList.push({
                    "datas": 1000,
                    "topic": "Chemistry"
                });

            } else {

                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].subject == "chemistry") {
                            $scope.groupedQuestions[i][j].datas = 1001;
                            $scope.newQustionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };
            }
            if (mat == 0) {
                //////console.log("No maths");
                $scope.newQustionList.push({
                    "datas": 1000,
                    "topic": "Mathematics"
                });
            } else {
                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].subject == "math") {
                            $scope.groupedQuestions[i][j].datas = 1001;
                            $scope.newQustionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };
            }
            //  //////console.log("Test :" + $scope.newQustionList);
            $scope.modiQuestionList = [];
            $scope.groupedQuestions = $scope.groupBy($scope.newQustionList, function(item) {
                return [item.topic];
            });

            //  //////console.log("dfgfd");


            if ($scope.count == 0) {
                for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                        if ($scope.groupedQuestions[i][j].datas == 1001) {
                            $scope.modiQuestionList.push($scope.groupedQuestions[i][j]);
                        }
                    };
                };

                $scope.groupedQuestions = $scope.groupBy($scope.modiQuestionList, function(item) {
                    return [item.topic];
                });
            }


            //////console.log($scope.groupedQuestions);
            $scope.currentView = view;
            $scope.currentQuestion = 0;
        };


        $scope.processAnswers = function() {

            for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                for (var j = 0; j < $scope.groupedQuestions[i].length; j++) {
                    if ($scope.groupedQuestions[i][j].type === 'single_correct_choice_type') {
                        if ($scope.groupedQuestions[i][j].answer_type === 'skipped') {
                            $scope.groupedQuestions[i].skippedAnswer = $scope.groupedQuestions[i].skippedAnswer + 1;
                            $scope.groupedQuestions[i][j].skipped = true;
                            for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].answer[0].opt) {
                                    $scope.groupedQuestions[i][j].options[k].skipped = true;
                                } else {
                                    $scope.groupedQuestions[i][j].options[k].skipped = false;
                                }
                            };

                        }
                        if ($scope.groupedQuestions[i][j].answer_type === 'attempted') {
                            if ($scope.groupedQuestions[i][j].submitted[0].opt == $scope.groupedQuestions[i][j].answer[0].opt) {
                                $scope.groupedQuestions[i][j].correct = true;
                                $scope.groupedQuestions[i].correctAnswer = $scope.groupedQuestions[i].correctAnswer + 1;
                                for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                    if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].answer[0].opt) {
                                        $scope.groupedQuestions[i][j].options[k].correct = true;
                                    }
                                };
                            } else {
                                $scope.groupedQuestions[i][j].incorrect = true;
                                for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                    if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].submitted[0].opt) {
                                        $scope.groupedQuestions[i][j].options[k].incorrect = true;
                                        $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                                    }
                                    if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].answer[0].opt) {
                                        $scope.groupedQuestions[i][j].options[k].correct = true;
                                    }
                                };
                            }
                        }
                    } else if ($scope.groupedQuestions[i][j].type === 'multiple_correct_choice_type') {
                        $scope.correct_count = 0;
                        $scope.incorrect_count = 0;
                        if ($scope.groupedQuestions[i][j].answer_type === 'skipped') {
                            // //////console.log("hooo");
                            $scope.groupedQuestions[i].skippedAnswer = $scope.groupedQuestions[i].skippedAnswer + 1;
                            $scope.groupedQuestions[i][j].skipped = true;
                            for (var l = 0; l < $scope.groupedQuestions[i][j].answer.length; l++) {
                                for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                    if ($scope.groupedQuestions[i][j].answer[l].opt == $scope.groupedQuestions[i][j].options[k].opt_id) {
                                        $scope.groupedQuestions[i][j].options[k].skipped = true;
                                    }

                                };
                            };
                        }
                        if ($scope.groupedQuestions[i][j].answer_type === 'attempted') {
                            // //////console.log("hiiii");
                            if ($scope.groupedQuestions[i][j].submitted.length === $scope.groupedQuestions[i][j].answer.length) {

                                var multiple_correct = 0;
                                for (var l = 0; l < $scope.groupedQuestions[i][j].submitted.length; l++) {
                                    var tmp = 0;
                                    for (var m = 0; m < $scope.groupedQuestions[i][j].answer.length; m++) {
                                        if ($scope.groupedQuestions[i][j].submitted[l].opt === $scope.groupedQuestions[i][j].answer[m].opt.toString()) {
                                            multiple_correct++;
                                            tmp++;
                                            for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id.toString() === $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].correct = true;
                                                    //  ////console.log("correct");
                                                    break;
                                                }
                                            };
                                        }

                                    };
                                    if (tmp === 0) {
                                        for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                            if ($scope.groupedQuestions[i][j].options[k].opt_id.toString() === $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                $scope.groupedQuestions[i][j].options[k].incorrect = true;
                                                // ////console.log("Incorrect");
                                            }
                                        };
                                    }

                                }
                                if (multiple_correct === $scope.groupedQuestions[i][j].answer.length) {
                                    $scope.groupedQuestions[i][j].correct = true;
                                    $scope.groupedQuestions[i].correctAnswer = $scope.groupedQuestions[i].correctAnswer + 1;
                                } else {
                                    $scope.groupedQuestions[i][j].incorrect = true;
                                    $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                                }
                                for (var x = 0; x < $scope.groupedQuestions[i][j].answer.length; x++) {
                                    for (var y = 0; y < $scope.groupedQuestions[i][j].options.length; y++) {
                                        if ($scope.groupedQuestions[i][j].options[y].opt_id == $scope.groupedQuestions[i][j].answer[x].opt) {
                                            $scope.groupedQuestions[i][j].options[y].skipped = true;
                                        }
                                    };
                                };
                            } else if ($scope.groupedQuestions[i][j].submitted.length < $scope.groupedQuestions[i][j].answer.length) {
                                //////console.log("less answers");
                                $scope.groupedQuestions[i][j].incorrect = true;
                                var tmp = false;
                                // var diff=$scope.groupedQuestions[i][j].submitted.length - $scope.groupedQuestions[i][j].answer.length;
                                $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                                for (var l = 0; l < $scope.groupedQuestions[i][j].submitted.length; l++) {
                                    for (var m = 0; m < $scope.groupedQuestions[i][j].answer.length; m++) {
                                        if ($scope.groupedQuestions[i][j].submitted[l].opt == $scope.groupedQuestions[i][j].answer[m].opt) {
                                            tmp = true;
                                            for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].correct = true;
                                                }
                                            };
                                        }
                                    };
                                    if (tmp == false) {
                                        for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                            if (tmp == 0) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].incorrect = true;
                                                }
                                            }
                                        };
                                    }
                                    tmp = false;
                                };
                                for (var m = 0; m < $scope.groupedQuestions[i][j].answer.length; m++) {
                                    for (var l = 0; l < $scope.groupedQuestions[i][j].submitted.length; l++) {
                                        if ($scope.groupedQuestions[i][j].submitted[l].opt != $scope.groupedQuestions[i][j].answer[m].opt) {
                                            for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].answer[m].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].skipped = true;
                                                }
                                            };
                                        }
                                    };
                                };
                            } else if ($scope.groupedQuestions[i][j].submitted.length > $scope.groupedQuestions[i][j].answer.length) {
                                //////console.log("more answers");
                                $scope.groupedQuestions[i][j].incorrect = true;
                                var tmp = false;
                                // var diff=$scope.groupedQuestions[i][j].submitted.length - $scope.groupedQuestions[i][j].answer.length;
                                $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                                for (var l = 0; l < $scope.groupedQuestions[i][j].submitted.length; l++) {
                                    for (var m = 0; m < $scope.groupedQuestions[i][j].answer.length; m++) {
                                        if ($scope.groupedQuestions[i][j].submitted[l].opt == $scope.groupedQuestions[i][j].answer[m].opt) {
                                            tmp = true;
                                            for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].correct = true;
                                                }
                                            };
                                        }
                                    };
                                    if (tmp == false) {
                                        for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                            if (tmp == 0) {
                                                if ($scope.groupedQuestions[i][j].options[k].opt_id == $scope.groupedQuestions[i][j].submitted[l].opt) {
                                                    $scope.groupedQuestions[i][j].options[k].incorrect = true;
                                                }
                                            }
                                        };
                                    }
                                    tmp = false;
                                };
                                /*for (var m = $scope.groupedQuestions[i][j].answer.length; m < $scope.groupedQuestions[i][j].submitted.length; m++) {
                                    var c = $scope.groupedQuestions[i][j].answer[m].opt;
                                    for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                                        if ($scope.groupedQuestions[i][j].options[k].opt_id == c) {
                                            $scope.groupedQuestions[i][j].options[k].skipped = true;
                                        }
                                    };
                                };*/
                            }
                        }
                    } else if ($scope.groupedQuestions[i][j].type === 'integer_type') {
                        if ($scope.groupedQuestions[i][j].answer_type === "attempted") {
                            if (parseInt($scope.groupedQuestions[i][j].integer_answer) === parseInt($scope.groupedQuestions[i][j].submitted[0].opt)) {
                                $scope.groupedQuestions[i][j].correct = true;
                                $scope.groupedQuestions[i].correctAnswer = $scope.groupedQuestions[i].correctAnswer + 1;
                            } else {
                                $scope.groupedQuestions[i][j].incorrect = true;
                                $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                            }
                        } else if ($scope.groupedQuestions[i][j].answer_type === "skipped") {
                            $scope.groupedQuestions[i][j].skipped = true;
                            $scope.groupedQuestions[i].skippedAnswer = $scope.groupedQuestions[i].skippedAnswer + 1;
                        }
                    } else if ($scope.groupedQuestions[i][j].type === 'matrix_type') {
                        if ($scope.groupedQuestions[i][j].answer_type === 'attempted') {

                            for (var m = 0; m < $scope.ObjectLength($scope.groupedQuestions[i][j].submitted); m++) {
                                if ($scope.groupedQuestions[i][j].submitted[m] !== null) {
                                    //   ////console.log($scope.groupedQuestions[i][j].submitted[m].length);
                                    for (var k = 0; k < $scope.groupedQuestions[i][j].submitted[m].length; k++) {
                                        ////console.log($scope.groupedQuestions[i][j].submitted[m]);
                                        for (var l = 0; l < $scope.groupedQuestions[i][j].matrix_option_params.length; l++) {
                                            // ////console.log($scope.groupedQuestions[i][j].submitted[k][l]);
                                            if ($scope.groupedQuestions[i][j].submitted[m][k] === $scope.groupedQuestions[i][j].matrix_option_params[l]) {
                                                $scope.groupedQuestions[i][j].matrixarray[m][l] = true;
                                            }
                                        };
                                    };
                                }
                            };
                            for (var w = 0; w < $scope.groupedQuestions[i][j].answer.length; w++) {
                                //  ////console.log($scope.groupedQuestions[i][j].answer);
                                for (var x = 0; x < $scope.groupedQuestions[i][j].options.length; x++) {
                                    if ($scope.groupedQuestions[i][j].answer[w].opt === $scope.groupedQuestions[i][j].options[x].opt_id) {
                                        var options = $scope.groupedQuestions[i][j].options[x].opt.split(',');
                                        ////console.log(options);
                                        for (var y = 0; y < options.length; y++) {
                                            for (var z = 0; z < $scope.groupedQuestions[i][j].matrix_option_params.length; z++) {
                                                if (options[y] === $scope.groupedQuestions[i][j].matrix_option_params[z]) {
                                                    $scope.groupedQuestions[i][j].ans_matrixarray[w][z] = true;
                                                }
                                            };
                                        }
                                    }
                                };
                            };

                            /*Comapre answer mtrix and answered matrix*/
                            var correct = 0;
                            for (var k = 0; k < $scope.groupedQuestions[i][j].matrix_answer_params.length; k++) {
                                for (var l = 0; l < $scope.groupedQuestions[i][j].matrix_option_params.length; l++) {
                                    if ($scope.groupedQuestions[i][j].ans_matrixarray[k][l] !== $scope.groupedQuestions[i][j].matrixarray[k][l]) {
                                        correct = 1;
                                    }
                                };
                            };
                            //////console.log(correct);
                            if (correct === 0) {
                                $scope.groupedQuestions[i][j].correct = true;
                                $scope.groupedQuestions[i].correctAnswer = $scope.groupedQuestions[i].correctAnswer + 1;
                            } else {
                                $scope.groupedQuestions[i][j].incorrect = true;
                                $scope.groupedQuestions[i].incorrectAnswer = $scope.groupedQuestions[i].incorrectAnswer + 1;
                            }
                        } else if ($scope.groupedQuestions[i][j].answer_type === 'skipped') {
                            $scope.groupedQuestions[i][j].skipped = true;
                            $scope.groupedQuestions[i].skippedAnswer = $scope.groupedQuestions[i].skippedAnswer + 1;
                            for (var w = 0; w < $scope.groupedQuestions[i][j].answer.length; w++) {
                                //  ////console.log($scope.groupedQuestions[i][j].answer);
                                for (var x = 0; x < $scope.groupedQuestions[i][j].options.length; x++) {
                                    if ($scope.groupedQuestions[i][j].answer[w].opt === $scope.groupedQuestions[i][j].options[x].opt_id) {
                                        var options = $scope.groupedQuestions[i][j].options[x].opt.split(',');
                                        ////console.log(options);
                                        for (var y = 0; y < options.length; y++) {
                                            for (var z = 0; z < $scope.groupedQuestions[i][j].matrix_option_params.length; z++) {
                                                if (options[y] === $scope.groupedQuestions[i][j].matrix_option_params[z]) {
                                                    $scope.groupedQuestions[i][j].ans_matrixarray[w][z] = true;
                                                }
                                            };
                                        }
                                    }
                                };
                            };
                        }
                    }
                };
            };
        }


        $scope.addAnswerField = function() {

            for (var i = $scope.groupedQuestions.length - 1; i >= 0; i--) {
                for (var j = $scope.groupedQuestions[i].length - 1; j >= 0; j--) {
                    $scope.groupedQuestions[i][j].min = Math.floor($scope.groupedQuestions[i][j].time_spent / 60);
                    $scope.groupedQuestions[i][j].sec = $scope.groupedQuestions[i][j].time_spent % 60;
                    $scope.groupedQuestions[i][j].integer_answer = parseInt($scope.groupedQuestions[i][j].integer_answer);
                    $scope.groupedQuestions[i][j].correct = false;
                    $scope.groupedQuestions[i][j].incorrect = false;
                    $scope.groupedQuestions[i][j].skipped = false;
                    if ($scope.groupedQuestions[i][j].datas != 1000) {
                        for (var k = 0; k < $scope.groupedQuestions[i][j].options.length; k++) {
                            $scope.groupedQuestions[i][j].options[k].correct = false;
                            $scope.groupedQuestions[i][j].options[k].incorrect = false;
                        };
                    }
                    if ($scope.groupedQuestions[i][j].type === "matrix_type") {

                        $scope.quest_options = $scope.groupedQuestions[i][j].matrix_answer_params;
                        $scope.answer_options = $scope.groupedQuestions[i][j].matrix_option_params;

                        $scope.groupedQuestions[i][j].ans_matrixarray = new Array($scope.quest_options.length)
                        for (k = 0; k < $scope.quest_options.length; k++)
                            $scope.groupedQuestions[i][j].ans_matrixarray[k] = new Array($scope.answer_options.length)

                        for (var l = 0; l < $scope.quest_options.length; l++) {
                            for (var m = 0; m < $scope.answer_options.length; m++) {
                                $scope.groupedQuestions[i][j].ans_matrixarray[l][m] = false;
                            };
                        };

                        $scope.groupedQuestions[i][j].matrixarray = new Array($scope.quest_options.length)
                            //var matrixarray = new Array($scope.quest_options.length)
                        for (k = 0; k < $scope.quest_options.length; k++)
                            $scope.groupedQuestions[i][j].matrixarray[k] = new Array($scope.answer_options.length)

                        for (var l = 0; l < $scope.quest_options.length; l++) {
                            for (var m = 0; m < $scope.answer_options.length; m++) {
                                $scope.groupedQuestions[i][j].matrixarray[l][m] = false;
                            };
                        };
                    }
                    if ($scope.groupedQuestions[i][j].hint === "" && $scope.groupedQuestions[i][j].hint_2 === "" && $scope.groupedQuestions[i][j].hint_3 === "") {
                        $scope.groupedQuestions[i][j].no_hint = true;
                    } else {
                        $scope.groupedQuestions[i][j].no_hint = false;
                        console.log($scope.groupedQuestions[i][j].hint);
                        console.log(i + ':' + j);
                    }
                };
                $scope.groupedQuestions[i].correctAnswer = 0;
                $scope.groupedQuestions[i].incorrectAnswer = 0;
                $scope.groupedQuestions[i].skippedAnswer = 0;





            };
        }

        $scope.changeSub = function(num, id) {
            $scope.hintForm = false;
            if ($scope.groupedQuestions[num][0].datas == 1000) {
                $scope.question_space = true;
                $scope.question_space1 = true;

            } else {
                $scope.question_space = false;
                $scope.question_space1 = false;
            }
            if (num !== $scope.currentView) {
                if (num > $scope.currentView) {
                    $ionicScrollDelegate.scrollTop();
                    $scope.currentView = num;
                    $scope.currentQuestion = 0;
                    $scope.top = 0;
                } else if (num < $scope.currentView) {
                    if (id == 10) {
                        $ionicScrollDelegate.scrollTop();
                        $scope.currentView = num;
                        $scope.currentQuestion = 0;
                    } else {
                        $scope.currentView = num;
                        $scope.currentQuestion = 0;
                        $ionicScrollDelegate.scrollBottom();
                    }
                }

            }


        }

        $scope.changeQuestion = function(num) {
            $scope.hintForm = false;
            $scope.subjective_sol = false;
            if (num != -1) {
                //////console.log(num)
                $scope.currentQuestion = num;
                $scope.top = (num - 7) * 50;
                $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                //////console.log($scope.groupedQuestions);
                // $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            }

        }


        $scope.changeQuestionForward = function(num) {
            $scope.hintForm = false;
            $scope.subjective_sol = false;
            if (num != -1) {
                //////console.log(num)
                $scope.currentQuestion = num;
                if ($scope.currentQuestion > 7) {
                    $scope.top += 50;
                    $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                } else {
                    $scope.top = 0;
                }
                //////console.log($scope.groupedQuestions);
                // $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            }

        }

        $scope.changeQuestionBackward = function(num) {
            $scope.hintForm = false;
            $scope.subjective_sol = false;
            if (num != -1) {
                //////console.log(num)
                $scope.currentQuestion = num;
                if ($scope.currentQuestion != 0) {
                    $scope.top -= 50;
                    $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                } else {
                    $scope.top = 0;
                }
                //////console.log($scope.groupedQuestions);
                // $scope.groupedQuestionsFilter = $scope.groupedQuestions;
            }

        }

        $scope.setQuestions = function() {
            $scope.quest = $scope.groupedQuestions[$scope.currentView];
        }
        $scope.showNextQuestion = function() {

            if (($scope.currentView === $scope.groupedQuestions.length - 1) && ($scope.currentQuestion === $scope.groupedQuestions[$scope.currentView].length - 1)) {
                $scope.showConfirm();
            } else if ($scope.currentQuestion === $scope.groupedQuestions[$scope.currentView].length - 1) {
                // End of the subject is reached , switch the subject
                $scope.changeSub($scope.currentView + 1, 11);
            } else {
                // change question
                $scope.changeQuestionForward($scope.currentQuestion + 1);
            }
        };
        $scope.showPrevQuestion = function() {
            // Check if the question has reached the first question , then switch the subject
            if ($scope.currentQuestion === 0) {
                $scope.changeSub($scope.currentView - 1, 9);
                $scope.changeQuestion($scope.groupedQuestions[$scope.currentView].length - 1)
            } else {
                // change question
                $scope.changeQuestionBackward($scope.currentQuestion - 1);
            }
        };

        $scope.showConfirm = function() {
            $scope.testSubmit = true;
        };
        $scope.closeConfirmPopup = function() {
            $scope.testSubmit = false;
        }
        $scope.goToCompletion = function() {
            $scope.analyCompleted = true;
            //$location.path('/practice_review');
            $ionicHistory.goBack();
        }
        $scope.showHint = function() {
                $scope.hintForm = !$scope.hintForm;
            }
            /** Filters **/

        $scope.allQuestions = function() {
            $scope.sidebarCollapse = true;
            //////console.log("All Questions");
            $scope.question_space = false;
            $scope.question_space1 = false;

            $scope.correctCheck = false;
            $scope.incorrectCheck = false;
            $scope.skippedCheck = false;
            $scope.allCheck = true;

            $scope.questionsList = $scope.filterdata;
            $scope.processQuestions($scope.currentView);
            // //////console.log($scope.groupedQuestions);
            $scope.addAnswerField();
            $scope.processAnswers();
            $scope.setQuestions();
        }
        $scope.correctQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "corret answered";
            //////console.log("Answered Questions");
            $scope.allCheck = false;
            $scope.incorrectCheck = false;
            $scope.skippedCheck = false;
            $scope.question_space = false;
            $scope.question_space1 = false;
            // $scope.correctCheck = true;

            $scope.correctAnsFilt = [];

            // $scope.questionsList=[];
            if ($scope.correctCheck) {
                //////console.log($scope.groupedQuestionsFilter);
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].correct == true) {
                            $scope.correctAnsFilt.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                if ($scope.correctAnsFilt.length == 0) {
                    $scope.question_space = true;
                    $scope.question_space1 = true;
                    $scope.questionsList = $scope.correctAnsFilt;
                    //////console.log($scope.questionsList);
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                } else {
                    $scope.questionsList = $scope.correctAnsFilt;
                    //////console.log($scope.questionsList);
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                }

            } else {
                $scope.correctCheck = false;
                $scope.allQuestions();
            }

        }
        $scope.skippedQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "skipped";
            //////console.log("Skipped Questions");
            $scope.correctCheck = false;
            $scope.incorrectCheck = false;
            $scope.allCheck = false;
            $scope.question_space = false;
            $scope.question_space1 = false;
            // $scope.skippedCheck = true;

            $scope.skippedFilt = [];
            if ($scope.skippedCheck) {
                //////console.log($scope.groupedQuestionsFilter);
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].skipped == true) {
                            $scope.skippedFilt.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                if ($scope.skippedFilt.length == 0) {
                    $scope.questionsList = $scope.skippedFilt;
                    //////console.log($scope.questionsList);
                    $scope.question_space = true;
                    $scope.question_space1 = true;
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                } else {
                    $scope.questionsList = $scope.skippedFilt;
                    //////console.log($scope.questionsList);
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                }

            } else {
                $scope.skippedCheck = false;
                $scope.allQuestions();
            }

        }
        $scope.incorrectQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "incorret answered";
            //////console.log("Incorrect Questions");
            $scope.correctCheck = false;
            $scope.allCheck = false;
            $scope.skippedCheck = false;

            $scope.question_space1 = false;
            $scope.question_space = false;
            //  $scope.incorrectCheck = true;

            $scope.incorrectFilt = [];
            if ($scope.incorrectCheck) {
                //////console.log($scope.groupedQuestionsFilter);
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestionsFilter[i].length; j++) {
                        if ($scope.groupedQuestionsFilter[i][j].incorrect == true) {
                            $scope.incorrectFilt.push($scope.groupedQuestionsFilter[i][j]);
                        }
                    };
                };
                if ($scope.incorrectFilt.length == 0) {
                    $scope.questionsList = $scope.incorrectFilt;
                    //////console.log($scope.questionsList);
                    $scope.question_space = true;
                    $scope.question_space1 = true;
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                } else {
                    $scope.questionsList = $scope.incorrectFilt;
                    //////console.log($scope.questionsList);
                    $scope.processQuestions($scope.currentView);
                    $scope.addAnswerField();
                    $scope.processAnswers();
                    $scope.setQuestions();
                }
            } else {
                $scope.incorrectCheck = false;
                $scope.allQuestions();
            }
        }
    })
    .controller('testQuestCtrl', function($scope, $ionicHistory, directiveService, HardwareBackButtonManager, $ionicSideMenuDelegate, $interval, $location, $stateParams, $sce, $http, TestService, $ionicLoading, localstorage, $rootScope, $timeout, $ionicScrollDelegate, $ionicPopup, $state, appConfig) {
        $scope.dataFound = false;
        $scope.questions = [];
        $scope.loaderTimer = $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 10000);
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $ionicHistory.goBack();
        }

        $ionicHistory.nextViewOptions({
            disableAnimate: true
        });

        $scope.topic_name = $stateParams.name;
        $scope.topic_subject = $stateParams.sub;
        $scope.paperid = $stateParams.paper;
        $scope.test_type = $stateParams.type;
        $scope.subjective_sol = false;
        //////console.log($scope.test_type);

        $scope.hintForm = false;
        $scope.no_solution = true;
        HardwareBackButtonManager.disable();
        $scope.stud_id = localstorage.get('stud_id');

        $scope.subj_solu = function() {
            $scope.subjective_sol = !$scope.subjective_sol;
        }

        $scope.close_side_bar = function() {
            $scope.sidebarCollapse = true;
        }
        $scope.show_side = function() {
            $scope.sidebarCollapse = false;
        }

        $scope.paper_id = 0;
        $scope.answered = 0;
        $scope.skippedQues = 0;


        $scope.showTestButton = {
            'Physics': false,
            'Chemistry': false,
            'Maths': false
        };
        // Logic to identify the test
        if ($scope.topic_subject === 'PHYSICS') {
            $scope.currentView = 0;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Maths = true;
        } else if ($scope.topic_subject === 'physics') {
            $scope.currentView = 0;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Maths = true;
        } else if ($scope.topic_subject === 'CHEMISTRY') {
            $scope.currentView = 1;
            $scope.showTestButton.Physics = $scope.showTestButton.Maths = true;
        } else if ($scope.topic_subject === 'chemistry') {
            $scope.currentView = 1;
            $scope.showTestButton.Physics = $scope.showTestButton.Maths = true;
        } else if ($scope.topic_subject === 'MATH') {
            $scope.currentView = 2;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Physics = true;
        } else if ($scope.topic_subject === 'math') {
            $scope.currentView = 2;
            $scope.showTestButton.Chemistry = $scope.showTestButton.Physics = true;
        }

        $scope.overlay = true;

        MathJax.Hub.Config({
            skipStartupTypeset: true,
            messageStyle: "none",
            "HTML-CSS": {
                showMathMenu: false
            }
        });
        MathJax.Hub.Configured();

        $scope.getNumber = function(num) {
            return new Array(num);
        }

        $scope.timeInSeconds = 0;;
        $scope.readableTime = {
            hours: 0,
            minutes: 0,
            seconds: 0
        };
        // show 0:0 as 00:00
        $scope.addZero = function(i) {
            if (i < 10) {
                i = "0" + i
            };
            return i;
        }
        $scope.startTimer = function() {
            $scope.testTimer = $interval(function() {
                $scope.timeInSeconds++;
                $scope.readableTime.seconds = $scope.addZero($scope.timeInSeconds % 60);
                $scope.readableTime.minutes = $scope.addZero(Math.floor(($scope.timeInSeconds / 60) % 60));
                $scope.readableTime.hours = $scope.addZero(Math.floor($scope.timeInSeconds / 3600));
            }, 1000);
            //  $scope.timeToEndTest = parseInt($stateParams.time) || 0;
            // End test after specified time 
            $timeout(function() {
                $scope.readableTime.seconds = 00;
                $scope.readableTime.minutes = 00;
                $scope.readableTime.hours = 00;
                $interval.cancel($scope.testTimer);
                $interval.cancel($scope.questionTimer);
                if ($scope.groupedQuestions.length != 0) {
                    $scope.showConfirm();
                }

            }, $scope.timeToEndTest);
        };
        // Capture the time take for each question
        $scope.changeQuestionTimer = function() {
            if ($scope.questionTimer) {
                $interval.cancel($scope.questionTimer);
            }
            $scope.questionTimer = $interval(function() {
                $scope.groupedQuestions[$scope.currentQuestion].time++;
            }, 1000);
        };

        $scope.getQuestions = function() {

            TestService.getTestQuestions("http://conceptowl.com/apis/question_details.json?quesnum=" + $stateParams.num).success(function(successData) {
                    if (successData !== null) {
                        $scope.questions.push(successData);
                        $timeout.cancel($scope.loaderTimer);
                        $scope.dataFound = true;
                        $scope.overlay = false;
                        console.log(successData);
                        $scope.paper_id = successData.paper_id;
                        $scope.groupedQuestions = $scope.questions;
                        //  //////console.log($scope.groupedQuestions);
                        if ($scope.groupedQuestions.length == 0) {
                            $scope.showEndpop();
                        }
                        // $scope.timeInSeconds = $scope.groupedQuestions.length * 2 * 60;
                        $scope.timeToEndTest = $scope.groupedQuestions.length * 2 * 1000 * 60;

                        // $scope.startTimer();
                        $scope.addAnswerField();
                        $scope.currentQuestion = 0;
                        // $scope.changeQuestionTimer();
                        //////console.log($scope.groupedQuestions);
                        for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                            if ($scope.groupedQuestions[i].hint === "" && $scope.groupedQuestions[i].hint_2 === "" && $scope.groupedQuestions[i].hint_3 === "") {
                                $scope.groupedQuestions[i].no_hint = true;
                            } else {
                                $scope.groupedQuestions[i].no_hint = false;
                            }
                            /*if ($scope.groupedQuestions[i].solution == "") {
                                $scope.groupedQuestions[i].solution = "No Solution Available!!";
                            }*/
                            if ($scope.groupedQuestions[i].type == 'multiple_correct_choice_type') {
                                //////console.log("multiple");
                            } else {
                                //////console.log("single");
                            }

                        };
                        $scope.groupedQuestionsFilter = $scope.groupedQuestions;
                        //  $scope.chan$scope.groupedQuestionsgeQuestionTimer();
                    }
                })
                .error(function(e) {
                    //////console.log(e);
                })


        };
        $scope.getQuestions();
        $scope.processFilterQuestions = function() {

            if ($scope.groupedQuestions.length == 0) {
                $scope.question_space1 = true;
                $scope.question_space = true;
                $interval.cancel($scope.questionTimer);
            } else {
                $scope.question_space1 = false;
                $scope.question_space = false;
                $scope.currentQuestion = 0;
                $scope.changeQuestionTimer();
            }

        }
        $scope.addAnswerField = function() {
            for (var i = $scope.groupedQuestions.length - 1; i >= 0; i--) {

                $scope.groupedQuestions[i].submitted = false;
                $scope.groupedQuestions[i].attempted = false;
                $scope.groupedQuestions[i].bookmarked = false;
                $scope.groupedQuestions[i].skipped = false;
                $scope.groupedQuestions[i].submittedAns = [];
                $scope.groupedQuestions[i].correctAns = false;
                $scope.groupedQuestions[i].incorrectAns = false;
                $scope.groupedQuestions[i].integer_choice = null;

                $scope.groupedQuestions[i].time = 0;
                for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                    $scope.groupedQuestions[i].options[j].chosen = true;
                    $scope.groupedQuestions[i].options[j].correct = false;
                    $scope.groupedQuestions[i].options[j].incorrect = false;
                }

                if ($scope.groupedQuestions[i].type === "matrix_type") {
                    $scope.quest_options = $scope.groupedQuestions[i].matrix_answer_params;
                    $scope.answer_options = $scope.groupedQuestions[i].matrix_option_params;

                    $scope.groupedQuestions[i].ans_matrixarray = new Array($scope.quest_options.length)
                    for (j = 0; j < $scope.quest_options.length; j++)
                        $scope.groupedQuestions[i].ans_matrixarray[j] = new Array($scope.answer_options.length)

                    for (var j = 0; j < $scope.quest_options.length; j++) {
                        for (var k = 0; k < $scope.answer_options.length; k++) {
                            $scope.groupedQuestions[i].ans_matrixarray[j][k] = false;
                        };
                    };

                    $scope.groupedQuestions[i].matrixarray = new Array($scope.quest_options.length)
                        //var matrixarray = new Array($scope.quest_options.length)
                    for (j = 0; j < $scope.quest_options.length; j++)
                        $scope.groupedQuestions[i].matrixarray[j] = new Array($scope.answer_options.length)

                    for (var j = 0; j < $scope.quest_options.length; j++) {
                        for (var k = 0; k < $scope.answer_options.length; k++) {
                            $scope.groupedQuestions[i].matrixarray[j][k] = false;
                        };
                    };
                }
                // //////console.log($scope.groupedQuestions);
            };
        }

        $scope.showNextQuestion = function() {



            if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                if ($scope.groupedQuestions[$scope.currentQuestion].submitted) {
                    // $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                } else {

                    if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                        $scope.skippedQues = $scope.skippedQues + 1;
                    }
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                    $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                    for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                        for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                            $scope.groupedQuestions[i].options[j].chosen = true;
                        }
                    };
                }
            } else {
                if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                    $scope.skippedQues = $scope.skippedQues + 1;
                }
                $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
            }
            // Check if the question has reached the end
            if ($scope.currentQuestion === $scope.groupedQuestions.length - 1) {
                $scope.Practicepop();
            } else {
                // change question
                $scope.changeQuestionForward($scope.currentQuestion + 1);
            }
        };
        $scope.showPrevQuestion = function() {


            if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                if ($scope.groupedQuestions[$scope.currentQuestion].submitted) {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                } else {
                    if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                        $scope.skippedQues = $scope.skippedQues + 1;
                    }
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                    $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                    for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                        for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                            $scope.groupedQuestions[i].options[j].chosen = true;
                        }
                    };
                }
            } else {
                if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                    $scope.skippedQues = $scope.skippedQues + 1;
                }
                $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
            }
            // Check if the question has reached the first question , then switch the subject
            if ($scope.currentQuestion === 0) {
                $scope.changeQuestionBackward($scope.groupedQuestions.length - 1)
            } else {
                // change question
                $scope.changeQuestionBackward($scope.currentQuestion - 1);
            }
        };


        $scope.changeQuestion = function(num) {
            $scope.subjective_sol = false;
            if (num != -1) {
                //////console.log(num)
                //  $scope.currentQuestion = num;
                $scope.top = (num - 7) * 50;
                $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                //  $scope.changeQuestionTimer();
                $scope.hintForm = false;
                if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                    if ($scope.groupedQuestions[$scope.currentQuestion].submitted) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                    } else {
                        if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                            $scope.skippedQues = $scope.skippedQues + 1;
                        }
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                        $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                        for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                            for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                                $scope.groupedQuestions[i].options[j].chosen = true;
                            }
                        };
                    }
                } else {
                    if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                        $scope.skippedQues = $scope.skippedQues + 1;
                    }
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                }
                //////console.log(num)
                $scope.currentQuestion = num;
                $scope.integer_ans = $scope.groupedQuestions[$scope.currentQuestion].integer_choice;
                $scope.changeQuestionTimer();
                // //////console.log($scope.groupedQuestions);
            }
        }
        $scope.changeQuestionForward = function(num) {
            $scope.subjective_sol = false;
            if (num != -1) {
                //////console.log(num)
                //  $scope.currentQuestion = num;
                if ($scope.currentQuestion > 7) {
                    $scope.top += 50;
                    $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                } else {
                    $scope.top = 0;
                }
                $scope.hintForm = false;
                if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                    if ($scope.groupedQuestions[$scope.currentQuestion].submitted) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                    } else {
                        if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                            $scope.skippedQues = $scope.skippedQues + 1;
                        }
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                        $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                        for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                            for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                                $scope.groupedQuestions[i].options[j].chosen = true;
                            }
                        };
                    }
                } else {
                    if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                        $scope.skippedQues = $scope.skippedQues + 1;
                    }
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                }

                if ($scope.currentQuestion === $scope.groupedQuestions.length - 1) {
                    $scope.currentQuestion = $scope.groupedQuestions.length - 1;
                    $scope.Practicepop();
                } else {
                    //////console.log(num)
                    $scope.currentQuestion = num;
                    $scope.integer_ans = $scope.groupedQuestions[$scope.currentQuestion].integer_choice;
                    $scope.changeQuestionTimer();
                }

                // //////console.log($scope.groupedQuestions);
            }
        }
        $scope.changeQuestionBackward = function(num) {
            $scope.subjective_sol = false;
            if (num != -1) {
                //////console.log(num)
                //  $scope.currentQuestion = num;
                if ($scope.currentQuestion != 0) {
                    $scope.top -= 50;
                    $ionicScrollDelegate.scrollTo(0, $scope.top, true);
                } else {
                    $scope.top = 0;
                }
                $scope.hintForm = false;
                if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                    if ($scope.groupedQuestions[$scope.currentQuestion].submitted) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                    } else {
                        if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                            $scope.skippedQues = $scope.skippedQues + 1;
                        }
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                        $scope.groupedQuestions[$scope.currentQuestion].attempted = false;
                        for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                            for (j = 0; j < $scope.groupedQuestions[i].options.length; j++) {
                                $scope.groupedQuestions[i].options[j].chosen = true;
                            }
                        };
                    }
                } else {
                    if ($scope.groupedQuestions[$scope.currentQuestion].skipped == false) {
                        $scope.skippedQues = $scope.skippedQues + 1;
                    }
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                }
                //////console.log(num)
                $scope.currentQuestion = num;
                $scope.integer_ans = $scope.groupedQuestions[$scope.currentQuestion].integer_choice;
                $scope.changeQuestionTimer();
                // //////console.log($scope.groupedQuestions);
            }
        }


        $scope.markAnswer = function(quesNo, optionIndex) {
            //////console.log(optionIndex);
            if ($scope.groupedQuestions[quesNo].submitted == false) {
                if ($scope.groupedQuestions[quesNo].type === 'single_correct_choice_type') {
                    for (var i = 0; i < $scope.groupedQuestions.length; i++) {
                        for (var k = 0; k < $scope.groupedQuestions[quesNo].options.length; k++) {
                            if (k == optionIndex) {
                                $scope.groupedQuestions[quesNo].options[optionIndex].chosen = false;
                                $scope.groupedQuestions[quesNo].attempted = true;
                            } else {
                                $scope.groupedQuestions[quesNo].options[k].chosen = true;
                            }
                        };
                    };
                }
                if ($scope.groupedQuestions[quesNo].type === 'multiple_correct_choice_type') {
                    //////console.log(quesNo);
                    $scope.groupedQuestions[quesNo].options[optionIndex].chosen = !$scope.groupedQuestions[quesNo].options[optionIndex].chosen;
                    $scope.groupedQuestions[quesNo].attempted = true;
                }

            }

        }
        $scope.showSubmitted = function() {

            var correct_test = 0;
            var incorrect_test = 0;

            $scope.groupedQuestions[$scope.currentQuestion].submitted = true;
            if ($scope.groupedQuestions[$scope.currentQuestion].solution == "No Solution Available!!") {
                $scope.no_solution = false;
            } else {
                $scope.no_solution = true;
            }
            if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                $scope.answered = $scope.answered + 1;
            }

            //////console.log($scope.groupedQuestions);
            if (($scope.groupedQuestions[$scope.currentQuestion].skipped == true) && ($scope.groupedQuestions[$scope.currentQuestion].attempted)) {
                $scope.skippedQues = $scope.skippedQues - 1;
            }
            if ($scope.groupedQuestions[$scope.currentQuestion].type === 'single_correct_choice_type') {
                for (var i = 0; i < $scope.groupedQuestions[$scope.currentQuestion].options.length; i++) {
                    if (!$scope.groupedQuestions[$scope.currentQuestion].options[i].chosen) {
                        $scope.groupedQuestions[$scope.currentQuestion].submittedAns.push($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id);
                        if ($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id == $scope.groupedQuestions[$scope.currentQuestion].answer[0].opt_id) {
                            $scope.groupedQuestions[$scope.currentQuestion].options[i].correct = true;
                            $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                            $scope.groupedQuestions[$scope.currentQuestion].correctAns = true;
                        } else {
                            $scope.groupedQuestions[$scope.currentQuestion].options[i].incorrect = true;
                            $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                            $scope.groupedQuestions[$scope.currentQuestion].incorrectAns = true;
                        }
                    } else {
                        if ($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id == $scope.groupedQuestions[$scope.currentQuestion].answer[0].opt_id) {
                            $scope.groupedQuestions[$scope.currentQuestion].options[i].skipped = true;
                        }
                    }
                };
            } else if ($scope.groupedQuestions[$scope.currentQuestion].type === 'multiple_correct_choice_type') {

                if ($scope.groupedQuestions[$scope.currentQuestion].attempted) {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                    var tmp = false;
                    for (var i = 0; i < $scope.groupedQuestions[$scope.currentQuestion].options.length; i++) {
                        if (!$scope.groupedQuestions[$scope.currentQuestion].options[i].chosen) {
                            $scope.groupedQuestions[$scope.currentQuestion].submittedAns.push($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id);
                            for (var j = 0; j < $scope.groupedQuestions[$scope.currentQuestion].answer.length; j++) {
                                if ($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id == $scope.groupedQuestions[$scope.currentQuestion].answer[j].opt_id) {
                                    $scope.groupedQuestions[$scope.currentQuestion].options[i].correct = true;
                                    correct_test++;
                                    tmp = true;
                                    break;
                                } else {
                                    tmp = false;
                                }
                            };
                            if (tmp == false) {
                                incorrect_test++;
                                $scope.groupedQuestions[$scope.currentQuestion].options[i].incorrect = true;
                            }
                        } else {
                            //////console.log("Not attempted");
                            for (var j = 0; j < $scope.groupedQuestions[$scope.currentQuestion].answer.length; j++) {
                                if ($scope.groupedQuestions[$scope.currentQuestion].options[i].opt_id == $scope.groupedQuestions[$scope.currentQuestion].answer[j].opt_id) {
                                    $scope.groupedQuestions[$scope.currentQuestion].options[i].skipped = true;
                                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                                }
                            }
                        }
                    };
                    if (correct_test == $scope.groupedQuestions[$scope.currentQuestion].answer.length) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                        $scope.groupedQuestions[$scope.currentQuestion].correctAns = true;
                    }
                    if (correct_test < $scope.groupedQuestions[$scope.currentQuestion].answer.length) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                        $scope.groupedQuestions[$scope.currentQuestion].incorrectAns = true;
                    }
                    if (incorrect_test > 0) {
                        $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                        $scope.groupedQuestions[$scope.currentQuestion].incorrectAns = true;
                    }
                } else {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                    $scope.skippedQues = $scope.skippedQues + 1;

                    for (var i = 0; i < $scope.groupedQuestions[$scope.currentQuestion].answer.length; i++) {
                        for (var j = 0; j < $scope.groupedQuestions[$scope.currentQuestion].options.length; j++) {
                            if ($scope.groupedQuestions[$scope.currentQuestion].options[j].opt_id == $scope.groupedQuestions[$scope.currentQuestion].answer[i].opt_id) {
                                $scope.groupedQuestions[$scope.currentQuestion].options[j].skipped = true;
                                $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                            }
                        };

                    }
                }
            } else if ($scope.groupedQuestions[$scope.currentQuestion].type === 'integer_type') {
                //////console.log($scope.integer_ans);
                $scope.groupedQuestions[$scope.currentQuestion].submittedAns.push($scope.integer_ans);
                if ($scope.integer_ans !== undefined && $scope.integer_ans !== null) {
                    $scope.answered = $scope.answered + 1;
                    $scope.groupedQuestions[$scope.currentQuestion].integer_choice = $scope.integer_ans;
                    $scope.groupedQuestions[$scope.currentQuestion].attempted = true;
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = false;

                    if (parseInt($scope.groupedQuestions[$scope.currentQuestion].integer_choice) === parseInt($scope.groupedQuestions[$scope.currentQuestion].integer_answer)) {
                        $scope.groupedQuestions[$scope.currentQuestion].correctAns = true;
                    } else {
                        $scope.groupedQuestions[$scope.currentQuestion].incorrectAns = true;
                    }
                } else {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                    $scope.skippedQues = $scope.skippedQues + 1;
                }
                $scope.groupedQuestions[$scope.currentQuestion].integer_answer = parseInt($scope.groupedQuestions[$scope.currentQuestion].integer_answer)
            } else if ($scope.groupedQuestions[$scope.currentQuestion].type === 'matrix_type') {
                //////console.log($scope.groupedQuestions[$scope.currentQuestion].matrixarray);
                if ($scope.groupedQuestions[$scope.currentQuestion].attempted && $scope.groupedQuestions[$scope.currentQuestion].submitted) {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = false;
                } else {
                    $scope.groupedQuestions[$scope.currentQuestion].skipped = true;
                    $scope.skippedQues = $scope.skippedQues + 1;
                }

                /*To create answer matrix*/
                for (var i = 0; i < $scope.groupedQuestions[$scope.currentQuestion].answer.length; i++) {
                    for (var j = 0; j < $scope.groupedQuestions[$scope.currentQuestion].options.length; j++) {
                        if ($scope.groupedQuestions[$scope.currentQuestion].answer[i].opt_id === $scope.groupedQuestions[$scope.currentQuestion].options[j].opt_id) {
                            var options = $scope.groupedQuestions[$scope.currentQuestion].options[j].opt.split(',');
                            for (var k = 0; k < options.length; k++) {
                                for (var l = 0; l < $scope.answer_options.length; l++) {
                                    if (options[k] === $scope.groupedQuestions[$scope.currentQuestion].matrix_option_params[l]) {
                                        $scope.groupedQuestions[$scope.currentQuestion].ans_matrixarray[i][l] = true;
                                    }
                                };
                            }
                        }
                    };
                };

                /*Comapre answer mtrix and answered matrix*/
                var correct = 0;
                if (!$scope.groupedQuestions[$scope.currentQuestion].skipped) {
                    for (var i = 0; i < $scope.quest_options.length; i++) {
                        for (var j = 0; j < $scope.answer_options.length; j++) {
                            if ($scope.groupedQuestions[$scope.currentQuestion].ans_matrixarray[i][j] !== $scope.groupedQuestions[$scope.currentQuestion].matrixarray[i][j]) {
                                correct = 1;
                            }
                        };
                    };
                    //////console.log(correct);
                    if (correct === 0) {
                        $scope.groupedQuestions[$scope.currentQuestion].correctAns = true;
                    } else {
                        $scope.groupedQuestions[$scope.currentQuestion].incorrectAns = true;
                    }
                }

                /*Create answer json for submission*/
                var obj = {};
                var empty = 0;
                for (var i = 0; i < $scope.quest_options.length; i++) {
                    var temp = [];
                    for (var j = 0; j < $scope.answer_options.length; j++) {
                        if ($scope.groupedQuestions[$scope.currentQuestion].matrixarray[i][j] == true) {
                            var opt = $scope.groupedQuestions[$scope.currentQuestion].matrix_option_params[j];
                            temp.push(opt);
                        }
                    };
                    if (temp.length == 0) {
                        empty++;
                    }
                    obj["" + i + ""] = temp;
                };
                if (empty !== $scope.quest_options.length) {
                    $scope.groupedQuestions[$scope.currentQuestion].MatrixAns = obj;
                } else {
                    $scope.groupedQuestions[$scope.currentQuestion].MatrixAns = "";
                }
                //////console.log(obj);
            }
        }
        $scope.matrix_index = function(row, col) {
            $scope.groupedQuestions[$scope.currentQuestion].attempted = true;
            //////console.log(row + '-' + col);
            //////console.log($scope.quest_options.length);
            // //////console.log(matrixarray);
            if (!$scope.groupedQuestions[$scope.currentQuestion].submitted) {
                $scope.groupedQuestions[$scope.currentQuestion].matrixarray[row][col] = !$scope.groupedQuestions[$scope.currentQuestion].matrixarray[row][col];
            }
        }
        $scope.bookmarkQuestion = function(ques) {
            $scope.groupedQuestions[ques].bookmarked = !$scope.groupedQuestions[ques].bookmarked;
        }
        $scope.getBookmarkedQuestions = function() {
            $scope.bookmarkCount = 0;
            if ($scope.groupedQuestions === undefined) {
                return;
            }
            $scope.groupedQuestions.forEach(function(element) {
                if (element.bookmarked) {
                    $scope.bookmarkCount++;
                }
            });
            return $scope.bookmarkCount;
        };
        $scope.showConfirm = function() {
            $scope.testSubmit = true;
        }
        $scope.showEndpop = function() {
            $scope.endPopup = true;
        }
        $scope.closePop = function() {
            $scope.practiceCompleted = true;
            $scope.endPopup = false;
            $state.go('heatmap');
        }
        $scope.Practicepop = function() {
            $scope.endPracice = true;
        }
        $scope.endPracticepop = function() {
            $scope.endPracice = false;
        }
        $scope.stopLoader = function() {
            $scope.dataFound = false;

            $timeout(function() {
                if ($scope.dataFound === false) {
                    $scope.overlay = false;
                    $scope.NetworkSubmitError = true;
                }
            }, 10000);

        }
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $state.go('heatmap');
        }
        $scope.goToHeatmap = function() {
            $scope.ServerSubmitError = false;
            $scope.NetworkSubmitError = false;
            $state.go('heatmap');
        }
        $scope.goToCompletion = function() {

            $scope.practiceCompleted = true;
            $scope.overlay = false;
            $state.go('heatmap');
            ////console.log($scope.finalAnswerJson);


        }

        $scope.closeConfirmPopup = function() {
            $scope.testSubmit = false;
        }
        $scope.showHint = function() {
                $scope.hintForm = !$scope.hintForm;
            }
            /**** Filters ****/

        $scope.allQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.correctCheck = false;
            $scope.incorrectCheck = false;
            $scope.skippedCheck = false;
            $scope.bookmarkCheck = false;
            $scope.AllQuestion = [];
            for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                $scope.AllQuestion.push($scope.groupedQuestionsFilter[i]);
            };
            $scope.groupedQuestions = $scope.AllQuestion;
            $scope.processFilterQuestions();
        }
        $scope.correctQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "correct";
            $scope.allCheck = false;
            $scope.incorrectCheck = false;
            $scope.skippedCheck = false;
            $scope.answerFilter = [];
            if ($scope.correctCheck && !$scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].correctAns == true) {
                        $scope.answerFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.answerFilter);
                $scope.groupedQuestions = $scope.answerFilter;
                $scope.processFilterQuestions();
            } else if ($scope.correctCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].correctAns == true && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.answerFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.answerFilter);
                $scope.groupedQuestions = $scope.answerFilter;
                $scope.processFilterQuestions();
            } else if ($scope.bookmarkCheck && !$scope.correctCheck) {
                $scope.bookmarkedQuestions();
            } else {
                $scope.allQuestions();
            }

        }
        $scope.incorrectQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "incorrect";
            $scope.allCheck = false;
            $scope.correctCheck = false;
            $scope.skippedCheck = false;
            $scope.incorrectFilt = [];
            if ($scope.incorrectCheck && !$scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].incorrectAns == true) {
                        $scope.incorrectFilt.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.incorrectFilt);
                $scope.groupedQuestions = $scope.incorrectFilt;
                $scope.processFilterQuestions();
            } else if ($scope.incorrectCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].incorrectAns == true && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.incorrectFilt.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.incorrectFilt);
                $scope.groupedQuestions = $scope.incorrectFilt;
                $scope.processFilterQuestions();
            } else if ($scope.bookmarkCheck && !$scope.incorrectCheck) {
                $scope.bookmarkedQuestions();
            } else {
                $scope.allQuestions();
            }

        }
        $scope.skippedQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "unattempted";
            $scope.allCheck = false;
            $scope.correctCheck = false;
            $scope.incorrectCheck = false;
            $scope.notAttemptedFilter = [];
            if ($scope.skippedCheck && !$scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].submitted == false) {
                        $scope.notAttemptedFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.notAttemptedFilter);
                $scope.groupedQuestions = $scope.notAttemptedFilter;
                $scope.processFilterQuestions();
            } else if ($scope.skippedCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].submitted == false && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.notAttemptedFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.notAttemptedFilter);
                $scope.groupedQuestions = $scope.notAttemptedFilter;
                $scope.processFilterQuestions();
            } else if ($scope.bookmarkCheck && !$scope.skippedCheck) {
                $scope.bookmarkedQuestions();
            } else {
                $scope.allQuestions();
            }

        }
        $scope.bookmarkedQuestions = function() {
            $scope.sidebarCollapse = true;
            $scope.filter_text = "bookmarked";
            $scope.allCheck = false;
            $scope.bookmarkFilter = [];
            if ($scope.bookmarkCheck && !$scope.correctCheck && !$scope.incorrectCheck && !$scope.skippedCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.bookmarkFilter);
                $scope.groupedQuestions = $scope.bookmarkFilter;
                $scope.processFilterQuestions();
            } else if ($scope.correctCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].correctAns == true && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.bookmarkFilter);
                $scope.groupedQuestions = $scope.bookmarkFilter;
                $scope.processFilterQuestions();
            } else if ($scope.incorrectCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].incorrectAns == true && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.bookmarkFilter);
                $scope.groupedQuestions = $scope.bookmarkFilter;
                $scope.processFilterQuestions();
            } else if ($scope.skippedCheck && $scope.bookmarkCheck) {
                for (var i = 0; i < $scope.groupedQuestionsFilter.length; i++) {
                    if ($scope.groupedQuestionsFilter[i].submitted == false && $scope.groupedQuestionsFilter[i].bookmarked == true) {
                        $scope.bookmarkFilter.push($scope.groupedQuestionsFilter[i]);
                    }
                };
                //////console.log($scope.bookmarkFilter);
                $scope.groupedQuestions = $scope.bookmarkFilter;
                $scope.processFilterQuestions();
            } else if ($scope.skippedCheck && !$scope.bookmarkCheck && !$scope.correctCheck && !$scope.incorrectCheck) {
                $scope.skippedQuestions();
            } else if (!$scope.skippedCheck && !$scope.bookmarkCheck && !$scope.correctCheck && $scope.incorrectCheck) {
                $scope.incorrectQuestions();
            } else if (!$scope.skippedCheck && !$scope.bookmarkCheck && $scope.correctCheck && !$scope.incorrectCheck) {
                $scope.correctQuestions();
            } else {
                $scope.allQuestions();
            }

        }

        /*****/
    })
    .controller('QuestNumCtrl', function($scope, $rootScope, $location, appConfig) {
        $scope.ques = {};
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
        $scope.navClick = function(bol) {
            $rootScope.navhider = !$rootScope.navhider;
            //////console.log($rootScope.navhider);
        };
        $scope.getQuest = function() {
            //////console.log($scope.ques.ques_num);
            $location.path('/test_quest/' + $scope.ques.ques_num);
        }
    })
    .controller('previewCtrl', function($scope, $location, $ionicHistory, $state, $timeout, $interval, $ionicPlatform, $ionicScrollDelegate, HardwareBackButtonManager, TestService, $rootScope, $stateParams, localstorage, appConfig) {
        $scope.overlay = true;
        $scope.$watch('online', function(newStatus) {
            ////console.log(newStatus);
            if (!newStatus) {
                $scope.netWork = true;
                $scope.overlay = false;
            } else {
                // $scope.overlay = true;
                $scope.netWork = false;
                $scope.sub_topics = false;
                //$scope.dataFound = false;
            }
        });

        $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 10000);
        $scope.goToLogin = function() {
            $scope.timeOut = false;
            $scope.netWork = false;
            $scope.overlay = false;
            localstorage.set('logged', false);
        }
        HardwareBackButtonManager.enable();

        $scope.top = localstorage.get('topic_pos');
        //////console.log('Position : ' + $scope.top);

        $scope.login = function() {
            $scope.nav_views = true;
            $state.go('newlogin');
        }
        $scope.register = function() {
            $scope.nav_views = true;
            $state.go('newregister');
        }
        $scope.practClick = function(id, sub) {

            // ////console.log(sub);
            // ////console.log($scope.heatData[0].topics[id]);
            if (sub === 'phy') {
                $location.path('/newlogin/' + $scope.heatData[0].topics[id] + '/' + $scope.heatData[0].subject + '/preview/0');
            } else if (sub === 'che') {
                $location.path('/newlogin/' + $scope.heatData[1].topics[id] + '/' + $scope.heatData[1].subject + '/preview/0');
            } else if (sub === 'mat') {
                $location.path('/newlogin/' + $scope.heatData[2].topics[id] + '/' + $scope.heatData[2].subject + '/preview/0');
            }
            $scope.nav_views = true;
            // $state.go('newlogin');
        }

        $scope.hide_nav = function() {
            $rootScope.navhider = false;
        }
        $scope.phy_topics = function(id) {
            $scope.phy_sub_topics = !$scope.phy_sub_topics;
        }
        $scope.che_topics = function(id) {
            $scope.che_sub_topics = !$scope.che_sub_topics;
        }
        $scope.mat_topics = function(id) {
            $scope.mat_sub_topics = !$scope.mat_sub_topics;
        }



        TestService.getHeatMap("http://conceptowl.com/apis/get_all_topics.json").success(function(successData) {

            if (successData !== null) {
                $scope.dataFound = true;
                $scope.overlay = false;
                ////console.log(successData);
                successData[0].topics.physcore = 50;
                successData[0].topics.chescore = 50;
                successData[0].topics.matscore = 50;
                $scope.heatData = successData;


            }
        });
        /*$scope.practClick = function(id, sub) {
            localstorage.set('topic', sub);
            //////console.log($scope.heatData[0].topics[id].topic_name);
            if (sub === 'phy') {
                $location.path('/practice/' + $scope.heatData[0].topics[id].topic_name + '/' + $scope.heatData[0].subject + '/' + '0000/practice');
            } else if (sub === 'che') {
                $location.path('/practice/' + $scope.heatData[1].topics[id].topic_name + '/' + $scope.heatData[1].subject + '/' + '0000/practice');
            } else if (sub === 'mat') {
                $location.path('/practice/' + $scope.heatData[2].topics[id].topic_name + '/' + $scope.heatData[2].subject + '/' + '0000/practice');
            }

        };*/



        $rootScope.navhider = false;
        $rootScope.doubt = false;
        $rootScope.mock = false;
        $rootScope.practice = true;
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.navClick = function(bol) {


            $rootScope.navhider = !$rootScope.navhider;

            //////console.log($rootScope.navhider);

        };
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };

        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
    })
    .controller('testpreviewCtrl', function($scope, $location, $ionicHistory, $state, $timeout, $interval, $ionicPlatform, $ionicScrollDelegate, HardwareBackButtonManager, TestService, $rootScope, $stateParams, localstorage, appConfig) {
        $scope.overScore = false;
        $rootScope.navhider = false;
        $scope.overlay = true;
        $scope.dataFound = false;

        $timeout(function() {
            if ($scope.dataFound === false) {
                $scope.overlay = false;
                $scope.timeOut = true;
            }
        }, 10000);
        $scope.goToLogin = function() {
                $scope.timeOut = false;
                $state.go('heatmap');
            }
            //////console.log($rootScope.navhider);
        $rootScope.mock = true;
        $rootScope.doubt = false;

        HardwareBackButtonManager.enable();
        $scope.hide_nav = function() {
            $rootScope.navhider = false;
        }

        $scope.login = function() {
            $scope.nav_views = true;
            $state.go('newlogin');
        }
        $scope.register = function() {
            $scope.nav_views = true;
            $state.go('newregister');
        }

        $scope.stud_id = localstorage.get("stud_id");



        TestService.getAllTest("http://conceptowl.com/apis/get_all_test_series_for_guest.json").success(function(successData) {
                if (successData !== null) {
                    $scope.overlay = false;
                    $scope.dataFound = true;
                    ////console.log(successData);
                    $scope.tests = successData;
                    $scope.primar_test = {};
                    $scope.maintest = [];

                    for (var i = 0; i < successData.length; i++) {
                        if (successData[i].testtype === "main_test") {
                            $scope.primary_test = successData[i];
                        }
                    };
                    var j = 0;
                    for (var i = 0; i < successData.length; i++) {
                        if (successData[i].testtype !== "main_test") {
                            $scope.maintest[j] = successData[i];
                            j++;
                        }
                    };
                    //////console.log($scope.primary_test);

                    //////console.log($scope.maintest);
                    $scope.results = $scope.maintest;
                }

            })
            .error(function(e) {
                $timeout(function() {
                    $scope.overlay = false;
                    $ionicHistory.goBack();
                }, 2000);
            })

        $scope.testpop = function(id) {
            //////console.log(id);
            $scope.ind = id;
        };



        $scope.registerExam = function() {
            $scope.nav_views = true;
            $location.path('/newlogin/' + $scope.primary_test.testname + '/' + $scope.primary_test.test_time + '/test_preview/' + $scope.primary_test.testid);
            // $state.go('newlogin');
        }


        $scope.reg_test = function(id) {
            $scope.nav_views = true;
            $location.path('/newlogin/' + $scope.results[id].testname + '/' + $scope.results[id].test_time + '/test_preview/' + $scope.results[id].testid);
            // $state.go('newlogin');
        }

        $rootScope.practice = false;
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.navClick = function(bol) {


            $rootScope.navhider = !$rootScope.navhider;

            //////console.log($rootScope.navhider);

        };
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };

        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
    })
    .controller('forgotCtrl', function($scope, ForgotService, $state, appConfig) {

        $scope.forgot = {};
        $scope.jsonMail = {};
        $scope.forgot_pass = function() {
            $scope.email_error = false;
            $scope.overlay = true;
            //console.log($scope.forgot.mail);
            $scope.jsonMail.email = $scope.forgot.mail;
            if ($scope.forgot.mail === "" || $scope.forgot.mail === undefined) {
                $scope.overlay = false;
                $scope.email_error = true;
            }
            if ($scope.forgot.mail !== "" && $scope.forgot.mail !== undefined) {
                $scope.email_error = false;
                ForgotService.postForgot("/apis/forgot_password.json", $scope.jsonMail).success(function(successData) {
                        if (successData !== null) {
                            $scope.overlay = false;
                            //console.log(successData);
                            $scope.forgotSubmit = true;
                            $scope.status = successData.status;
                            $scope.forgot_message = successData.message;
                            $scope.forgot.mail = "";
                        }
                    })
                    .error(function(err) {
                        $scope.overlay = false;
                        $scope.forgotSubmit = true;
                        $scope.forgot_message = "No network"
                            //console.log(err);
                        $scope.forgot.mail = "";
                    })
            }

        }
        $scope.closeforgotConfirm = function() {
            if ($scope.status === "Success") {
                $scope.forgot_view = true;
                $state.go('newlogin');
                $scope.forgotSubmit = false;
            }
            $scope.forgotSubmit = false;
        }
    })
    .controller('payCtrl', function($scope, $stateParams, PayService, appConfig, localstorage, $ionicHistory) {
        //console.log($stateParams.page);
        $scope.overlay = true;
    })
    .controller('practiceSetCtrl', function($scope, $rootScope, $state, $stateParams, appConfig, TestService, localstorage, $location, $ionicHistory) {

        $ionicHistory.nextViewOptions({
            disableAnimate: true
        });
        $scope.overlay = true;
        $scope.stud_id = localstorage.get('stud_id');
        $scope.topic = $stateParams.topic;

        $scope.subject = $stateParams.subject;

        $scope.subName = $scope.subject.charAt(0).toUpperCase() + $scope.subject.slice(1);

        localstorage.set('topic', $scope.topic);

        console.log($scope.topic + " : " + $scope.subject);

        function isEmpty(obj) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop))
                    return false;
            }
            return true;
        }
        //http://conceptowl.com/apis/practice_questions.json?topic="+encodeURIComponent($scope.topic)+"&subject="+$scope.subject
        TestService.getPracticeSets("http://conceptowl.com/apis/practice_questions.json?topic=" + encodeURIComponent($scope.topic) + "&subject=" + $scope.subject + "&studentid=" + $scope.stud_id).success(function(successData) {
            $scope.overlay = false;
            $scope.daily_practice_set = successData.daily_practice_problems;
            $scope.question_bank_set = successData.question_banks;
            if (isEmpty(successData.bookmarked_test)) {
                // console.log("empty");
                $scope.bookmarkCount = 0;
            } else {
                $scope.bookmarkCount = successData.bookmarked_test.total_ques_count;
                $scope.bookmarkQuestions = successData.bookmarked_test;
            }
            console.log(successData);
        });
        $scope.goBack = function() {
            $scope.nav_views = true;
            $state.go('heatmap');
        }
        $scope.startTest = function(testjson, mode, id, tag, type, pause) {
            if (mode === false) {
                if (pause) {
                    if (tag === "daily") {
                        if (type === 'easy') {
                            $scope.paperId = $scope.daily_practice_set.easy[id].paperid;
                            $scope.test_id = $scope.daily_practice_set.easy[id].test_id;
                            $location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        } else if (type === 'med') {
                            $scope.paperId = $scope.daily_practice_set.moderate[id].paperid;
                            $scope.test_id = $scope.daily_practice_set.moderate[id].test_id;
                            $location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        } else if (type === 'diff') {
                            $scope.paperId = $scope.daily_practice_set.difficult[id].paperid;
                            $scope.test_id = $scope.daily_practice_set.difficult[id].test_id;
                            $location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        }
                    }
                    if (tag === "bank") {
                        if (type === 'easy') {
                            $scope.paperId = $scope.question_bank_set.easy[id].paperid;
                            $scope.test_id = $scope.question_bank_set.easy[id].test_id;
                            $location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        } else if (type === 'med') {
                            $scope.paperId = $scope.question_bank_set.moderate[id].paperid;
                            $scope.test_id = $scope.question_bank_set.moderate[id].test_id;
                            $location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        } else if (type === 'diff') {
                            $scope.paperId = $scope.question_bank_set.difficult[id].paperid;
                            $scope.test_id = $scope.question_bank_set.difficult[id].test_id;
                            $location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        }
                    }
                } else {
                    TestService.postTestAnswers("/apis/create_paper.json", testjson).success(function(successData) {
                        if (successData !== null) {
                            $scope.dataFound = true;
                            $scope.overlay = false;
                            console.log(successData);
                            $scope.nav_views = true;
                            $location.path('/practice/' + successData.testid + '/' + $scope.subject + '/' + successData.paperid + '/practice');
                        }
                    });
                }

            } else if (mode === true) {
                console.log("Already taken");
                $scope.optionPopup = true;
                $scope.overlay = false;
                if (tag === "daily") {
                    if (type === 'easy') {
                        $scope.paperId = $scope.daily_practice_set.easy[id].paperid;
                        //$scope.test_id = $scope.daily_practice_set.easy[id].test_id;
                        //$location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        $scope.id = "E" + (id + 1);
                    } else if (type === 'med') {
                        $scope.paperId = $scope.daily_practice_set.moderate[id].paperid;
                        //$scope.test_id = $scope.daily_practice_set.moderate[id].test_id;
                        //$location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        $scope.id = "M" + (id + 1);
                    } else if (type === 'diff') {
                        $scope.paperId = $scope.daily_practice_set.difficult[id].paperid;
                        //$scope.test_id = $scope.daily_practice_set.difficult[id].test_id;
                        //$location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        $scope.id = "D" + (id + 1);
                    }
                }
                if (tag === "bank") {
                    if (type === 'easy') {
                        $scope.paperId = $scope.question_bank_set.easy[id].paperid;
                        //$scope.test_id = $scope.question_bank_set.easy[id].test_id;
                        //$location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        $scope.id = "E" + (id + 1);
                    } else if (type === 'med') {
                        $scope.paperId = $scope.question_bank_set.moderate[id].paperid;
                        //$scope.test_id = $scope.question_bank_set.moderate[id].test_id;
                        //$location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        $scope.id = "M" + (id + 1);
                    } else if (type === 'diff') {
                        $scope.paperId = $scope.question_bank_set.difficult[id].paperid;
                        //$scope.test_id = $scope.question_bank_set.difficult[id].test_id;
                        //$location.path('/practice/' + $scope.test_id + '/' + $scope.subject + '/' + $scope.paperId + '/practice');
                        $scope.id = "D" + (id + 1);
                    }
                }
            }

        }
        $scope.createPaper = function(type, id, tag, mode) {
            $scope.overlay = true;
            var pause = false;
            //console.log($scope.buttons[id]);
            console.log(type);
            if (tag === "daily") {
                if (type === 'easy') {
                    var testjson = {
                        "studentid": parseInt($scope.stud_id),
                        "testid": $scope.daily_practice_set.easy[id].test_id
                    }
                    if ($scope.daily_practice_set.easy[id].attempted_ques_count === 0) {
                        pause = false;
                    } else {
                        pause = true;
                    }
                } else if (type === 'med') {
                    var testjson = {
                        "studentid": parseInt($scope.stud_id),
                        "testid": $scope.daily_practice_set.moderate[id].test_id
                    }
                    if ($scope.daily_practice_set.moderate[id].attempted_ques_count === 0) {
                        pause = false;
                    } else {
                        pause = true;
                    }
                } else if (type === 'diff') {
                    var testjson = {
                        "studentid": parseInt($scope.stud_id),
                        "testid": $scope.daily_practice_set.difficult[id].test_id
                    }
                    if ($scope.daily_practice_set.difficult[id].attempted_ques_count === 0) {
                        pause = false;
                    } else {
                        pause = true;
                    }
                }
            }

            if (tag === "bank") {
                if (type === 'easy') {
                    var testjson = {
                        "studentid": parseInt($scope.stud_id),
                        "testid": $scope.question_bank_set.easy[id].test_id
                    }
                    if ($scope.question_bank_set.easy[id].attempted_ques_count === 0) {
                        pause = false;
                    } else {
                        pause = true;
                    }
                } else if (type === 'med') {
                    var testjson = {
                        "studentid": parseInt($scope.stud_id),
                        "testid": $scope.question_bank_set.moderate[id].test_id
                    }
                    if ($scope.question_bank_set.moderate[id].attempted_ques_count === 0) {
                        pause = false;
                    } else {
                        pause = true;
                    }
                } else if (type === 'diff') {
                    var testjson = {
                        "studentid": parseInt($scope.stud_id),
                        "testid": $scope.question_bank_set.difficult[id].test_id
                    }
                    if ($scope.question_bank_set.difficult[id].attempted_ques_count === 0) {
                        pause = false;
                    } else {
                        pause = true;
                    }
                }

            }
            console.log(testjson);
            $scope.startTest(testjson, mode, id, tag, type, pause);
        }
        $scope.closeOptionPopup = function() {
            $scope.optionPopup = false;
        }
        $scope.Review = function() {
            console.log($scope.paperId);
            $scope.nav_views = true;
            $location.path('/pract_analysis/' + $scope.paperId + '/' + $scope.subject);
        }
        $scope.Retake = function() {
            $scope.nav_views = true;
            $location.path('/practice/' + $scope.topic + '/' + $scope.subject + '/' + $scope.paperId + '/retake');
        }

        $scope.getBookmarked = function() {
            if ($scope.bookmarkCount !== 0) {
                localstorage.set("bookmark", true);
                $scope.overlay = true;
                var testjson = {
                    "studentid": parseInt($scope.stud_id),
                    "testid": $scope.bookmarkQuestions.test_id
                }
                TestService.postTestAnswers("/apis/create_paper.json", testjson).success(function(successData) {
                    if (successData !== null) {
                        $scope.dataFound = true;
                        $scope.overlay = false;
                        console.log(successData);
                        $scope.nav_views = true;
                        $location.path('/practice/' + successData.testid + '/' + $scope.subject + '/' + successData.paperid + '/practice');
                    }
                });
            }
        }

        $rootScope.navhider = false;
        $rootScope.doubt = false;
        $rootScope.mock = false;
        $rootScope.practice = true;
        $scope.practiceClick = function() {
            $rootScope.practice = !$rootScope.practice;
            $rootScope.doubt = false;
            $rootScope.mock = false;
        };
        $scope.navClick = function(bol) {


            $rootScope.navhider = !$rootScope.navhider;

            //////console.log($rootScope.navhider);

        };
        $scope.mockClick = function() {
            $rootScope.mock = !$rootScope.mock;
            $rootScope.doubt = false;
            $rootScope.practice = false;
        };

        $scope.doubtClick = function() {
            $rootScope.mock = false;
            $rootScope.doubt = !$rootScope.doubt;
            $rootScope.practice = false;
        };
    });
