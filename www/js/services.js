angular.module('concept.services', [])
    .constant("FACEBOOK_APP_ID", "1695283740705689")
    .factory('localstorage', ['$window', '$rootScope', function($window, $rootScope) {
        'use strict';
        return {
            set: function(key, value) {
                $window.localStorage.setItem(key, value);
            },
            get: function(key, defaultValue) {
                return $window.localStorage.getItem(key, defaultValue);
            },
            setObject: function(key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function(key) {
                return JSON.parse($window.localStorage[key] || '{}');
            }
        }
    }])
    .factory('TestService', ['$http', 'appConfig', function($http, appConfig) {
        'use strict';
        return {
            getQuestions: function(testId, paperId) {
                return $http.get(appConfig.baseUrl + "test_series_details.json?testid=" + testId + "&paperid=" + paperId);
            },
            getAllTest: function(url) {
                return $http.get(url);
            },
            getPrevTest: function(url) {
                return $http.get(url);
            },
            getUpTest: function(url) {
                return $http.get(url);
            },

            getTestQuestions: function(url) {
                return $http.get(url);
            },
            getHeatMap: function(url) {
                return $http.get(url);
            },
            getTopics: function(url) {
                return $http.get(url);
            },
            getPracticeSets: function(url) {
                return $http.get(url);
            },
            postTestAnswers: function(url, sub) {
                return $http({
                    method: 'POST',
                    url: url,
                    data: sub,
                    'Content-Type': 'application/json'
                });
            },
            postTestQues: function(url, sub) {
                return $http({
                    method: 'POST',
                    url: url,
                    data: sub,
                    'Content-Type': 'application/json'
                });
            },
            postTestQ: function(url) {
                return $http({
                    method: 'POST',
                    url: url,
                    'Content-Type': 'application/json'
                });
            },
            postSchedule: function(url, sub) {
                return $http({
                    method: 'POST',
                    url: url,
                    data: sub,
                    'Content-Type': 'application/json'
                });
            },
            postLogin: function(url, sub) {
                //console.log(sub);
                return $http({
                    method: 'POST',
                    url: url,
                    data: sub,
                    'Content-Type': 'application/json'
                });
            }
        };
    }])
    .factory('LoginService', ['$http', function($http) {
        'use strict';
        return {
            postLogin: function(url, sub) {
                //console.log(sub);
                return $http({
                    method: 'POST',
                    url: url,
                    data: sub,
                    'Content-Type': 'application/json'
                });
            },
            getGoogle: function(url, goog, code) {
                //console.log(goog);
                return $http({
                    method: 'POST',
                    url: url,
                    data: "client_id=" + goog.client_id + "&client_secret=" + goog.client_secret + "&redirect_uri=http://localhost:8100" + "&grant_type=authorization_code" + "&code=" + code,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            }
        };
    }])
    .factory('RegisterService', ['$http', function($http) {
        'use strict';
        return {

            postRegister: function(url, sub) {
                return $http({
                    method: 'POST',
                    url: url,
                    data: sub,
                    'Content-Type': 'application/json'
                });
            }
        };
    }])
    .factory('ForgotService', ['$http', function($http) {
        'use strict';
        return {

            postForgot: function(url, sub) {
                return $http({
                    method: 'POST',
                    url: url,
                    data: sub,
                    'Content-Type': 'application/json'
                });
            }
        };
    }])
    .factory('PracticeService', ['$http', 'appConfig', function($http, appConfig) {
        'use strict';
        return {
            getPracticeTest: function(studentId) {
                return $http.get(appConfig.baseUrl + "practice_session_review.json?student_id=" + studentId);
            }
        };
    }])
    .factory('PayService', ['$http','appConfig', function($http,appConfig) {
        'use strict';
        return {
            setPayment: function(url, data) {
                console.log(data);
                return $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    'Content-Type': 'application/json'
                });
            },
            proceedPayment:function(url,data){
                console.log(data);
                return $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    'Content-Type': 'application/json'
                });
            },
            getStatus:function(product_id,user_id){
                return $http.get(appConfig.baseUrl + "check_order_status.json?product_id="+product_id+"&user_id="+user_id);
            }
        };
    }])
    .factory('ngFB', function($q, $window) {

        function init(params) {
            return $window.openFB.init(params);
        }

        function login(options) {
            var deferred = $q.defer();
            $window.openFB.login(function(result) {
                if (result.status === "connected") {
                    deferred.resolve(result);
                } else {
                    deferred.reject(result);
                }
            }, options);
            return deferred.promise;
        }

        function logout() {
            var deferred = $q.defer();
            $window.openFB.logout(function() {
                deferred.resolve();
            });
            return deferred.promise;
        }

        function api(obj) {
            var deferred = $q.defer();
            obj.success = function(result) {
                deferred.resolve(result);
            };
            obj.error = function(error) {
                deferred.reject(error);
            };
            $window.openFB.api(obj);
            return deferred.promise;
        }

        function revokePermissions() {
            var deferred = $q.defer();
            $window.openFB.revokePermissions(
                function() {
                    deferred.resolve();
                },
                function() {
                    deferred.reject();
                }
            );
            return deferred.promise;
        }

        function getLoginStatus() {
            var deferred = $q.defer();
            $window.openFB.getLoginStatus(
                function(result) {
                    deferred.resolve(result);
                }
            );
            return deferred.promise;
        }

        return {
            init: init,
            login: login,
            logout: logout,
            revokePermissions: revokePermissions,
            api: api,
            getLoginStatus: getLoginStatus
        };

    })
    .service("directiveService", function() {
        var listeners = [];

        return {
            subscribe: function(callback) {

                listeners.push(callback);
            },
            publish: function(msg) {
                //console.log("ok");
                angular.forEach(listeners, function(value, key) {
                    value(msg);

                });
            }
        };
    })
    .directive("jQueryDirective", function(directiveService, $compile, $rootScope) {
        directiveService.subscribe(function(msg) {
            // pretend this is jQuery 
            var scope = $rootScope.sc;
            var template = $compile(msg)(scope);

            document.getElementById("question-option-" + 0).innerHTML = template;

            //$compile(document.getElementById("question-option-" + 0))(scope);
        });
        return {
            restrict: 'E'
        };
    })
    .directive("mathjaxBind", function() {
        return {
            restrict: "A",
            scope: {
                text: "@mathjaxBind"
            },
            controller: ["$scope", "$element", "$attrs", function($scope, $element, $attrs) {

                $scope.$watch('text', function(value) {
                    //                //console.log(value);
                    var $script = angular.element("<script type='math/tex; mode=" + ($attrs.mathjaxMode == undefined ? "inline" : $attrs.mathjaxMode) + "'>")
                        .html(value == undefined ? "" : value);
                    $element.html("");
                    $element.append($script);
                    MathJax.Hub.Queue(["Reprocess", MathJax.Hub, $element[0]]);
                });
            }]

        };
    })
    .directive('dynamic', function($compile) {
        return {
            restrict: 'A',
            replace: true,
            link: function(scope, ele, attrs) {
                scope.$watch(attrs.dynamic,
                    function(html) {
                        html = html.replace(/<br \/>/g, "");
                        if (html.indexOf('hspace') === -1 && html.indexOf('mathsf') === -1) {
                            html = html.replace(/\{/g, " \{");
                            html = html.replace(/\{/g, " \{");
                            html = html.replace(/\{\{/g, " \{ \{");
                            html = html.replace(/\}\}/g, " \} \}");
                        }

                        html = html.replace(/\$\$([^$]+)\$\$/g, "<span mathjax-bind=\"$1\"></span>");
                        html = html.replace(/\$([^$]+)\$/g, "<span mathjax-bind=\"$1\"></span>");
                        ele.html(html);
                        //   //console.log(html);
                        $compile(ele.contents())(scope);
                    });
            }
        };
    })
    .directive('elemReady', function($parse) {
        return {
            restrict: 'A',
            link: function($scope, elem, attrs) {
                elem.ready(function() {
                    $scope.$apply(function() {
                        var func = $parse(attrs.elemReady);
                        func($scope);
                    })
                })
            }
        }
    });
